#ifndef board_h
#define board_h

//----------- 时钟相关 ------------------
	//#define HSI_CLOCK		16000000	//内部RC振荡器频率，固定的16M
	//#define HSE_CLOCK		25000000	//外部晶振频率 ，在keil的 Misc Control 中定义：-DHSE_VALUE=25000000
										//		等价于#define HSE_VALUE	25000000
	#define PLL_M			HSE_VALUE/1000000	//在这套配置中，与晶振频率保持同步就好【8M就填8,25M就填25】
	#define PLL_N			336
	#define PLL_P			2			
	#define PLL_Q			7			//用于USB
	#define PLL_I2S_N		
	#define PLL_I2S_R
	#define PLL_SOUCE_MUX	1	//0:HSI， 1:HSE[晶振]
	#define AHB_Prescaler	1
	#define APB1_Presoaler	4
	#define APB2_Prescaler	2

//---------- GPIO相关 -------------------
	
	typedef enum {
		LED,
		RS485_RE,
		SPI_CS,
	}Pin_number;
	
	#define GPIO_LIST_DEF				\
		[LED]={	/*LED [PE13]*/				\
			.Mode=GPIO_OUT_PP,			/*GPIO_Mode*/			\
			.Port = GPIOE, 				/*GPIO_TypeDef**/		\
			.speed=GPIO_Speed_100MHz, 	/*GPIOSpeed_TypeDef*/	\
			.Pin=GPIO_Pin_13, 			/*Pin*/					\
			.pullMode=PULL_NONE			/*GPIO_PULL_Mode*/		\
		},														\
		[RS485_RE]={	/*RS485_RE [PD7]*/			\
			.Mode=GPIO_OUT_PP,			/*GPIO_Mode*/			\
			.Port = GPIOD, 				/*GPIO_TypeDef**/		\
			.speed=GPIO_Speed_100MHz, 	/*GPIOSpeed_TypeDef*/	\
			.Pin=GPIO_Pin_7, 			/*Pin*/					\
			.pullMode=PULL_NONE			/*GPIO_PULL_Mode*/		\
		},														\
		[SPI_CS]={	/*SPI_FLASH[ PE3 ]*/			\
			.Mode=GPIO_OUT_PP,			/*GPIO_Mode*/			\
			.Port = GPIOE, 				/*GPIO_TypeDef**/		\
			.speed=GPIO_Speed_100MHz, 	/*GPIOSpeed_TypeDef*/	\
			.Pin=GPIO_Pin_3, 			/*Pin*/					\
			.pullMode=PULL_NONE			/*GPIO_PULL_Mode*/		\
		},	
		
//---------- UART相关 -------------------
		
	typedef enum {
		UART1,
		UART2,
		UART_NUM		//放在最后，表示串口数量
	}UART_number;
	
			
		
#endif
		
