

#include "mcu.h"
#include "stdint.h"
#include "board.h"
#include "SoftDelay.h"
#include "mlibc_base.h"
#include "stdio.h"
#include "mcu_usart_1.h"


uint8_t txbuff[] = "hello9999\r\n";
int main()
{
	int i=0;
	mcu_clk_init();
	RCC_Enable_DMA1();
	RCC_Enable_DMA2();
	RCC_Enable_GPIOA();
	RCC_Enable_GPIOE();
	RCC_Enable_USART1();
	RCC_Enable_USART2();
	uart1_init(  USART1_PA9_PA10, 115200, Parity_none );
	//usart_init( Uart2, USART2_PA2_PA3,  115200, Parity_none );
	
	uart1_put_array_start(txbuff,sizeof(txbuff));
	while(1)
	{
////		if(RS232_REC_Flag == 1)	   //判断是否接收到一屏数据
//		{
//			log_inf(0,"hello\r\n",7);
//			int n = snprintf((char*)txbuff,sizeof(txbuff) , "hello%d\r\n", i++);
////			dma_uart_tx();
//			uart1_put_array_start(  txbuff, n);
////			uart_write(UART1,"hello\r\n",7);
//			delay_ms(100);
//		}
		
		TypeFrame* uart1rx = uart1_read();
		if(uart1rx != NULL)
		{
			if( uart1rx->len != 0)
			{
				log_inf(0,"rx=%d\r\n" , uart1rx->len);
				//--- 接收处理 ----
				//xcmd_task( uart1rx->array , uart1rx->len);
				//
				uart1_put_array_start( uart1rx->array , uart1rx->len);
//				LcomBase_Analayse(uart1rx->array ,uart1rx->len , app_callback);
				uart1_clear_rxframe(uart1rx);
			}
		}
	}
}

