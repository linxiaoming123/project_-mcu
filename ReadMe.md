# 目录说明

## mlibc【库】



## platform【硬件平台，官方库】

- STM32F1_STD : stm32f1的标准库
- STM32F4_STD : stm32f4的标准库
- N32G452 :  国民技术G452标准库

## Demo【模块例程】

1. Demo_COM : 通信Demo
   | 协议      | 特性 |
   | --------- | ---- |
   | ModbusRTU | 主   |
   |           | 从   |
   | DLT645    | 主   |
   |           | 从   |
   | Lcom      | 主   |
   |           | 从   |
2. Demo_Memory: 存储Demo
   | 存储库   | 特性           |
   | -------- | -------------- |
   | FATFS    |                |
   | Littlefs |                |
   | Mfs      | 单区域多文件   |
   |          | 多区域滚动存储 |
   |          | 单区域掉电存储 |
3. Demo_GUI : 显示Demo
   | GUI库 | 特性        |
   | ----- | ----------- |
   | LVGL  |             |
   | mGui  | LED         |
   |       | 数码管      |
   |       | 菜单界面GUI |
   |       | ....        |
   
4. NET_以太网
   
   | 协议库 | 功能     | 特性       |
   | ------ | -------- | ---------- |
   | Lwip   | Ping     |            |
   |        | DHCP     | 从机       |
   |        | TCP      | Sever      |
   |        |          | Client     |
   |        | UDP      | Sever      |
   |        |          | Client     |
   |        | MQTT     | Client     |
   |        | HTTPS1.1 | Client     |
   |        | Web      | 网页服务器 |

## Project【应用工程】
