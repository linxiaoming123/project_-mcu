/**
 * @file mcu_uart.c
 * @author your name (you@domain.com)
 * @brief 
 * @version 0.1
 * @date 2021-10-30
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#include "stm32f4xx.h"
#include "lib_usart.h"
#include "lib_dma.h"
#include "func_bit.h"
#include "mlibc_base.h"
#include "mcu.h"

//====================================================================================================


	




//====================================================================================================
//==================

	static TypeBuff buff[BUFF_NUM];
	static uint8_t BuffIndex = 0;
	static uint8_t BuffReadIndex = 0;
	static uint8_t Full_rxbuff = 0;
	
	volatile  bool IsBusyTX1 = false;
	
//==================	
	void usart_init(	Enum_UART_Number 	uartn,			//UART编号 
						Enum_io_usart 		io , 			//io配置
						uint32_t 			bandrate, 		//波特率
						Enum_Parity 		parity)			//校验位
	{
		usart_rcc_enable(uartn);                		//复位 + 使能模块时钟
        usart_select_io( io ); 							//选择IO
        usart_config_port(uartn , bandrate , parity);	//设置波特率，偶校验
		usart_start(uartn);
		usart_enable_txDMA(uartn);
		usart_enable_txDMA_it(uartn);	
		//接收设置
		usart_enable_rxDMA(Uart1, buff[0].array, NULL, BUFF_LEN);
		usart_enable_rxDMA_it(Uart1);
		//
		ClearBit( USART1->SR , 6);	//开串口中断前清理标志位，以防止异常的串口中断
		usart_interrupt_set(uartn, UART_IT_IDEL | UART_IT_TX); 
	}
	
	
	//------- 阻塞式发送array -----
	void uart_1_put_byte( uint8_t x)
	{
		usart_put_array_dma( Uart1 , &x, 1);
		IsBusyTX1 = true;
		while( IsBusyTX1 );
	}
	void uart_1_put_array( uint8_t* array, uint16_t len)
	{
		usart_put_array_dma( Uart1 , array, len);
		IsBusyTX1 = true;
		while( IsBusyTX1 );
	}
	//等待发送完成
	void uart_1_wait_txfree()
	{
		while( IsBusyTX1 );
	}
	//------- 开始发送array【触发dma发送】 ---------------
	void uart_1_put_array_start( uint8_t* array, uint16_t len )
	{
		IsBusyTX1 = true;
		usart_put_array_dma( Uart1 , array, len);
	}

/**
 * @brief uart1 中断
 * 
 */

unsigned char rxData;
volatile unsigned int count1=0;
	

//设置uart接收缓冲
void uart_set_rxbuff(Enum_UART_Number uartx,TypeBuff* buffx)
{
//	log_inf( 0 ,"set buff index = %d\r\n", BuffIndex);
	usart_enable_rxDMA(uartx, buffx->array, NULL, BUFF_LEN);
}

//--- 重启RX DMA，参数n：DMA接收完成时，剩余的空间 ---
void uart_restart_rx_dma(int n)
{
	usart_disable_rxDMA( Uart1 );
	dma_clear_it_all_flag(DMA_02,	DMA_Stream_02 );
	buff[BuffIndex].len = BUFF_LEN - n;
	BuffIndex++;
	if(BuffIndex >= BUFF_NUM)
	{
		BuffIndex = 0;
	}
	if(buff[BuffIndex].len == 0)	{ 
		uart_set_rxbuff(Uart1, &buff[BuffIndex]); 
	}
	else							{ 
		Full_rxbuff = true; 
		// log_inf( 0 ,"rx buff full\r\n");
	}
}

void USART1_IRQHandler(void)
{
	int tmp = USART1->SR;
	if(tmp &(1<<5))//接收到数据
	{
		rxData = USART1->DR;
	}
	if(tmp &(1<<4))//空闲
	{
		rxData = USART1->DR;
		ClearBit(USART1->SR , 4);
		count1 ++;
		//log_inf( 0 ,"uart Rx over\r\n");
		int n = usart_read_dma_ndtr(Uart1);
		
		uart_restart_rx_dma(n);
		
	}
	if(tmp &(1<<6))//发送完成
	{
		ClearBit( USART1->SR , 6); 
		IsBusyTX1 = false;
	}

}
	
/**
 * @brief uart1-DMA 中断
 * 
 */

//设定TxDMA 回调
void uart_1_txdma_callback()
{
	//log_inf( 0 ,"dma tx over \r\n");
}
uart_DMAHandlel(uart1,2,7,uart_1_txdma_callback)


//设定RxDMA 回调
void uart_1_rxdma_callback()
{
	// log_inf( 0 ,"dma rx full \r\n");
	uart_restart_rx_dma(0);	//DMA满时，剩余 空间为0
}
//uart_DMAHandlel(uart1,2,2,uart_1_rxdma_callback)


	#define uart_DMAHandle(uartn,dma,stream,handle)            		\
			void LINK5(DMA,dma,_Stream,stream,_IRQHandler())      	\
			{                                       \
				if(dma_read_it_flag(LINK2(DMA_0,dma) , LINK2(DMA_Stream_0,stream), Flag_transfer_complete))        \
				{                                                                           \
					dma_clear_it_flag(LINK2(DMA_0,dma) , LINK2(DMA_Stream_0,stream), Flag_transfer_complete);      \
					handle();                                                               \
				}                                                                           \
				if(dma_read_it_flag(LINK2(DMA_0,dma) , LINK2(DMA_Stream_0,stream), Flag_transfer_error))           \
				{                                                                           \
					dma_clear_it_flag(LINK2(DMA_0,dma) , LINK2(DMA_Stream_0,stream), Flag_transfer_error);         \
				}                                                                           \
			}
	#define DMA_NUMBER	2
	#define DMA_STREAM	2
	uart_DMAHandle(uart1,DMA_NUMBER,DMA_STREAM,uart_1_rxdma_callback)

//==========================================================
	TypeBuff* uart_1_read()
	{
		int tmp = BuffReadIndex;
		if(BuffReadIndex==BuffIndex && Full_rxbuff==false)
		{
			return NULL;
		}
		BuffReadIndex ++;
		if(BuffReadIndex >= BUFF_NUM)
		{
			BuffReadIndex = 0;
		}
		return &buff[tmp];
	}
	void  uart_1_clear_rxframe(TypeBuff* buffx)
	{
		if(Full_rxbuff)
		{
			Full_rxbuff = false;
			// log_inf( 0 ,"Clear\r\n");
			uart_set_rxbuff(Uart1, buffx);
		}
		buffx->len = 0;
	}
	
	void  uart_1_clear_allbuff()
	{
		if(Full_rxbuff)
		{
			Full_rxbuff = false;
			// log_inf( 0 ,"Clear\r\n");
			uart_set_rxbuff(Uart1, &buff[0]);
		}
		for(int i=0;i<BUFF_NUM;i++)
		{
			buff[i].len = 0;
		}
	}

