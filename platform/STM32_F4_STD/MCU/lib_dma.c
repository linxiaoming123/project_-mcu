    #include "stdint.h"	  
    #include "lib_clk.h"
//    #include "stdio.h"
    #include "func_bit.h"
	#include "stm32f4xx.h" 
	#include "lib_dma.h"
//	#include "lib_core.h"
	
//================================================================================

	DMA_TypeDef* const DMA_table[] = {DMA1, DMA2};

	DMA_Stream_TypeDef* const DMA_Stream[16] = {
		DMA1_Stream0,
		DMA1_Stream1,
		DMA1_Stream2,
		DMA1_Stream3,
		DMA1_Stream4,
		DMA1_Stream5,
		DMA1_Stream6,
		DMA1_Stream7,
		DMA2_Stream0,
		DMA2_Stream1,
		DMA2_Stream2,
		DMA2_Stream3,
		DMA2_Stream4,
		DMA2_Stream5,
		DMA2_Stream6,
		DMA2_Stream7
	}; 
    
//================================================================================
//		API
//================================================================================ 
	// 步骤1：使能 DMA 时钟
	void dma_enable_clk( Enum_DMAnumber dma )	//DMA编号
	{
		switch( dma)
		{
			case DMA_01:	RCC->AHB1ENR|=1<<21;//DMA1时钟使能 
							break;
			case DMA_02:	RCC->AHB1ENR|=1<<22;//DMA1时钟使能 
							break;
		}
	}
	
	// 步骤2：DMA开始配置
	void dma_wait_for_config(	Enum_DMAnumber dma ,	//DMA编号
								Enum_DmaStream n,		//流编号
								uint32_t timeout)		//超时时间
	{
		//------ 匹配DMA，获取DMA指针 -----
		//DMA_TypeDef* pDMA = DMA_table[dma];
		while(DMA_Stream[dma*8 + n]->CR & 0x01)
		{
			;
		}
	}
	// 步骤3：DMA设置CR寄存器
	void dma_config_stream_cr( Enum_DMAnumber dma ,			//DMA编号
							Enum_DmaStream n,				//流编号
							Register_DMA_config config)		//CR配置
	{
		DMA_Stream_TypeDef* stream = DMA_Stream[dma*8 + n];
		stream -> CR = *(uint32_t*)(& config);
	}

	// 步骤4：DMA设置fifo寄存器
	void dma_config_stream_fifo( Enum_DMAnumber dma ,		//DMA编号
							Enum_DmaStream n,				//流编号
							Register_DMA_SxFCR config)		//FIFO配置
	{
		DMA_Stream_TypeDef* stream = DMA_Stream[dma*8 + n];
		stream -> FCR= *(uint32_t*)(& config);
	}

	
	
	// 步骤5：设置dma外设地址
	void dma_set_peripheral_addr(	Enum_DMAnumber dma ,	//DMA编号
									Enum_DmaStream n,		//流编号
									uint32_t addr)			//超时时间
	{
		DMA_Stream_TypeDef* stream = DMA_Stream[dma*8 + n];
		stream -> PAR = addr;
	}

	// 步骤6：设置dma存储器地址
	void dma_set_memory_addr( 	Enum_DMAnumber dma ,		//DMA编号
								Enum_DmaStream n,			//流编号
								uint32_t buff0,				//缓冲1
								uint32_t buff1,				//缓冲2
								uint16_t len)				//长度
	{
		DMA_Stream_TypeDef* stream = DMA_Stream[dma*8 + n];
		stream -> M0AR = buff0;
		stream -> M1AR = buff1;
		stream -> NDTR = len;
	}

	// 步骤7：使能DMAstream
	void dma_enable_stream( Enum_DMAnumber dma ,			//DMA编号
							Enum_DmaStream n)				//流编号
	{
		DMA_Stream_TypeDef* stream = DMA_Stream[dma*8 + n];
		SetBit(stream->CR , 0);	
	}
	
	
	void dma_disable_stream( Enum_DMAnumber dma ,			//DMA编号
							Enum_DmaStream n)				//流编号
	{
		DMA_Stream_TypeDef* stream = DMA_Stream[dma*8 + n];
		ClearBit(stream->CR , 0);	
	}
	//------------------------- 中断相关 ------------------------
	// 读取DMA中断状态
	
	uint8_t dma_read_it_flag(	Enum_DMAnumber dma ,
								Enum_DmaStream n,
								EnumDmaItFlag flag)
	{
		int x;
		x = (int)n;
		//------ 匹配DMA，获取DMA指针 -----
		DMA_TypeDef* pDMA = DMA_table[dma];
		//-------- 根据DMAstream编号，读取指定位域 ------------
		uint32_t tmp;

		switch(x)
		{
			case 0:
			case 1:
				tmp = ReadBits( pDMA->LISR ,(6*x) ,(6*x+5) ); 
				return ReadBit(tmp, flag);
			case 2:
			case 3:
				x = x -2;
				tmp = ReadBits( pDMA->LISR ,(6*x + 16) ,(6*x+5+16) ); 
				return ReadBit(tmp, flag);
			case 4:
			case 5:
				x = x -4;
				tmp = ReadBits( pDMA->HISR ,(6*x) ,(6*x+5) ); 
				return ReadBit(tmp, flag);
			case 6:
			case 7:
				x = x -6;
				tmp = ReadBits( pDMA->HISR ,(6*x + 16) ,(6*x+5+16) ); 
				return ReadBit(tmp, flag);
		}
		return 0;
	}
	
	// 清DMA中断标志
	void dma_clear_it_flag(	Enum_DMAnumber dma ,	//DMA编号
							Enum_DmaStream n,		//流编号
							EnumDmaItFlag flag)		//标志位
	{
		int x;
		x = (int)n;
		//------ 匹配DMA，获取DMA指针 -----
		DMA_TypeDef* pDMA = DMA_table[dma];
		//-------- 根据DMAstream编号，清指定bit ------------
		switch(x)
		{
			case 0:
			case 1:
				SetBit( pDMA->LIFCR, (6*x+flag));
				break;
			case 2:
			case 3:
				x = x -2;
				SetBit( pDMA->LIFCR, (6*x+flag+16));
				break;
			case 4:
			case 5:
				x = x -4;
				SetBit( pDMA->HIFCR, (6*x+flag));
				break;
			case 6:
			case 7:
				x = x -6;
				SetBit( pDMA->HIFCR, (6*x + flag + 16));
				break;
		}
	}

	// 清DMA-stream所有中断标志
	void dma_clear_it_all_flag(	Enum_DMAnumber dma ,	//DMA编号
							Enum_DmaStream n)		//流编号
	{
		int x;
		x = (int)n;
		//------ 匹配DMA，获取DMA指针 -----
		DMA_TypeDef* pDMA = DMA_table[dma];
		//-------- 根据DMAstream编号，清指定bit ------------
		switch(x)
		{
			case 0:
			case 1:
				pDMA->LIFCR = 0x3d << (6*x) ;	//0b:111101
				break;
			case 2:
			case 3:
				x = x -2;
				pDMA->LIFCR = 0x3d << (6*x + 16) ;	//0b:111101
				break;
			case 4:
			case 5:
				x = x -4;
				pDMA->HIFCR = 0x3d << (6*x) ;	//0b:111101
				break;
			case 6:
			case 7:
				x = x -6;
				pDMA->HIFCR = 0x3d << (6*x + 16) ;	//0b:111101
				break;
		}
	}
	
	int dma_read_ndtr_reg(	Enum_DMAnumber dma ,			//DMA编号
							Enum_DmaStream n)				//流编号
	{
		DMA_Stream_TypeDef* stream = DMA_Stream[dma*8 + n];
		return stream->NDTR;
	}
//----------------------------------------------------------------------------
//	功能API
//----------------------------------------------------------------------------	
	//获取DMA_Stream的地址
	void* dma_get_stream_addr(	Enum_DMAnumber dma ,		//DMA编号
								Enum_DmaStream n)
	{
		DMA_Stream_TypeDef* stream = DMA_Stream[dma*8 + n];
		return (void*)stream;
	}
	
	//读取当前buff编号（返回 0/1）
	int dma_read_buff_index(Enum_DMAnumber dma ,			//DMA编号
							Enum_DmaStream n)				//流编号
	{
		DMA_Stream_TypeDef* stream = DMA_Stream[dma*8 + n];
		return ReadBit( stream->CR , 19);
	}
	
	//切换DMA缓冲为buff_x
	void dma_switch_buff_x(	Enum_DMAnumber dma ,			//DMA编号
							Enum_DmaStream n,				//流编号
							int x)							//设置为Buff0/1				
	{
		DMA_Stream_TypeDef* stream = DMA_Stream[dma*8 + n];
		SetBbitValue(stream->CR , 19, x);
	}	

