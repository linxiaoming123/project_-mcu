
#include "stm32f4xx.h"
#include "i_gpio.h"
#include "board.h"
#include "stdbool.h"

typedef struct{
	GPIO_Mode 			Mode;
	GPIO_TypeDef* 		Port;
	GPIOSpeed_TypeDef 	speed;
	uint32_t 			Pin;
	GPIO_PULL_Mode 		pullMode;
}GPIO_Config;

//==============================================================================================
//	GPIO清单
//==============================================================================================
	
	static GPIO_Config GPIO_List[] = {						  				 
		GPIO_LIST_DEF
	};


//==============================================================================================
//
//==============================================================================================
//转换数组： i_gpio中的枚举，转stm32头文件中的枚举
static const GPIOPuPd_TypeDef PullModeList[] = { GPIO_PuPd_NOPULL , GPIO_PuPd_UP , GPIO_PuPd_DOWN, GPIO_PuPd_UP};

static inline void base_gpio_config( GPIO_Config * config)
{
	GPIO_InitTypeDef  GPIO_InitStructure;
	/* Enable the GPIO_LED Clock */
	GPIO_InitStructure.GPIO_Pin = config->Pin ;
	switch(config->Mode)
	{
		case GPIO_OUT_PP:
			GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
			GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
			break;
		case GPIO_OUT_OD:
			GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
			GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
			break;
		case GPIO_IN:
			GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
			break;
		case GPIO_ANALOG:
			GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AN;
			break;
		case GPIO_AF:
			GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
		default:
			break;
	}
	GPIO_InitStructure.GPIO_PuPd = PullModeList[config->pullMode];
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_Init(GPIOE, &GPIO_InitStructure);
}

static bool base_gpio_read( GPIO_Config * config )
{
	return GPIO_ReadInputDataBit( config->Port , config->Pin);
}

static void base_gpio_write( GPIO_Config * config , uint8_t value)
{
	GPIO_WriteBit(config->Port, config->Pin, (BitAction)value);
}


//==============================================================================================
//
//==============================================================================================


/**
 * @brief 初始化单个io
 * 
 * @param number 
 */
void gpio_init(int number)
{
	//断言：number应小于GPIO_List的元素个数
	int n = sizeof(GPIO_List) / sizeof( GPIO_Config);
	if( number >= n)
	{
		while(1);
		return ;
	}
	//
	GPIO_Config* config = &GPIO_List[number]; 
	base_gpio_config(config);
}
/**
 * @brief 初始化所有io
 * 
 */
void gpio_init_all(void)
{
	GPIO_Config* config;
	int n = sizeof(GPIO_List) / sizeof( GPIO_Config);
	for(int i=0;i<n;i++)
	{
		config = &GPIO_List[i]; 
		base_gpio_config(config);
	}
}

/**
 * @brief 设置输出
 * 
 * @param number 
 * @param value 
 */
void gpio_write(int number , int value)
{
	GPIO_Config* config = &GPIO_List[number]; 
	base_gpio_write(config , value);
}

/**
 * @brief 读IO
 * 
 * @param number 
 * @return int 
 */
int  gpio_read(int number)
{
	GPIO_Config* config = &GPIO_List[number]; 
	return base_gpio_read(config);
}



