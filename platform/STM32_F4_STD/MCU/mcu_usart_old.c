

#include "stm32f4xx.h"
#include "i_uart.h"
#include "board.h"
#include "mcu.h"
#include "mlibc_base.h"
#include "frame.h"


#define RS232_REC_BUFF_SIZE	100



new_static_frame( uart1rxframe0 , 256);
new_static_frame( uart1rxframe1 , 256);



//串口接收中断接收
void USART1_IRQHandler(void)  
{
	if(USART_GetITStatus(USART1, USART_IT_RXNE) != RESET)
	{	
		frame_append_byte( &uart1rxframe0, USART1->DR);
		USART_ClearITPendingBit(USART1, USART_IT_RXNE);
	}
	
	if (USART_GetITStatus(USART1, USART_IT_TXE) != RESET) 
	{
        USART_ClearITPendingBit(USART1, USART_IT_TXE);           /* Clear the USART transmit interrupt                  */
    }	
}

//==================================================================================================
//	数据结构类型定义：
//==================================================================================================
	//UART用GPIO配置的定义
	typedef struct {
		USART_TypeDef* 	usart;
		GPIO_TypeDef* 	TxPort;
		int				TxPin;
		int 			TxPinSource;
		int				TxAF;		//AF 7 selection
		GPIO_TypeDef* 	RxPort;
		int				RxPin;
		int 			RxPinSource;
		int				RxAF;		//AF 7 selection
	}uart_gpio_config;

	//UART-TXDMA定义
    typedef struct{
        DMA_Stream_TypeDef* stream;
        uint8_t 	channel;
		IRQn_Type	IRQ_number;
		uint32_t    addr_m;
		uint32_t    addr_p;
    }TypeDMAconfig;

//==================================================================================================
//	数据结构定义：
//==================================================================================================
	//UART定义
	static TypeUART uart[UART_NUM];
	
//==================================================================================================
//	支持的设置、枚举、枚举与官方库的转换
//==================================================================================================
	//波特率转换
	const static int baudrateList[] = {
		[Baudrate_1200]=1200,
        [Baudrate_2400]=2400,
        [Baudrate_4800]=4800,
        [Baudrate_9600]=9600,
        [Baudrate_19200]=19200,
        [Baudrate_38400]=38400,
        [Baudrate_76800]=76800,
        [Baudrate_115200]=115200,
        [Baudrate_128000]=128000,
        [Baudrate_230400]=230400,
        [Baudrate_450800]=450800,
        [Baudrate_921600]=921600,
        [Baudrate_100000]=100000,
        [Baudrate_200000]=200000
	};
	//校验位转换
	const static int parityList[] = {
		[ParityNone] = USART_Parity_No,     //无校验
        [ParityEven] = USART_Parity_Even,     //偶校验
        [ParityOdd]  = USART_Parity_Odd
	};
	//uart编号枚举 ->寻UART
	const static USART_TypeDef* uartList[] = {
		USART1 , USART2, USART3, UART4 , UART5, USART6, UART7,UART8
	};
	//uart编号枚举 ->寻IRQn
	const static int UART_ISR[] ={
		USART1_IRQn , USART2_IRQn ,USART3_IRQn ,UART4_IRQn ,UART5_IRQn
	};
	
//==================================================================================================
//	芯片支持的设置
//==================================================================================================	
	
	//串口引脚设置
	#define USART1_PA9_PA10		{ USART1,  GPIOA , GPIO_Pin_9, GPIO_PinSource9,  GPIO_AF_USART1, GPIOA, GPIO_Pin_10, GPIO_PinSource10,GPIO_AF_USART1}
	#define USART2_PA2_PA3		{ USART2,  GPIOA , GPIO_Pin_2, GPIO_PinSource2,  GPIO_AF_USART2, GPIOA, GPIO_Pin_3,  GPIO_PinSource3 ,GPIO_AF_USART2}
	
	
	const static uart_gpio_config	uartGpioList[] = {
		[UART1] = USART1_PA9_PA10,	
		[UART2] = USART2_PA2_PA3	
	};

	//DMA配置
	#define UART1_TX_DMA02_Stream07		\
					{ .stream = DMA2_Stream7 , .channel = 4 , .IRQ_number = DMA2_Stream7_IRQn}
	#define UART2_TX_DMA01_Stream06		\
					{ .stream = DMA1_Stream6 , .channel = 4 , .IRQ_number = DMA1_Stream6_IRQn}
    #define UART1_RX_DMA02_Stream02		\
					{ .stream = DMA2_Stream2 , .channel = 4 ,.IRQ_number= DMA2_Stream2_IRQn}
	#define UART2_RX_DMA01_Stream05		\
					{ .stream = DMA1_Stream5 , .channel = 4 , .IRQ_number= DMA1_Stream5_IRQn}
	
	static TypeDMAconfig const ListTxDma[] ={
        [UART1] = UART1_TX_DMA02_Stream07,
        [UART2] = UART2_TX_DMA01_Stream06
    };
	
	static TypeDMAconfig const ListRxDma[] ={
        [UART1] = UART1_RX_DMA02_Stream02,
        [UART2] = UART2_RX_DMA01_Stream05
    };
//==================================================================================================
//
//===============================

	void config_uart_dma_tx( int number, TypeDMAconfig* txdma )
	{
		DMA_InitTypeDef  DMA_InitStructure;
		DMA_Cmd(txdma->stream, DISABLE);                      //关闭DMA传输 
		while (DMA_GetCmdStatus(txdma->stream) != DISABLE){}	//确保DMA可以被设置  
		/* 配置 DMA Stream */
		DMA_InitStructure.DMA_Channel = txdma->channel;  						//通道选择
		DMA_InitStructure.DMA_PeripheralBaseAddr =(uint32_t) &uartList[number]->DR;		//DMA外设地址
		DMA_InitStructure.DMA_Memory0BaseAddr = 0;								//DMA 存储器0地址
		DMA_InitStructure.DMA_DIR = DMA_DIR_MemoryToPeripheral;					//存储器到外设模式
		DMA_InitStructure.DMA_BufferSize = 0;								//数据传输量 
		DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;		//外设非增量模式
		DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;					//存储器增量模式
		DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;	//外设数据长度:8位
		DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;			//存储器数据长度:8位
		DMA_InitStructure.DMA_Mode = DMA_Mode_Normal;							// 使用普通模式 
		DMA_InitStructure.DMA_Priority = DMA_Priority_Medium;					//中等优先级
		DMA_InitStructure.DMA_FIFOMode = DMA_FIFOMode_Disable;         
		DMA_InitStructure.DMA_FIFOThreshold = DMA_FIFOThreshold_Full;
		DMA_InitStructure.DMA_MemoryBurst = DMA_MemoryBurst_Single;				//存储器突发单次传输
		DMA_InitStructure.DMA_PeripheralBurst = DMA_PeripheralBurst_Single;		//外设突发单次传输
		DMA_Init(txdma->stream, &DMA_InitStructure);								//初始化DMA Stream
	}
	
	void config_uart_dma_rx(TypeDMAconfig* rxdma)
	{
		
	}
	
	
	
//==================================================================================================

    /**
     * @brief uart初始化
     * 
     * @param number    : uart编号
     * @param baudrate  : 波特率
     * @param parity    : 校验位
     */
	void uart_gpio_init(uart_gpio_config* config)
	{
		GPIO_InitTypeDef GPIO_InitStructure;
		//TX RX引脚设置
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;    	//复用
		GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;	//GPIO频率
		GPIO_InitStructure.GPIO_OType = GPIO_OType_PP; 		//输出类型【推挽/开漏】
		GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
		GPIO_InitStructure.GPIO_Pin = config->TxPin; 
		GPIO_Init(	config->TxPort, 
					&GPIO_InitStructure);
		GPIO_InitStructure.GPIO_Pin = config->RxPin; 
		GPIO_Init(	config->RxPort, 
					&GPIO_InitStructure);
		//复用设置
		GPIO_PinAFConfig(	config->TxPort,				  	//引脚的Port 	GPIOA	
							config->TxPinSource,			//引脚的Pin		GPIO_PinSource9
							config->TxAF);        			//管脚复用		GPIO_AF_USART1
                                                                                                               
		GPIO_PinAFConfig(	config->RxPort,				  	//引脚的Port 	GPIOA	
							config->RxPinSource,			//引脚的Pin		GPIO_PinSource10
							config->RxAF);        			//管脚复用		GPIO_AF_USART1
	}
	
	
	
	
    void uart_init(int number, UART_Baudrate baudrate, UART_Parity parity)
	{
		C_ASSERT(number <= UART_NUM);
		//GPIO配置
		uart_gpio_config* gpioConfig =(uart_gpio_config*) &uartGpioList[ number ];
		uart_gpio_init(gpioConfig);	
		//UART配置
		USART_ClockInitTypeDef USART_ClockInitStruct;
		USART_InitTypeDef USART_InitStructure;
		USART_TypeDef* uart =(USART_TypeDef*) uartList[number];		
		
		USART_DeInit(uart);
		USART_StructInit(&USART_InitStructure);
		USART_ClockStructInit(&USART_ClockInitStruct);	
		USART_ClockInit(uart,&USART_ClockInitStruct);
		USART_InitStructure.USART_BaudRate = baudrateList[baudrate]; //设置波特率
		USART_InitStructure.USART_WordLength = USART_WordLength_8b;
		USART_InitStructure.USART_StopBits = USART_StopBits_1;
		USART_InitStructure.USART_Parity = parityList[parity]; 		//校验
		USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
		USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
		USART_Init(uart,&USART_InitStructure); 
		USART_ITConfig(uart, USART_IT_RXNE, ENABLE);          	//接收中断使能
		USART_ClearITPendingBit(uart, USART_IT_TC);				//清除中断TC位
		USART_Cmd(uart,ENABLE);									//最后使能串口
		//中断配置
		NVIC_InitTypeDef   NVIC_InitStructure;
		/* 2 bit for pre-emption priority, 2 bits for subpriority */
		NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2); 										
		NVIC_InitStructure.NVIC_IRQChannel = UART_ISR[number];	  
		NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
		NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;	
		NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
		NVIC_Init(&NVIC_InitStructure);
		//DMA配置
		TypeDMAconfig* dmaconfig =  (TypeDMAconfig*)&ListTxDma[number];
		config_uart_dma_tx( number , dmaconfig);
		USART_DMACmd( uart,USART_DMAReq_Tx,ENABLE);  //使能串口1的DMA发送
	}


    /**
     * @brief 设置txEnd中断
     * 
     * @param number 
     * @param txEnd 
     */
    void uart_config_isr_tx(int number, void(*txEnd)(void))
	{
		C_ASSERT(number <= UART_NUM);
	}

    //==============================================================
    //  发送
    //==============================================================
    
    void uart_is_txbusy(int number)
	{
		C_ASSERT(number <= UART_NUM);
	}

    void uart_write_byte(int number, uint8_t dat)
	{
		C_ASSERT(number <= UART_NUM);
	}

    /**
     * @brief 阻塞式发送
     * 
     * @param number 
     * @param array 
     * @param len 
     */
    void uart_write(int number, uint8_t* array, int len)
	{
		C_ASSERT(number <= UART_NUM);
		USART_TypeDef* p = (USART_TypeDef*)uartList[number];
		for(int i = 0;i < len;i ++)
		{
			p->DR = array[i];
			while((uartList[number]->SR&0X40)==0);	
		}
	}
    
	
	void uart_write_dma(int number, uint8_t* array, int len)
	{
		
		//设置DMA的Memory地址
		DMA_Stream_TypeDef* stream = ListTxDma[number].stream;
		stream -> PAR = (uint32_t)array;
		//使能DMA_Stream
		DMA_Cmd(stream, ENABLE);                       //开启DMA传输 
	}
	
    /**
     * @brief 阻塞式发送字符串
     * 
     * @param number 
     * @param str 
     * @param ... 
     */
    void uart_printf(int number, char* str ,...);


    /**
     * @brief 触发字节发送
     * 
     * @param number 
     * @param dat 
     */
    void uart_write_byte_async(int number, uint8_t dat);
	
    /**
     * @brief 触发数组发送
     * 
     * @param number 
     * @param array 
     * @param len 
     */
    void uart_write_async(int number, uint8_t* array, int len);

    /**
     * @brief 触发字符串发送
     * 
     * @param number 
     * @param str 
     * @param ... 
     */
    void uart_printf_async(int number, char* str ,...);
    


    //==============================================================
    //  接收
    //==============================================================

    /**
     * @brief 读字节
     * 
     * @param number 
     * @param timeout 
     * @return uint8_t 
     */
    uint8_t uart_read_byte(int number, int timeout);

    /**
     * @brief 读整帧数据
     * 
     * @param number 
     * @param timeout 
     * @return RxBuff* 
     */
    RxBuff* uart_read(int number ,int timeout);




