#include "stm32f4xx.h"
#include "board.h"


static void SetSysClock(void)
{
/******************************************************************************/
/*            PLL (clocked by HSE) used as System clock source                */
/******************************************************************************/
  __IO uint32_t StartUpCounter = 0, HSEStatus = 0;
  
  /* Enable HSE */
  RCC->CR |= ((uint32_t)RCC_CR_HSEON);
 
  /* Wait till HSE is ready and if Time out is reached exit */
  do
  {
    HSEStatus = RCC->CR & RCC_CR_HSERDY;
    StartUpCounter++;
  } while((HSEStatus == 0) && (StartUpCounter != HSE_STARTUP_TIMEOUT));

  if ((RCC->CR & RCC_CR_HSERDY) != RESET)
  {
    HSEStatus = (uint32_t)0x01;
  }
  else
  {
    HSEStatus = (uint32_t)0x00;
  }

  if (HSEStatus == (uint32_t)0x01)
  {
    /* Select regulator voltage output Scale 1 mode */
    RCC->APB1ENR |= RCC_APB1ENR_PWREN;
    PWR->CR |= PWR_CR_VOS;

    /* HCLK = SYSCLK / 1*/
    RCC->CFGR |= RCC_CFGR_HPRE_DIV1;

#if defined (STM32F40_41xxx) || defined (STM32F427_437xx) || defined (STM32F429_439xx)      
    /* PCLK2 = HCLK / 2*/
    RCC->CFGR |= RCC_CFGR_PPRE2_DIV2;
    
    /* PCLK1 = HCLK / 4*/
    RCC->CFGR |= RCC_CFGR_PPRE1_DIV4;
#endif /* STM32F40_41xxx || STM32F427_437x || STM32F429_439xx */

#if defined (STM32F401xx)
    /* PCLK2 = HCLK / 2*/
    RCC->CFGR |= RCC_CFGR_PPRE2_DIV1;
    
    /* PCLK1 = HCLK / 4*/
    RCC->CFGR |= RCC_CFGR_PPRE1_DIV2;
#endif /* STM32F401xx */
   
    /* Configure the main PLL */
    RCC->PLLCFGR = PLL_M | (PLL_N << 6) | (((PLL_P >> 1) -1) << 16) |
                   (RCC_PLLCFGR_PLLSRC_HSE) | (PLL_Q << 24);

    /* Enable the main PLL */
    RCC->CR |= RCC_CR_PLLON;

    /* Wait till the main PLL is ready */
    while((RCC->CR & RCC_CR_PLLRDY) == 0)
    {
    }
   
#if defined (STM32F427_437xx) || defined (STM32F429_439xx)
    /* Enable the Over-drive to extend the clock frequency to 180 Mhz */
    PWR->CR |= PWR_CR_ODEN;
    while((PWR->CSR & PWR_CSR_ODRDY) == 0)
    {
    }
    PWR->CR |= PWR_CR_ODSWEN;
    while((PWR->CSR & PWR_CSR_ODSWRDY) == 0)
    {
    }      
    /* Configure Flash prefetch, Instruction cache, Data cache and wait state */
    FLASH->ACR = FLASH_ACR_PRFTEN | FLASH_ACR_ICEN |FLASH_ACR_DCEN |FLASH_ACR_LATENCY_5WS;
#endif /* STM32F427_437x || STM32F429_439xx  */

#if defined (STM32F40_41xxx)     
    /* Configure Flash prefetch, Instruction cache, Data cache and wait state */
    FLASH->ACR = FLASH_ACR_PRFTEN | FLASH_ACR_ICEN |FLASH_ACR_DCEN |FLASH_ACR_LATENCY_5WS;
#endif /* STM32F40_41xxx  */

#if defined (STM32F401xx)
    /* Configure Flash prefetch, Instruction cache, Data cache and wait state */
    FLASH->ACR = FLASH_ACR_PRFTEN | FLASH_ACR_ICEN |FLASH_ACR_DCEN |FLASH_ACR_LATENCY_2WS;
#endif /* STM32F401xx */

    /* Select the main PLL as system clock source */
    RCC->CFGR &= (uint32_t)((uint32_t)~(RCC_CFGR_SW));
    RCC->CFGR |= RCC_CFGR_SW_PLL;

    /* Wait till the main PLL is used as system clock source */
    while ((RCC->CFGR & (uint32_t)RCC_CFGR_SWS ) != RCC_CFGR_SWS_PLL);
  }
  else
  { /* If HSE fails to start-up, the application will have wrong clock
         configuration. User can add here some code to deal with this error */
  }
}

volatile uint32_t sysclk = 0;
void mcu_clk_init()
{
	SetSysClock();
	SystemCoreClockUpdate();
	sysclk = SystemCoreClock;
}


  
void RCC_Enable_GPIOA()
{
	RCC_AHB1PeriphClockCmd( RCC_AHB1Periph_GPIOA, ENABLE);
}
void RCC_Enable_GPIOB()
{
	RCC_AHB1PeriphClockCmd( RCC_AHB1Periph_GPIOB, ENABLE);
}

void RCC_Enable_GPIOC()
{
	RCC_AHB1PeriphClockCmd( RCC_AHB1Periph_GPIOC, ENABLE);
}

void RCC_Enable_GPIOD()
{
	RCC_AHB1PeriphClockCmd( RCC_AHB1Periph_GPIOD, ENABLE);
}

void RCC_Enable_GPIOE()
{
	RCC_AHB1PeriphClockCmd( RCC_AHB1Periph_GPIOE, ENABLE);
}

void RCC_Enable_GPIOF()
{
	RCC_AHB1PeriphClockCmd( RCC_AHB1Periph_GPIOF, ENABLE);
}

void RCC_Enable_GPIOG()
{
	RCC_AHB1PeriphClockCmd( RCC_AHB1Periph_GPIOG, ENABLE);
}

void RCC_Enable_GPIOI()
{
	RCC_AHB1PeriphClockCmd( RCC_AHB1Periph_GPIOI, ENABLE);
}

void RCC_Enable_GPIOJ()
{
	RCC_AHB1PeriphClockCmd( RCC_AHB1Periph_GPIOJ, ENABLE);
}

void RCC_Enable_GPIOK()
{
	RCC_AHB1PeriphClockCmd( RCC_AHB1Periph_GPIOK, ENABLE);
}

void RCC_Enable_DMA1()
{
	RCC_AHB1PeriphClockCmd( RCC_AHB1Periph_DMA1, ENABLE);
}

void RCC_Enable_DMA2()
{
	RCC_AHB1PeriphClockCmd( RCC_AHB1Periph_DMA2, ENABLE);
}

void RCC_Enable_ETH()
{
	RCC_AHB1PeriphClockCmd( RCC_AHB1Periph_ETH_MAC, ENABLE);
	RCC_AHB1PeriphClockCmd( RCC_AHB1Periph_ETH_MAC_Tx, ENABLE);
	RCC_AHB1PeriphClockCmd( RCC_AHB1Periph_ETH_MAC_Rx, ENABLE);
	RCC_AHB1PeriphClockCmd( RCC_AHB1Periph_ETH_MAC_PTP, ENABLE);
}


void RCC_Enable_TIM2()
{
	RCC_APB1PeriphClockCmd( RCC_APB1Periph_TIM2 , ENABLE);
}
void RCC_Enable_TIM3()
{
	RCC_APB1PeriphClockCmd( RCC_APB1Periph_TIM3 , ENABLE);
}
void RCC_Enable_TIM4()
{
	RCC_APB1PeriphClockCmd( RCC_APB1Periph_TIM4 , ENABLE);
}
void RCC_Enable_TIM5()
{
	RCC_APB1PeriphClockCmd( RCC_APB1Periph_TIM5 , ENABLE);
}
void RCC_Enable_TIM6()
{
	RCC_APB1PeriphClockCmd( RCC_APB1Periph_TIM6 , ENABLE);
}
void RCC_Enable_TIM7()
{
	RCC_APB1PeriphClockCmd( RCC_APB1Periph_TIM7 , ENABLE);
}

void RCC_Enable_TIM12()
{
	RCC_APB1PeriphClockCmd( RCC_APB1Periph_TIM12 , ENABLE);
}

void RCC_Enable_TIM13()
{
	RCC_APB1PeriphClockCmd( RCC_APB1Periph_TIM13 , ENABLE);
}

void RCC_Enable_TIM14()
{
	RCC_APB1PeriphClockCmd( RCC_APB1Periph_TIM14 , ENABLE);
}


void RCC_Enable_wwdg()
{
	RCC_APB1PeriphClockCmd( RCC_APB1Periph_WWDG , ENABLE);
}


void RCC_Enable_SPI2()
{
	RCC_APB1PeriphClockCmd( RCC_APB1Periph_SPI2 , ENABLE);
}


void RCC_Enable_SPI3()
{
	RCC_APB1PeriphClockCmd( RCC_APB1Periph_SPI3 , ENABLE);
}


void RCC_Enable_USART2()
{
	RCC_APB1PeriphClockCmd( RCC_APB1Periph_USART2 , ENABLE);
}

void RCC_Enable_USART3()
{
	RCC_APB1PeriphClockCmd( RCC_APB1Periph_USART3 , ENABLE);
}

void RCC_Enable_UART4()
{
	RCC_APB1PeriphClockCmd( RCC_APB1Periph_UART4 , ENABLE);
}

void RCC_Enable_UART5()
{
	RCC_APB1PeriphClockCmd( RCC_APB1Periph_UART5 , ENABLE);
}

void RCC_Enable_I2C1()
{
	RCC_APB1PeriphClockCmd( RCC_APB1Periph_I2C1 , ENABLE);
}

void RCC_Enable_I2C2()
{
	RCC_APB1PeriphClockCmd( RCC_APB1Periph_I2C2 , ENABLE);
}

void RCC_Enable_I2C3()
{
	RCC_APB1PeriphClockCmd( RCC_APB1Periph_I2C3 , ENABLE);
}

void RCC_Enable_CAN1()
{
	RCC_APB1PeriphClockCmd( RCC_APB1Periph_CAN1 , ENABLE);
}

void RCC_Enable_CAN2()
{
	RCC_APB1PeriphClockCmd( RCC_APB1Periph_CAN2 , ENABLE);
}

void RCC_Enable_PWR()
{
	RCC_APB1PeriphClockCmd( RCC_APB1Periph_PWR , ENABLE);
}

void RCC_Enable_DAC()
{
	RCC_APB1PeriphClockCmd( RCC_APB1Periph_DAC , ENABLE);
}

void RCC_Enable_UART7()
{
	RCC_APB1PeriphClockCmd( RCC_APB1Periph_UART7 , ENABLE);
}

void RCC_Enable_UART8()
{
	RCC_APB1PeriphClockCmd( RCC_APB1Periph_UART8 , ENABLE);
}

void RCC_Enable_TIM8( )
{
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM8 ,ENABLE);
}

void RCC_Enable_USART1( )
{
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1 ,ENABLE);
}
void RCC_Enable_USART6( )
{
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART6 ,ENABLE);
}
void RCC_Enable_ADC1( )
{
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1 ,ENABLE);
}
void RCC_Enable_ADC2( )
{
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC2 ,ENABLE);
}
void RCC_Enable_ADC3( )
{
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC3 ,ENABLE);
}
void RCC_Enable_SDIO( )
{
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_SDIO ,ENABLE);
}

void RCC_Enable_TIM1( )
{
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM1 ,ENABLE);
}
void RCC_Enable_SPI1( )
{
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_SPI1 ,ENABLE);
}
void RCC_Enable_SPI4( )
{
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_SPI4 ,ENABLE);
}
void RCC_Enable_SYSCFG( )
{
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG ,ENABLE);
}
void RCC_Enable_TIM9( )
{
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM9 ,ENABLE);
}
void RCC_Enable_TIM10( )
{
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM10 ,ENABLE);
}
void RCC_Enable_TIM11( )
{
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM11 ,ENABLE);
}
void RCC_Enable_SPI5( )
{
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_SPI5 ,ENABLE);
}
void RCC_Enable_SPI6( )
{
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_SPI6 ,ENABLE);
}
void RCC_Enable_SAI( )
{
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_SAI1 ,ENABLE);
}
void RCC_Enable_LTDC( )
{
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_LTDC ,ENABLE);
}




