/**
 * @file mcu_usart_1.c
 * @author 林晓明 (lxiaoming@chint.com)
 * @brief 
 * @version 0.1
 * @date 2024-01-14
 * 
 * @copyright Copyright (c) 2024
 * 
 */

#include "stm32f4xx.h"
#include "frame.h"
#include "MyFifo.h"
#include "lib_usart.h"
#include "lib_dma.h"
#include "func_bit.h"
#include "mlibc_base.h"
#include "board.h"

#define UART_RX_MODE_FIFO       1
#define UART_RX_MODE_FRAME      2

//=====================================================================
//  UART1
//=====================================================================
#define uart1_ENABLE           1
#define uart1_IO				
#define uart1_ENABLE_TXDMA     1
#define uart1_ENABLE_RXDMA     1
#define uart1_RX_MODE          UART_RX_MODE_FRAME

//--------- DMA设置--------
#if uart1_ENABLE_TXDMA
	#define uart1_TXDMA			   DMA_02
    #define uart1_TXDMA_STREAM     DMA02_Stream07
    #define uart1_TXDMA_CHANNEL    7
#endif

#if uart1_ENABLE_RXDMA
	#define uart1_RXDMA			   DMA_02
    #define uart1_RXDMA_STREAM     DMA_Stream_02
    #define uart1_RXDMA_CHANNEL    7
#endif


//--------- RX设置 -------

#if uart1_RX_MODE == UART_RX_MODE_FIFO
	#if uart1_ENABLE_RXDMA
		#define UART1_FRAME_SIZE  512
	#endif
    #define UART1_FIFO_SIZE   1024
#endif

#if uart1_RX_MODE == UART_RX_MODE_FRAME
    #define UART1_FRAME_SIZE  512
    #define UART1_FRAME_NUM   2
#endif

//======================================================================
//	UART公共
//======================================================================



	
	
//=====================================================================
//
//=====================================================================
#if uart1_ENABLE
    #if uart1_RX_MODE == UART_RX_MODE_FIFO
		#if uart1_ENABLE_RXDMA
		uint8_t uart1_rxbuff[ UART1_FRAME_SIZE  * 1];
        TypeFrame uart1_rxframeList[1];
		#endif
        new_fifo(uart1_rxfifo , FIFO_SIZE);
    #endif
    #if uart1_RX_MODE == UART_RX_MODE_FRAME
        static uint8_t uart1_rxbuff[ UART1_FRAME_SIZE  * UART1_FRAME_NUM];
        static TypeFrame uart1_rxframeList[UART1_FRAME_NUM];
		static uint8_t uart1_wIndex = 0;
		static uint8_t uart1_rIndex = 0;
		static uint8_t uart1_full = 0;
		static bool uart1_txBusy = false;
    #endif
    //UART1中断与DMA配置
	void uart1_config2()
	{
		#if uart1_ENABLE_RXDMA	//开启了RX_DMA
			//缓冲初始化
			#if uart1_RX_MODE == UART_RX_MODE_FIFO
			for(int i=0;i<1;i++)
			{
				frame_init( &uart1_rxframeList[i], &uart1_rxbuff[UART1_FRAME_SIZE * i], 0,UART1_FRAME_SIZE );
			}
			#endif
			#if uart1_RX_MODE == UART_RX_MODE_FRAME
			for(int i=0;i<UART1_FRAME_NUM;i++)
			{
				frame_init( &uart1_rxframeList[i], &uart1_rxbuff[UART1_FRAME_SIZE * i], 0,UART1_FRAME_SIZE );
			}
			//DMA配置【单缓冲模式】
			usart_enable_rxDMA(Uart1, uart1_rxframeList[0].array, NULL, UART1_FRAME_SIZE);
			usart_enable_rxDMA_it(Uart1);
			//中断配置【发送中断 + 接收空闲中断】
			ClearBit( USART1->SR , 6);	//开串口中断前清理标志位，以防止异常的串口中断
			usart_interrupt_set(Uart1, UART_IT_IDEL | UART_IT_TX); 
			#endif
		#else
			//缓冲初始化
			#if uart1_RX_MODE == UART_RX_MODE_FIFO
			#endif
			#if uart1_RX_MODE == UART_RX_MODE_FRAME
			for(int i=0;i<UART1_FRAME_NUM;i++)
			{
				frame_init( &uart1_rxframeList[i], &uart1_rxbuff[UART1_FRAME_SIZE * i], 0,UART1_FRAME_SIZE );
			}
			//DMA配置【单缓冲模式】
			usart_enable_rxDMA(Uart1, uart1_rxframeList[0].array, NULL, UART1_FRAME_SIZE);
			usart_enable_rxDMA_it(Uart1);
			//中断配置【发送中断 + 接收空闲中断】
			ClearBit( USART1->SR , 6);		//开串口中断前清理标志位，以防止异常的串口中断
			usart_interrupt_set(Uart1, UART_IT_RX | UART_IT_TX); 
			#endif
		#endif
	}
	
	static void uart1_set_rxbuff(TypeFrame* frame)
	{
		usart_enable_rxDMA(Uart1, frame->array, NULL, frame->size);
	}

	//--- 重启RX DMA，参数n：DMA接收完成时，剩余的空间 ---
	static void uart1_restart_rx_dma(int n)
	{
		usart_disable_rxDMA( Uart1 );
		dma_clear_it_all_flag(DMA_02,	DMA_Stream_02 );
		uart1_rxframeList[ uart1_wIndex ].len = uart1_rxframeList[ uart1_wIndex ].size - n;
		uart1_wIndex++;
		if(uart1_wIndex >= UART1_FRAME_NUM)
		{
			uart1_wIndex = 0;
		}
		if(uart1_rxframeList[uart1_wIndex].len == 0)	{ 
			uart1_set_rxbuff(&uart1_rxframeList[uart1_wIndex]); 
		}
		else							
		{ 	//无空闲缓冲
			uart1_full = true; 
			// log_inf( 0 ,"rx buff full\r\n");
		}
	}
    
	
	
	//UART1中断
    void USART1_IRQHandler(void)
	{
		int tmp = USART1->SR;
		volatile uint8_t rxData;
		if(tmp &(1<<5))//接收到数据
		{
			rxData = USART1->DR;
		}
		if(tmp &(1<<4))//空闲
		{
			rxData = USART1->DR;
			ClearBit(USART1->SR , 4);
			//log_inf( 0 ,"uart Rx over\r\n");
			int n = usart_read_dma_ndtr(Uart1);
			uart1_restart_rx_dma(n);
			
		}
		if(tmp &(1<<6))//发送完成
		{
			ClearBit( USART1->SR , 6); 
			uart1_txBusy = false;
		}
	}
	/**
	 * @brief uart1-DMA 中断
	 * 
	 */

	//设定TxDMA 回调
	void uart1_txdma_callback()
	{
		//log_inf( 0 ,"dma tx over \r\n");
	}
	uart_DMAHandle(uart1,2,7,uart1_txdma_callback)

	//设定RxDMA 回调
	void uart1_rxdma_callback()
	{
		// log_inf( 0 ,"dma rx full \r\n");
		uart1_restart_rx_dma(0);	//DMA满时，剩余 空间为0
	}
	uart_DMAHandle(uart1,2,2,uart1_rxdma_callback)
	//=====================================================================================================
	//	发送API
	//=====================================================================================================
	//------- 阻塞式发送array -----
	void uart1_put_byte( uint8_t x)
	{
		usart_put_array_dma( Uart1 , &x, 1);
		uart1_txBusy = true;
		while( uart1_txBusy );
	}
	void uart1_put_array( uint8_t* array, uint16_t len)
	{
		usart_put_array_dma( Uart1 , array, len);
		uart1_txBusy = true;
		while( uart1_txBusy );
	}
	//等待发送完成
	void uart1_wait_txfree()
	{
		while( uart1_txBusy );
	}
	//触发dma发送
	void uart1_put_array_start( uint8_t* array, uint16_t len )
	{
		uart1_txBusy = true;
		usart_put_array_dma( Uart1 , array, len);
	}
	//========================================================================================
	//	接收API
	//========================================================================================
	TypeFrame* uart1_read()
	{
		int tmp =  uart1_rIndex;//BuffReadIndex
		if(	uart1_rIndex==uart1_wIndex && 
			uart1_full==false)
		{
			return NULL;
		}
		uart1_rIndex ++;
		if(uart1_rIndex >= UART1_FRAME_NUM)
		{
			uart1_rIndex = 0;
		}
		return &uart1_rxframeList[tmp];
	}
	void  uart1_clear_rxframe(TypeFrame* buffx)
	{
		if(uart1_full)
		{
			uart1_full = false;
			uart1_set_rxbuff( buffx);
		}
		buffx->len = 0;
	}
	
	void  uart1_clear_allbuff()
	{
		if(uart1_full)
		{
			uart1_full = false;
			// log_inf( 0 ,"Clear\r\n");
			uart1_set_rxbuff( &uart1_rxframeList[0]);
		}
		for(int i=0;i<UART1_FRAME_NUM;i++)
		{
			frame_reset(&uart1_rxframeList[i]);
		}
	}
	
//=============================================================================================
//	初始化
//=============================================================================================
	void uart1_init(	Enum_io_usart 		io , 			//io配置
						uint32_t 			bandrate, 		//波特率
						Enum_Parity 		parity)			//校验位
	{
		usart_rcc_enable(Uart1);                		//复位 + 使能模块时钟
        usart_select_io( io ); 							//选择IO
        usart_config_port(Uart1 , bandrate , parity);	//设置波特率，偶校验
		usart_start(Uart1);
		usart_enable_txDMA(Uart1);
		usart_enable_txDMA_it(Uart1);	
		uart1_config2();
	}	
	
	
	
#endif

	




