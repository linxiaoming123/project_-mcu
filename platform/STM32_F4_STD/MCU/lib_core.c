
	#include "stm32f407xx.h"
	#include "core_cm4.h"
	#include "lib_core.h"

//================================================================================================
//	中断配置
//================================================================================================
	
	
	/**
	 * @brief (stack)采用如下方法实现执行汇编指令WFI (THUMB指令不支持汇编内联)
	 * 
	 */
	static void WFI_SET(void)
	{
		__ASM volatile("wfi");		  
	}

	/**
	 * @brief 关闭所有中断(但是不包括fault和NMI中断)
	 * 
	 */
	void disable_all_interrupt(void)
	{
		__ASM volatile("cpsid i");
	}

	/**
	 * @brief 允许所有中断
	 * 
	 */
	void enable_all_interrupt(void)
	{
		__ASM volatile("cpsie i");		  
	}

	/**
	 * @brief 设置中断向量表的偏移地址
	 * 
	 * @param NVIC_VectTab  : 基址
	 * @param Offset 		: 偏移量	
	 */
	void nvic_set_vectorTable(uint32_t NVIC_VectTab,uint32_t Offset)	 
	{ 	   	  
		//设置NVIC的向量表偏移寄存器,VTOR低9位保留,即[8:0]保留。
		SCB -> VTOR = NVIC_VectTab | (Offset&(uint32_t)0xFFFFFE00);	
	}

	/**
	 * @brief (stack)设置NVIC分组
	 * 
	 * @param NVIC_Group: NVIC分组 0~4 总共5组 	
	 */
	static void nvic_set_priority_group(uint8_t NVIC_Group)	 
	{ 
		uint32_t temp,temp1;	  
		temp1=(~NVIC_Group)&0x07;	//取后三位
		temp1<<=8;
		temp=SCB->AIRCR;  			//读取先前的设置
		temp &= 0X0000F8FF; 		//清空先前分组
		temp |= 0X05FA0000; 		//写入钥匙
		temp |= temp1;	   
		SCB -> AIRCR = temp;  		//设置分组	    	  				   
	}

	/**
	 * @brief 设置NVIC 
	 * 
	 * @param NVIC_PreemptionPriority 	抢占优先级
	 * @param NVIC_SubPriority 			响应优先级
	 * @param NVIC_Channel 				中断编号
	 * @param NVIC_Group 				优先级bit分配(推荐设置为2)
	 */
	void nvic_init(	uint8_t NVIC_PreemptionPriority,	//抢占优先级 (数值越小,越优先)
						uint8_t NVIC_SubPriority,			//响应优先级 (数值越小,越优先)
						uint8_t NVIC_Channel,				//中断编号
						uint8_t NVIC_Group					//优先级bit分配(推荐设置为2)
															//____________________________________________
															// value	|	抢占优先级位数 | 响应优先级位数 |
															//  0		|	0			 |	4			 |
															//  1		|	1			 |	3			 |
															//  2		|	2			 |	2			 |
															//  3		|	3			 |	1			 |	
															//  4		|	4			 |	0			 |
															//__________|________________|_______________|
					)	 
	{ 
		uint32_t temp;	  
		nvic_set_priority_group(NVIC_Group);//设置分组
		temp=NVIC_PreemptionPriority<<(4-NVIC_Group);	  
		temp|=NVIC_SubPriority&(0x0f>>NVIC_Group);
		temp&=0xf;								//取低四位
		NVIC->ISER[NVIC_Channel/32]|=1<<NVIC_Channel%32;//使能中断位(要清除的话,设置ICER对应位为1即可)
		NVIC->IP[NVIC_Channel]|=temp<<4;				//设置响应优先级和抢断优先级   	    	  				   
	} 

	
	
	void nvic_enable_irq(IRQn_Type IRQn)
	{
		/*  NVIC->ISER[((uint32_t)(IRQn) >> 5)] = (1 << ((uint32_t)(IRQn) & 0x1F));  enable interrupt */
		  NVIC->ISER[(uint32_t)((int32_t)IRQn) >> 5] = (uint32_t)(1 << ((uint32_t)((int32_t)IRQn) & (uint32_t)0x1F)); /* enable interrupt */
	}

	void nvic_disable_irq( IRQn_Type IRQn )
	{
		NVIC_DisableIRQ( IRQn );
	}
//================================================================================================
//	 	系统API，堆栈设置，休眠，软复位
//================================================================================================

	//设置栈顶地址
	//addr:栈顶地址
//	__asm void MSR_MSP(uint32_t addr) 
//	{
//		MSR MSP, r0 			//set Main Stack value
//		BX r14
//	}
	/**
	 * @brief 进入待机模式
	 * 
	 */
	void Sys_Standby(void)
	{ 
		SCB->SCR|=1<<2;		//使能SLEEPDEEP位 (SYS->CTRL)	   
		RCC->APB1ENR|=1<<28;//使能电源时钟 
		PWR->CSR|=1<<8;     //设置WKUP用于唤醒
		PWR->CR|=1<<2;      //清除Wake-up 标志
		PWR->CR|=1<<1;      //PDDS置位   	
		WFI_SET();			//执行WFI指令,进入待机模式		 
	}	     
	/**
	 * @brief 软复位
	 * 
	 */
	void Sys_Soft_Reset(void)
	{   
		SCB->AIRCR = 0X05FA0000 | (uint32_t)0x04;	  
	} 		 
