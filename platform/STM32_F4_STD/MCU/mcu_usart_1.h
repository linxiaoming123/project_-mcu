/**
 * @file mcu_usart_1.h
 * @author 林晓明 (lxiaoming@chint.com)
 * @brief 
 * @version 0.1
 * @date 2024-01-12
 * 
 * @copyright Copyright (c) 2024
 * 
 */

#ifdef __cplusplus
extern "C" {
#endif 

#ifndef mcu_usart_1_h
#define mcu_usart_1_h

//============================================================================================
//      包含头文件
//============================================================================================
    #include "stdint.h"
    #include "stdbool.h"
    #include "frame.h"
    #include "MyFiFo.h"
//============================================================================================
//      宏定义
//============================================================================================


//============================================================================================
//  枚举
//============================================================================================
 
    //UART编号
    typedef enum{
        Uart1 = 0,
        Uart2,
        Uart3,
        Uart4,
        Uart5,
        Uart6
    }UART_Number;

    typedef enum{
        Parity_none,    //无校验
        Parity_even,    //偶校验
        Parity_odd      //奇校验
    }Enum_Parity;

    
    typedef enum{
        USART1_PA9_PA10,    //usart1: PA9, PA10
		USART2_PA2_PA3,		//USART2: PA2, PA3
    }Enum_io_usart;

//    typedef enum{

//    }Enum_dma_rx;

//    typedef enum{

//    }Enum_dma_tx;

//============================================================================================
//      对外数据类型定义
//============================================================================================

    typedef struct{
        uint8_t number;
        uint8_t io;
        uint8_t parity;
        uint8_t e_txdma;
        uint8_t e_rxdma;
        uint32_t baudrate;
        uint8_t isBusyTx;
        uint8_t isBusyRx;
        uint8_t rxmode1;    //DMA ? IT
        uint8_t rxmode2;    //Frame ? fifo
    }TypeUART;
    
//============================================================================================
//      全局变量对外声明
//============================================================================================
//============================================================================================
//      函数对外声明
//============================================================================================
//    //UART IO 复用配置
//    int uart_set_io(    UART_Number uart, 
//                        Enum_io_usart io    );
//    //UART属性配置
//    int uart_config(    UART_Number uart, 
//                        int baudrate, 
//                        Enum_Parity parity);
//    //UART使能TXDMA
//    void uart_enable_txDMA(     UART_Number uart, 
//                                Enum_dma_tx dma);
//    
//    //发送API
//    void uart_put_byte(         UART_Number uart, uint8_t value);

//    void uart_put_array(        UART_Number uart,  
//                                uint8_t* array,
//                                uint16_t len);
//    void uart_put_array_dma(    UART_Number uart, 
//                                uint8_t* array, 
//                                uint16_t len);

//    bool uart_is_txbusy(        UART_Number uart );



//    //UART使能DMA
//    int uart_set_rx_dma(        UART_Number uart,      
//                                Enum_dma_rx      dmaSelect, 
//                                void(*callback)(UART_Number uart,uint8_t*arr, int len));

//    //UART使能接收中断
//    int uart_set_rx_it(         UART_Number uart,  
//                                void(*callback)(UART_Number uart,uint8_t*arr, int len));
//    //是否接收忙
//    bool uart_is_rxbusy(        UART_Number uart);
//    //UART设置接收模式为fifo模式
//    int uart_set_rxmode_fifo(   UART_Number uart,
//                                void* fifo );

//    void* uart_read_fifo(UART_Number uart);
//    //UART设置接收模式为frame模式【接收->超时->frame】
//    int uart_set_rxmode_frame(  UART_Number uart, 
//                                void* frame, 
//                                int num_frame,
//                                int rxtimeoud);
//    void uart_rx_timeout(       UART_Number uart );
//    void* uart_read_frame(      UART_Number uart);    


	//初始化
	void uart1_init(	Enum_io_usart 		io , 			//io配置
						uint32_t 			bandrate, 		//波特率
						Enum_Parity 		parity);		//校验位
	
	//触发dma发送
	void uart1_put_array_start( uint8_t* array, uint16_t len );
	//发送数据，直到结束
	void uart1_put_array( uint8_t* array, uint16_t len);
	//读取接收帧
	TypeFrame* uart1_read();
	//清理接收帧
	void  uart1_clear_rxframe(TypeFrame* buffx);
	
	
//============================================================================================
//  END
//============================================================================================

#endif

#ifdef __cplusplus
}
#endif 




