
#ifndef mcu_h
#define mcu_h

	#include "stdint.h"
	//==============================================================
	//	RCC与时钟
	//==============================================================
    extern void mcu_clk_init(void);;


    extern void RCC_Enable_GPIOA(void);
    extern void RCC_Enable_GPIOB(void);
    extern void RCC_Enable_GPIOC(void);
    extern void RCC_Enable_GPIOD(void);
    extern void RCC_Enable_GPIOE(void);
    extern void RCC_Enable_GPIOF(void);
    extern void RCC_Enable_GPIOG(void);
    extern void RCC_Enable_GPIOI(void);
    extern void RCC_Enable_GPIOJ(void);
    extern void RCC_Enable_GPIOK(void);
    extern void RCC_Enable_DMA1(void);
    extern void RCC_Enable_DMA2(void);
    extern void RCC_Enable_ETH(void);
    extern void RCC_Enable_TIM2(void);
    extern void RCC_Enable_TIM3(void);
    extern void RCC_Enable_TIM4(void);
    extern void RCC_Enable_TIM5(void);
    extern void RCC_Enable_TIM6(void);
    extern void RCC_Enable_TIM7(void);
    extern void RCC_Enable_TIM12(void);
    extern void RCC_Enable_TIM13(void);
    extern void RCC_Enable_TIM14(void);
    extern void RCC_Enable_wwdg(void);
    extern void RCC_Enable_SPI2(void);
    extern void RCC_Enable_SPI3(void);
    extern void RCC_Enable_USART2(void);
    extern void RCC_Enable_USART3(void);
    extern void RCC_Enable_UART4(void);
    extern void RCC_Enable_UART5(void);
    extern void RCC_Enable_I2C1(void);
    extern void RCC_Enable_I2C2(void);
    extern void RCC_Enable_I2C3(void);
    extern void RCC_Enable_CAN1(void);
    extern void RCC_Enable_CAN2(void);
    extern void RCC_Enable_PWR(void);
    extern void RCC_Enable_DAC(void);
    extern void RCC_Enable_UART7(void);
    extern void RCC_Enable_UART8(void);

	
    extern void RCC_Enable_TIM8(void);
    extern void RCC_Enable_USART1(void);
    extern void RCC_Enable_USART6(void);
    extern void RCC_Enable_ADC1(void);
    extern void RCC_Enable_ADC2(void);
    extern void RCC_Enable_ADC3(void);
    extern void RCC_Enable_SDIO(void);
    extern void RCC_Enable_TIM1(void);
    extern void RCC_Enable_SPI1(void);
    extern void RCC_Enable_SPI4(void);
    extern void RCC_Enable_SYSCFG(void);
    extern void RCC_Enable_TIM9(void);
    extern void RCC_Enable_TIM10(void);
    extern void RCC_Enable_TIM11(void);
    extern void RCC_Enable_SPI5(void);
    extern void RCC_Enable_SPI6(void);
    extern void RCC_Enable_SAI(void);
    extern void RCC_Enable_LTDC(void);
	
	//==============================================================
	//	DMA
	//==============================================================


	//==============================================================
	//	GPIO
	//==============================================================
	#include "i_gpio.h"

	//==============================================================
	//	UART
	//==============================================================
//	#include "lib_usart.h"
//	void usart_init(Enum_UART_Number 	uartn,			//UART编号 
//					Enum_io_usart 		io , 			//io配置
//					uint32_t 			bandrate, 		//波特率
//					Enum_Parity 		parity);		//校验位
//	
//	
//	
//	#define BUFF_LEN	1024
//	#define BUFF_NUM	2
//	
//	
//	typedef struct{
//		int len;
//		uint8_t array[BUFF_LEN];
//	}TypeBuff;
//	//-------- 发送 -------------
//	//阻塞式发送byte
//	void uart_1_put_byte( uint8_t x);
//	//阻塞式发送array
//	void uart_1_put_array( uint8_t* array, uint16_t len);
//	//等待发送完成
//	void uart_1_wait_txfree(void);
//	//开始发送array【触发dma发送】
//	void uart_1_put_array_start( uint8_t* array, uint16_t len );
//	//-------- 接收 -------------
//	TypeBuff* uart_1_read(void);
//	void  uart_1_clear_rxframe(TypeBuff* buffx);
//	void  uart_1_clear_allbuff(void);
	
	
	//SPI2
	#include "i_spi.h"
	extern TypeSPI spi2;
	
	


#endif
