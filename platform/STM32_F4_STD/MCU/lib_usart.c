/**
 * @file usart.c
 * @author your name (you@domain.com)
 * @brief  stm32的串口库
 * @version 0.1
 * @date 2021-10-10
 * 
 * @copyright Copyright (c) 2021
 * 
 */
    #include "stdint.h"	  
    #include "lib_gpio.h"
	#include "lib_core.h"
    #include "lib_clk.h"
    #include "stdio.h"
    #include "func_bit.h"
	#include "lib_usart.h"
	#ifndef Bool
		#define  Bool uint8_t
	#endif
//=============================================================================
//  寄存器地址：
//=============================================================================
    //模块基地址
    static  USART_TypeDef*  const usart_addr_list[] = {
        USART1, USART2 ,USART3, UART4, UART5, USART6
    };
	
    //中断编号
    static const IRQn_Type   usart_irq_list[] = {
        USART1_IRQn , USART2_IRQn, USART3_IRQn, UART4_IRQn, UART5_IRQn,USART6_IRQn
    };
    
    //DR地址
	static const __IO uint16_t*   usart_DR[] = {
        (const __IO uint16_t* )&(USART1->DR),
        (const __IO uint16_t* )&(USART2->DR),
        (const __IO uint16_t* )&(USART3->DR),
        (const __IO uint16_t* )&(UART4->DR),
        (const __IO uint16_t* )&(UART5->DR),
        (const __IO uint16_t* )&(USART6->DR)
    };

//=============================================================================
//  子API
//=============================================================================
    //复位串口n(1--6)
    void usart_reset( Enum_UART_Number n)
    {
        switch(n)
        {
            case Uart1: //USART1, RCC_APB2RSTR，bit4
                SetBit( RCC->APB2RSTR , 4); 
                break;
            case Uart2: //USART2，RCC_APB1RSTR，bit 17
                SetBit( RCC->APB1RSTR , 17); 
                break;
            case Uart3: //USART2，RCC_APB1RSTR，bit 18
                SetBit( RCC->APB1RSTR , 18); 
                break;
            case Uart4: //USART2，RCC_APB1RSTR，bit 19
                SetBit( RCC->APB1RSTR , 19); 
                break;
            case Uart5: //USART2，RCC_APB1RSTR，bit 20
                SetBit( RCC->APB1RSTR , 20); 
                break;
            case Uart6: //USART6, RCC_APB2RSTR，bit5
                SetBit( RCC->APB2RSTR , 5); 
                break;
        }
    }
    
    //  USARTn时钟使能(n:1--6)
    void usart_rcc_enable( Enum_UART_Number n)
    {
        //使能外设时钟
        switch(n)
        {
            case Uart1: //USART1, RCC_APB2ENR，bit4
                SetBit( RCC->APB2ENR , 4); 
                break;
            case Uart2: //USART2，RCC_APB1ENR，bit 17
                SetBit( RCC->APB1ENR , 17); 
                break;
            case Uart3: //USART2，RCC_APB1ENR，bit 18
                SetBit( RCC->APB1ENR , 18); 
                break;
            case Uart4: //USART2，RCC_APB1ENR，bit 19
                SetBit( RCC->APB1ENR , 19); 
                break;
            case Uart5: //USART2，RCC_APB1ENR，bit 20
                SetBit( RCC->APB1ENR , 20); 
                break;
            case Uart6: //USART6, RCC_APB2ENR，bit5
                SetBit( RCC->APB2ENR , 5); 
                break;
        }
    }

    //  USARTn时钟关闭(n:1--6)
    void usart_rcc_disable( Enum_UART_Number n)
    {
        //禁用外设时钟
        switch(n)
        {
            case Uart1: //USART1, RCC_APB2ENR，bit4
                ClearBit( RCC->APB2ENR , 4); 
                break;
            case Uart2: //USART2，RCC_APB1ENR，bit 17
                ClearBit( RCC->APB1ENR , 17); 
                break;
            case Uart3: //USART2，RCC_APB1ENR，bit 18
                ClearBit( RCC->APB1ENR , 18); 
                break;
            case Uart4: //USART2，RCC_APB1ENR，bit 19
                ClearBit( RCC->APB1ENR , 19); 
                break;
            case Uart5: //USART2，RCC_APB1ENR，bit 20
                ClearBit( RCC->APB1ENR , 20); 
                break;
            case Uart6: //USART6, RCC_APB2ENR，bit5
                ClearBit( RCC->APB2ENR , 5); 
                break;
        }
    }

    //  设置IO口 -- usart的绑定关系
    void usart_select_io(Enum_io_usart x )
    {
        switch(x)
        {
            case USART1_PA9_PA10:
                SetBit(RCC->AHB1ENR, 0);                //使能PORTA口时钟 (RCC_AHB1ENR , BIT0)
                GPIO_Set(   GPIOA,                      //PA
                            PIN9|PIN10,                 //PA9 , PA10
                            GPIO_MODE_AF,               //复用功能
                            GPIO_OTYPE_PP,
                            GPIO_SPEED_50M,             
                            GPIO_PUPD_PU);             
                GPIO_AF_Set(GPIOA,9,7);					//PA9,AF7
                GPIO_AF_Set(GPIOA,10,7);				//PA10,AF7 
                break;
			case USART2_PA2_PA3:
				SetBit(RCC->AHB1ENR, 0);                //使能PORTA口时钟 (RCC_AHB1ENR , BIT0)
                GPIO_Set(   GPIOA,                      //PA
                            PIN2|PIN3,                 //PA9 , PA10
                            GPIO_MODE_AF,               //复用功能
                            GPIO_OTYPE_PP,
                            GPIO_SPEED_50M,             
                            GPIO_PUPD_PU);             
                GPIO_AF_Set(GPIOA,2,7);					//PA2,AF7
                GPIO_AF_Set(GPIOA,3,7);				//PA3,AF7 
				break;
				
        }					
        
    }
    
    //  设置usart的波特率、校验位
    void usart_config_port(Enum_UART_Number n ,int boundrate ,Enum_Parity parity)
    {
        float temp;
		float temp2 = boundrate*16;
        uint16_t mantissa;
        uint16_t fraction;	 
        //时钟频率相关计算
        temp=(float)(PCLK2) ;
		temp= temp / temp2;	//得到USARTDIV@OVER8=0
        mantissa = temp;				 		//得到整数部分
        fraction = (temp-mantissa)*16; 			//得到小数部分@OVER8=0 
        mantissa <<= 4;
        mantissa +=fraction; 
        //写波特率寄存器
        usart_addr_list[n]->BRR = mantissa;
        //写校验位
        switch(parity)
        {
            case Parity_none: //无校验
                ClearBit( usart_addr_list[n]->CR1 , 12);      //8bit数据位
                SetBits( usart_addr_list[n]->CR1 , 9, 10, 0); //禁用奇偶校验
                break;
            case Parity_even: //偶校验
                SetBit( usart_addr_list[n]->CR1 , 12);        //9bit数据位
                SetBits( usart_addr_list[n]->CR1 , 9, 10, 2); //偶校验
                break;
            case Parity_odd: //奇校验
                SetBit( usart_addr_list[n]->CR1 , 12);        //9bit数据位
                SetBits( usart_addr_list[n]->CR1 , 9, 10, 3); //奇校验
                break;
        }
    }

    //  设置中断
    void usart_interrupt_set(Enum_UART_Number n, uint8_t IT)        //中断
    {
        SetBits( usart_addr_list[n]->CR1 , 4, 6, IT);
        if( IT )
        {
            nvic_enable_irq( usart_irq_list[n] );
        }
        else
        {
			
        }
    }

    //uart启动
    void usart_start(Enum_UART_Number n)
    {
        SetBit( usart_addr_list[n]->CR1 , 3);   //发送使能
        SetBit( usart_addr_list[n]->CR1 , 2);   //接收使能
        SetBit( usart_addr_list[n]->CR1 , 13);  //启动usart
		volatile int x = usart_addr_list[n]->SR; //先读取标志位再发送，否则第一个字节发送失败
    }


//---------------------------------------------------------------------
//	阻塞式发送
//---------------------------------------------------------------------	
    void usart_put_byte( Enum_UART_Number n, uint8_t x)
	{
		USART_TypeDef* p = (USART_TypeDef*)usart_addr_list[n];
		p->DR = x;
		while(( usart_addr_list[n]->SR & 0X40) == 0);
	}
//---------------------------------------------------------------------
//	DMA发送
//---------------------------------------------------------------------	
	//  DMA设置 DMA2,数据流7，通道4
	#include "lib_dma.h"
	#include "stm32f4xx.h"
	static void  template_config_uart_TxDma(   Enum_DMAnumber dma,
                                        Enum_DmaStream n,
                                        uint8_t channel)
	{
        
        Register_DMA_config config ;
        Register_DMA_SxFCR config_fifo;
        { 	//这里的大括号仅用于折叠代码
            //设置CR
            config . DMAEnable          =0; //DMA使能，在即将发送的时候使能
            config . DMEIE              =0; //bit1          中断使能：直接模式错误中断使能
            config . enable_TEIE        =0; //bit2          中断使能：传输错误中断使能
            config . enable_HTIE        =0; //bit3          中断使能：半传输中断
            config . enable_TCIE        =1; //bit4       ★ 中断使能：传输完成中断
            config . EnableFlowCtrl	 	=0; //bit5          是否使能外设流控
            config . direction          =1; //bit6-7     ★ 数据传输方向
                                            //              0：外设   --> 存储器
                                            //              1：存储器 --> 外设
                                            //              2：存储器 --> 存储器
            config . isloopMode         =0; //bit8       ★ 是否循环模式
            config . isPeripheralInc    =0; //bit9          是否外设地址自增模式
            config . isMemoryInc        =1; //bit10         是否存储器地址自增模式
            config . psize              =0; //bit11-12      外设数据大小（位数）
                                            //              0     1       2       
                                            //              8位   16位    32位
            config . msize              =0; //bit13-14      存储数据大小
                                            //              0     1       2       
                                            //              8位   16位    32位
            config . pincos             =0; //bit15         为1时，固定偏移量为4（32位对齐），
                                            //              此时msize不起作用   
            config . DMA_priority       =0; //bit16-17      DMA优先级
                                            //              0	1	2	3
                                            //              低	中	高	非常高
            config . DBM          		=0; //bit18         双缓冲
            config . current_taget      =0; //bit19         当前使用哪个缓冲（针对双缓冲应用）
                                            //              0 ：缓冲0
                                            //              1 ：缓冲1
            config . Reserved0          =0; //bit20         预留       
            config . pburst             =0; //bit21-22      外设突发模式选择
                                            //              0	    1	    2	    4 
                                            //              单次	4个     8个     16个
            config . mburst             =0; //bit23-24      存储器突发模式选择
                                            //              0	    1	    2	    4 
                                            //              单次	4个     8个     16个
            config . channel_select=channel;//bit25-27   ★ 通道选择【0-7】

            
            //---------- fifo设置 --------
            
            config_fifo . FTH			 =0;// 阀值选择：
                                            //  00： FIFO 容量的 1/4?
                                            //	01： FIFO 容量的 1/2?
                                            //	10： FIFO 容量的 3/4?
                                            //	11： FIFO 完整容量
            config_fifo . DMDIS		    =1; // 直接模式使能
            config_fifo . FS			=0;	// FIFO 状态（直接模式下无意义）
                                            //	000  0/4  < fifo_level < 1/4 
                                            //	001  1/4  < fifo_level < 2/4 
                                            //	010  2/4  < fifo_level < 3/4 
                                            //	011  3/4  < fifo_level < 4/4 
                                            //	100  FIFO空
                                            //	101	 FIFO已满
            config_fifo . Reserved0	    =0; //
            config_fifo . FEIE		    =0; //	FIFO 错误中断使能

        }
        // 步骤1： 使能DMA时钟
        dma_enable_clk( dma );			       	

        // 步骤2：DMA开始配置
        dma_wait_for_config(    dma ,	//DMA编号
                                n,		//流编号
                                100);  //等待超时时间

        // 步骤3：DMA设置CR和fifo
        dma_config_stream_cr(   dma ,			//DMA编号
                                n,				//流编号
                                config);		//CR配置

        // 步骤4：DMA设置fifo
        dma_config_stream_fifo( dma ,		//DMA编号
                                n,			//流编号
                                config_fifo);//FIFO配置
        // // 步骤5：设置dma外设地址 -- 放到函数外实现
        // dma_set_peripheral_addr(	dma ,	//DMA编号
        //                             n,		//流编号
        //                             usart_DR[n]);			//外设地址	
        
	}

    
	//uart-TXDMA定义
    typedef struct{
        Enum_DMAnumber dma;
        Enum_DmaStream stream;
        uint8_t 	channel;
		IRQn_Type	IRQ_number;
    }TypeTxDMAconfig;

    static TypeTxDMAconfig  ListTxDma[] ={
        [Uart1] = {.dma = DMA_02 , .stream =  DMA_Stream_07 , .channel = 4 , .IRQ_number = DMA2_Stream7_IRQn},
        [Uart2] = {.dma = DMA_01 , .stream =  DMA_Stream_06 , .channel = 4 , .IRQ_number = DMA1_Stream6_IRQn},
    };

    void usart_enable_txDMA(Enum_UART_Number  n)
    {
		//使能usart-n 的DMA发送
		SetBit( usart_addr_list[n]->CR3 , 7);   //发送的DMA使能

        template_config_uart_TxDma( ListTxDma[n].dma, 
                                    ListTxDma[n].stream , 
                                    ListTxDma[n].channel);

        dma_set_peripheral_addr(	ListTxDma[n].dma,     		//
                                    ListTxDma[n].stream , 		//流编号
                                    (uint32_t)usart_DR[n]);		//外设地址
    }

	void usart_enable_txDMA_it(Enum_UART_Number  n)
	{
		NVIC_EnableIRQ(ListTxDma[n].IRQ_number);
	}
	
	void usart_put_array_dma(uint8_t n,uint8_t* array, uint16_t len)
	{			
		dma_set_memory_addr( ListTxDma[n].dma ,						//DMA buff设置
							 ListTxDma[n].stream,
							 (uint32_t)array ,
							 NULL,
							 len);
		dma_enable_stream( ListTxDma[n].dma ,ListTxDma[n].stream );	//使能DMA 
	}
	
//---------------------------------------------------------------------
//	接收DMA
//---------------------------------------------------------------------
    TypeTxDMAconfig const ListRxDma[] ={
        [Uart1] = {.dma = DMA_02 , .stream =  DMA_Stream_02 , .channel = 4 ,.IRQ_number= DMA2_Stream2_IRQn},
        [Uart2] = {.dma = DMA_01 , .stream =  DMA_Stream_05 , .channel = 4 ,.IRQ_number= DMA1_Stream5_IRQn},
    };
	
	void template_config_uart_RxDma(    Enum_UART_Number  n,
										uint8_t* buff0,
										uint8_t* buff1,
										uint16_t len)
    {
		Register_DMA_config config ;
        Register_DMA_SxFCR config_fifo;
        { 	//这里的大括号仅用于折叠代码
            //设置CR
            config . DMAEnable          =0; //DMA使能，在即将发送的时候使能
            config . DMEIE              =0; //bit1          中断使能：直接模式错误中断使能
            config . enable_TEIE        =0; //bit2          中断使能：传输错误中断使能
            config . enable_HTIE        =0; //bit3          中断使能：半传输中断
            config . enable_TCIE        =1; //bit4       ★ 中断使能：传输完成中断
            config . EnableFlowCtrl	 	=0; //bit5          是否使能外设流控
            config . direction          =0; //bit6-7     ★ 数据传输方向
                                            //              0：外设   --> 存储器
                                            //              1：存储器 --> 外设
                                            //              2：存储器 --> 存储器
            config . isloopMode         =0; //bit8       ★ 是否循环模式
            config . isPeripheralInc    =0; //bit9          是否外设地址自增模式
            config . isMemoryInc        =1; //bit10         是否存储器地址自增模式
            config . psize              =0; //bit11-12      外设数据大小（位数）
                                            //              0     1       2       
                                            //              8位   16位    32位
            config . msize              =0; //bit13-14      存储数据大小
                                            //              0     1       2       
                                            //              8位   16位    32位
            config . pincos             =0; //bit15         为1时，固定偏移量为4（32位对齐），
                                            //              此时msize不起作用   
            config . DMA_priority       =0; //bit16-17      DMA优先级
                                            //              0	1	2	3
                                            //              低	中	高	非常高
            config . DBM          		=0; //bit18         双缓冲
            config . current_taget      =0; //bit19         当前使用哪个缓冲（针对双缓冲应用）
                                            //              0 ：缓冲0
                                            //              1 ：缓冲1
            config . Reserved0          =0; //bit20         预留       
            config . pburst             =0; //bit21-22      外设突发模式选择
                                            //              0	    1	    2	    4 
                                            //              单次	4个     8个     16个
            config . mburst             =0; //bit23-24      存储器突发模式选择
                                            //              0	    1	    2	    4 
                                            //              单次	4个     8个     16个
            config . channel_select=ListRxDma[n].channel;//bit25-27   ★ 通道选择【0-7】

            
            //---------- fifo设置 --------
            
            config_fifo . FTH			 =0;// 阀值选择：
                                            //  00： FIFO 容量的 1/4?
                                            //	01： FIFO 容量的 1/2?
                                            //	10： FIFO 容量的 3/4?
                                            //	11： FIFO 完整容量
			config_fifo . DMDIS		    =0; // 直接模式使能（这里设置为0 后）
											//注释：这里设置为0后，配置串口接收DMA，收到数据就存储
											//		设置为1时，感觉是4字节对齐
            config_fifo . FS			=0;	// FIFO 状态（直接模式下无意义）
                                            //	000  0/4  < fifo_level < 1/4 
                                            //	001  1/4  < fifo_level < 2/4 
                                            //	010  2/4  < fifo_level < 3/4 
                                            //	011  3/4  < fifo_level < 4/4 
                                            //	100  FIFO空
                                            //	101	 FIFO已满
            config_fifo . Reserved0	    =0; //
            config_fifo . FEIE		    =0; //	FIFO 错误中断使能

        }
        // 步骤1：使能DMA时钟
        dma_enable_clk( ListRxDma[n].dma );			       	

        // 步骤2：DMA开始配置
        dma_wait_for_config(    ListRxDma[n].dma ,				//DMA编号
                                ListRxDma[n].stream,			//流编号
                                100);   						//等待超时时间

        // 步骤3：DMA设置CR和fifo
        dma_config_stream_cr(   ListRxDma[n].dma ,				//DMA编号
                                ListRxDma[n].stream,			//流编号
                                config);						//CR配置

        // 步骤4：DMA设置fifo
        dma_config_stream_fifo( ListRxDma[n].dma ,				//DMA编号
                                ListRxDma[n].stream,			//流编号
                                config_fifo);					//FIFO配置
		// 步骤5：设置dma外设地址 -- 放到函数外实现
		dma_set_peripheral_addr(	ListRxDma[n].dma ,			//DMA编号
									ListRxDma[n].stream,		//流编号
									(uint32_t)usart_DR[n]);		//外设地址	
		// 步骤6：设置dma存储器地址
		dma_set_memory_addr( 	ListRxDma[n].dma ,				//DMA编号
                                ListRxDma[n].stream,			//流编号
								(uint32_t)buff0,				//缓冲1
								(uint32_t)buff1,				//缓冲2
								len);							//长度

		// 步骤7：使能DMAstream（使能后，触发DMA发送 / 开启DMA接收）
		dma_enable_stream( 	 	ListRxDma[n].dma ,				//DMA编号
                                ListRxDma[n].stream);			//流编号
		
        
    }

	void usart_enable_rxDMA(Enum_UART_Number  n, 
							uint8_t* buff0,
							uint8_t* buff1,
							int bufflen)
    {
		//使能usart-n 的DMA接收
		SetBit( usart_addr_list[n]->CR3 , 6);   //接收的DMA使能

        template_config_uart_RxDma( n,
									buff0,
									buff1,
									bufflen);

    }

	void usart_disable_rxDMA(Enum_UART_Number  n)
	{
		dma_disable_stream( ListRxDma[n].dma,
							ListRxDma[n].stream);
	}
	
	void usart_enable_rxDMA_it(Enum_UART_Number  n)
	{
		NVIC_EnableIRQ(ListRxDma[n].IRQ_number);
	}
	
	int usart_read_dma_ndtr(int n)
	{
		return dma_read_ndtr_reg(ListRxDma[n].dma, ListRxDma[n].stream );
	}
