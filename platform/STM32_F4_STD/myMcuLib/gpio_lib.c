
#include "gpio_lib.h"
	
//================================================================================================
//	GPIO配置
//================================================================================================
	#define RCC_ENABLE_CLK_GPIOB_A	RCC->AHB1ENR |= (1<<0) 
	#define RCC_ENABLE_CLK_GPIOB_B	RCC->AHB1ENR |= (1<<1) 
	#define RCC_ENABLE_CLK_GPIOB_C	RCC->AHB1ENR |= (1<<2) 
	#define RCC_ENABLE_CLK_GPIOB_D	RCC->AHB1ENR |= (1<<3) 
	#define RCC_ENABLE_CLK_GPIOB_E	RCC->AHB1ENR |= (1<<4) 
	#define RCC_ENABLE_CLK_GPIOB_F	RCC->AHB1ENR |= (1<<5) 
	#define RCC_ENABLE_CLK_GPIOB_G	RCC->AHB1ENR |= (1<<6) 
	#define RCC_ENABLE_CLK_GPIOB_H	RCC->AHB1ENR |= (1<<7) 
	#define RCC_ENABLE_CLK_GPIOB_I	RCC->AHB1ENR |= (1<<8) 

	void GPIO_Enable( int GPIOx)
	{
		switch(GPIOx)
		{
			case  GPIO_A :
				RCC_ENABLE_CLK_GPIOB_A;
				break;
			case  GPIO_B :
				RCC_ENABLE_CLK_GPIOB_B;
				break;
			case  GPIO_C :
				RCC_ENABLE_CLK_GPIOB_C;
				break;
			case  GPIO_D  :
				RCC_ENABLE_CLK_GPIOB_D;
				break;
			case  GPIO_E :
				RCC_ENABLE_CLK_GPIOB_E;
				break;
			case  GPIO_F :
				RCC_ENABLE_CLK_GPIOB_F;
				break;
			case  GPIO_G :
				RCC_ENABLE_CLK_GPIOB_G;
				break;
			case  GPIO_H :
				RCC_ENABLE_CLK_GPIOB_H;
				break;
			case  GPIO_I :
				RCC_ENABLE_CLK_GPIOB_I;
				break;				 	
		}
	}


	/**
	 * @brief GPIO通用设置 
	 * 
	 * @param GPIOx 	GPIOA~GPIOI.
	 * @param BITx 		0X0000~0XFFFF , 位设置,每个位代表一个IO（允许同时设置多个bit）,
	 * 									第0位代表Px0,第1位代表Px1,依次类推.
	 * 									比如0X0101,代表同时设置Px0和Px8.
	 * @param MODE 		0~3:模式选择	
	 * @param OTYPE  	0~1:输出类型选择	(输入模式下无效) 	 
	 * @param OSPEED 	0~3:输出速度设置	(输入模式下无效)  	 
	 * @param PUPD 		0~3:上下拉设置		 
	 */
	void GPIO_Set(	GPIO_TypeDef* GPIOx,//GPIOA~GPIOI.
					uint32_t BITx,		//0X0000~0XFFFF
					uint32_t MODE,		//0~3:模式选择 	 	 _________________
										//			  		| 0 | 输入(默认)  |
										//			 		| 1 | 普通输出    |
										//			 		| 2 | 复用功能    |
										//			  		| 3 | 模拟输入    |
										//			  		|___|____________|
					uint32_t OTYPE,		//0~1:输出类型选择	 __________________
										//(输入模式下无效) 	 | 0 | 推挽输出	   |
										//					| 1 | 开漏输出	  |
										//					|___|_____________
					uint32_t OSPEED,	//0~3:输出速度设置	 _________________
										//(输入模式下无效)   | 0 | 2MHz		  |
										//					| 1 | 25Mhz	     |
										//					| 2 | 50Mhz	     |
										//					| 3 | 100Mh	     |
										//					|___|____________|
					uint32_t PUPD
				)
	{  
		uint32_t pinpos=0,pos=0,curpin=0;
		for(pinpos=0;pinpos<16;pinpos++)
		{
			pos=1<<pinpos;		//一个个位检查 
			curpin=BITx&pos;	//检查引脚是否要设置
			if(curpin==pos)		//需要设置
			{
				GPIOx->MODER&=~(3<<(pinpos*2));				//先清除原来的设置
				GPIOx->MODER|=MODE<<(pinpos*2);				//设置新的模式 
				if((MODE==0X01)||(MODE==0X02))				//如果是输出模式/复用功能模式
				{  
					GPIOx->OSPEEDR&=~(3<<(pinpos*2));		//清除原来的设置
					GPIOx->OSPEEDR|=(OSPEED<<(pinpos*2));	//设置新的速度值  
					GPIOx->OTYPER&=~(1<<pinpos) ;			//清除原来的设置
					GPIOx->OTYPER|=OTYPE<<pinpos;			//设置新的输出模式
				}  
				GPIOx->PUPDR&=~(3<<(pinpos*2));				//先清除原来的设置
				GPIOx->PUPDR|=PUPD<<(pinpos*2);				//设置新的上下拉
			}
		}
	}

	/**
	 * @brief 外部中断配置（只针对GPIOA~I;不包括PVD,RTC,USB_OTG,USB_HS,以太网唤醒等）
	 * 
	 * @param GPIOx : 取值范围（0~8）,代表GPIOA~I
	 * @param BITx  : 需要使能的位   _____________________________
	 * @param TRIM  触发模式： 		 | 1 	 | 2 		 |3		  |
	 * 								|-------|-----------|--------|
	 * 						   		|上升沿  |下降沿     |任意电平 |
	 * 						   		|_______|___________|________|	
	 */
	void Ex_NVIC_Config(uint8_t GPIOx, uint8_t BITx, uint8_t TRIM) 
	{ 
		uint8_t EXTOFFSET=(BITx%4)*4;  
		RCC->APB2ENR|=1<<14;  							//使能SYSCFG时钟  
		SYSCFG->EXTICR[BITx/4]&=~(0x000F<<EXTOFFSET);	//清除原来设置！！！
		SYSCFG->EXTICR[BITx/4]|=GPIOx<<EXTOFFSET;		//EXTI.BITx映射到GPIOx.BITx 
		//自动设置
		EXTI->IMR|=1<<BITx;								//开启line BITx上的中断(如果要禁止中断，则反操作即可)
		if(TRIM&0x01)EXTI->FTSR|=1<<BITx;				//line BITx上事件下降沿触发
		if(TRIM&0x02)EXTI->RTSR|=1<<BITx;				//line BITx上事件上升降沿触发
	} 	

	//GPIO复用设置
	void GPIO_AF_Set(	GPIO_TypeDef* GPIOx,	// GPIOA ~ GPIOI.
						uint8_t BITx,			// 0 ~ 15,代表IO引脚编号.
						uint8_t AFx				// AF0~15 ，详细的请见407数据手册, 62页 Table9 : Alternate function mapping
					)
	{  
		GPIOx->AFR[BITx>>3] &= ~(0X0F<<((BITx&0X07)*4));
		GPIOx->AFR[BITx>>3] |= (uint32_t)AFx<<((BITx&0X07)*4);
	}   
