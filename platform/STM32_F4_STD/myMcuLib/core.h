#ifndef core_h
#define core_h

    #include "stdint.h"
	#include "stm32f407xx.h"
	#include "core_cm4.h"
//================================================================================================
//	中断配置
//================================================================================================
	/**
	 * @brief 关闭所有中断(但是不包括fault和NMI中断)
	 * 
	 */
	void disable_all_interrupt(void);

    /**
	 * @brief 允许所有中断
	 * 
	 */
	void enable_all_interrupt(void);


    /**
	 * @brief 设置中断向量表的偏移地址
	 * 
	 * @param NVIC_VectTab  : 基址
	 * @param Offset 		: 偏移量	
	 */
	void nvic_set_vectorTable(uint32_t NVIC_VectTab,uint32_t Offset);

    /**
	 * @brief (stack)设置NVIC分组
	 * 
	 * @param NVIC_Group: NVIC分组 0~4 总共5组 	
	 */
	static void nvic_set_priority_group(uint8_t NVIC_Group)	;

    /**
	 * @brief 设置NVIC 
	 * 
	 * @param NVIC_PreemptionPriority 	抢占优先级
	 * @param NVIC_SubPriority 			响应优先级
	 * @param NVIC_Channel 				中断编号
	 * @param NVIC_Group 				优先级bit分配(推荐设置为2)
	 */
	void nvic_init(	uint8_t NVIC_PreemptionPriority,	    //抢占优先级 (数值越小,越优先)
						uint8_t NVIC_SubPriority,			//响应优先级 (数值越小,越优先)
						uint8_t NVIC_Channel,				//中断编号
						uint8_t NVIC_Group					//优先级bit分配(推荐设置为2)
															//____________________________________________
															// value	|	抢占优先级位数 | 响应优先级位数 |
															//  0		|	0			 |	4			 |
															//  1		|	1			 |	3			 |
															//  2		|	2			 |	2			 |
															//  3		|	3			 |	1			 |	
															//  4		|	4			 |	0			 |
															//__________|________________|_______________|
					);
	
	void nvic_enable_irq(IRQn_Type IRQn);
//================================================================================================
//	 	
//================================================================================================
	/**
	 * @brief 系统定时器设置
	 * 
	 * @param period 		周期（ms）
	 * @param NvicPriority 	系统定时器中断的优先级
	 * @param callback 		系统定时器中断回调函数
	 */
	void systick_config_ms(	uint8_t period , 			///< 周期（ms）
							uint8_t NvicPriority ,		///< 系统定时器中断的优先级
							void(*callback)(void) ); 	///< 系统定时器中断回调函数

//================================================================================================
//	 	系统API，堆栈设置，休眠，软复位
//================================================================================================

	//设置栈顶地址
	//addr:栈顶地址
	void MSR_MSP(uint32_t addr) ;

    /**
	 * @brief 进入待机模式
	 * 
	 */
	void Sys_Standby(void);


    /**
	 * @brief 软复位
	 * 
	 */
	void Sys_Soft_Reset(void);

#endif



