#ifndef	DMA_lib_h
#define DMA_lib_h

	#include "stdint.h"
//================================================================================
//	
//================================================================================
	
	typedef enum{
		DMA_01 = 0,				//DMA模块1
		DMA_02 = 1				//DMA模块2
	}Enum_DMAnumber;
	
	typedef enum{				//DMA流
		DMA_Stream_00 = 0,
		DMA_Stream_01 = 1,
		DMA_Stream_02 = 2,
		DMA_Stream_03 = 3,
		DMA_Stream_04 = 4,
		DMA_Stream_05 = 5, 
		DMA_Stream_06 = 6,
		DMA_Stream_07 = 7
	}Enum_DmaStream;

//================================================================================
//	寄存器定义
//================================================================================

//	//------------------ 与手册一致版本 --------------------
//    typedef struct{
//        uint32_t EN         :1; //bit0          使能DMA
//        uint32_t DMEIE      :1; //bit1          中断使能：直接模式错误中断使能
//        uint32_t TEIE       :1; //bit2          中断使能：传输错误中断使能
//        uint32_t HTIE       :1; //bit3          中断使能：半传输中断
//        uint32_t TCIE       :1; //bit4          中断使能：传输完成中断
//        uint32_t TPFCTRL    :1; //bit5          是否使能外设流控
//        uint32_t DIR        :2; //bit6-7        数据传输方向
//                                //              0：外设   --> 存储器
//								//              1：存储器 --> 外设
//								//              2：存储器 --> 存储器
//        uint32_t CIRC       :1; //bit8          是否循环模式
//        uint32_t PINC       :1; //bit9          是否外设地址自增模式
//        uint32_t MINC       :1; //bit10         是否存储器地址自增模式
//        uint32_t PSIZE      :2; //bit11-12      外设数据大小（位数）
//                                //              0     1       2       
//                                //              8位   16位    32位
//        uint32_t MSIZE      :2; //bit13-14      存储数据大小
//                                //              0     1       2       
//                                //              8位   16位    32位
//        uint32_t PINCOS     :1; //bit15         为1时，固定偏移量为4（32位对齐），
//                                //              此时msize不起作用   
//        uint32_t PL         :2; //bit16-17      DMA优先级
//								//              0	1	2	3
//								//              低	中	高	非常高

//        uint32_t DBM        :1; //bit18         双缓冲模式
//        uint32_t CT         :1; //bit19         当前使用哪个缓冲（针对双缓冲应用）
//                                //              0 ：缓冲0
//                                //              1 ：缓冲1
//        uint32_t Reserved   :1; //bit20         预留       
//        uint32_t PBURST     :2; //bit21-22      外设突发模式选择
//								//              0	    1	    2	    4 
//								//              单次	4个     8个     16个
//        uint32_t MBURST     :2; //bit23-24      存储器突发模式选择
//								//              0	    1	    2	    4 
//								//              单次	4个     8个     16个
//        uint32_t CHSEL      :3; //bit25-27      通道选择【0-7】
//    }Register_DMA_SxPAR;
	
    //------------------ 寄存器名称自定义版本 --------------------
    typedef struct{
        uint32_t DMAEnable          :1; //bit0          使能DMA
        uint32_t DMEIE              :1; //bit1          中断使能：直接模式错误中断使能
        uint32_t enable_TEIE        :1; //bit2          中断使能：传输错误中断使能
        uint32_t enable_HTIE        :1; //bit3          中断使能：半传输中断
        uint32_t enable_TCIE        :1; //bit4          中断使能：传输完成中断
        uint32_t EnableFlowCtrl		:1; //bit5          是否使能外设流控
        uint32_t direction          :2; //bit6-7        数据传输方向
										//              0：外设   --> 存储器
										//              1：存储器 --> 外设
										//              2：存储器 --> 存储器
        uint32_t isloopMode         :1; //bit8          是否循环模式
        uint32_t isPeripheralInc    :1; //bit9          是否外设地址自增模式
        uint32_t isMemoryInc        :1; //bit10         是否存储器地址自增模式
        uint32_t psize              :2; //bit11-12      外设数据大小（位数）
										//              0     1       2       
										//              8位   16位    32位
        uint32_t msize              :2; //bit13-14      存储数据大小
										//              0     1       2       
										//              8位   16位    32位
        uint32_t pincos             :1; //bit15         为1时，固定偏移量为4（32位对齐），
                                        //              此时msize不起作用   
        uint32_t DMA_priority       :2; //bit16-17      DMA优先级
										//              0	1	2	3
										//              低	中	高	非常高
        uint32_t DBM          		:1; //bit18         预留
        uint32_t current_taget      :1; //bit19         当前使用哪个缓冲（针对双缓冲应用）
										//              0 ：缓冲0
										//              1 ：缓冲1
        uint32_t Reserved0          :1; //bit20         预留       
        uint32_t pburst             :2; //bit21-22      外设突发模式选择
										//              0	    1	    2	    4 
										//              单次	4个     8个     16个
        uint32_t mburst             :2; //bit23-24      存储器突发模式选择
										//              0	    1	    2	    4 
										//              单次	4个     8个     16个
        uint32_t channel_select     :3; //bit25-27      通道选择【0-7】
    }Register_DMA_config;
	
	//---------------------  FIFO 相关   --------------------------
	typedef struct{
		uint32_t FTH				:2; // 阀值选择：
										//  00： FIFO 容量的 1/4?
										//	01： FIFO 容量的 1/2?
										//	10： FIFO 容量的 3/4?
										//	11： FIFO 完整容量
		uint32_t DMDIS				:1;	// 直接模式使能
		uint32_t FS					:2;	// FIFO 状态（直接模式下无意义）
										//	000  0/4  < fifo_level < 1/4 
										//	001  1/4  < fifo_level < 2/4 
										//	010  2/4  < fifo_level < 3/4 
										//	011  3/4  < fifo_level < 4/4 
										//	100  FIFO空
										//	101	 FIFO已满
		uint32_t Reserved0			:1;	//
		uint32_t FEIE				:1;	//	FIFO 错误中断使能
	}Register_DMA_SxFCR;
	
	typedef enum{
		Flag_FIFO_error 		= 0,	//数据流 x 上发生 FIFO 错误事件
		Flag_resume 			= 1,	//预留
		Flag_direct_mode_error 	= 2,	//数据流 x 上发生直接模式错误
		Flag_transfer_error 	= 3,	//数据流 x 上发生传输错误
		Flag_half_transfer 		= 4,	//数据流 x 上发生半传输事件
		Flag_transfer_complete  = 5		//传输完成中断标记	
	}EnumDmaItFlag;
	
//========================================================================
//
//========================================================================
	// 步骤1：使能DMA时钟
	void dma_enable_clk( Enum_DMAnumber dma );				//DMA编号

	// 步骤2：等待DMA直到可配置
	void dma_wait_for_config(Enum_DMAnumber dma ,			//DMA编号
							Enum_DmaStream n,				//流编号
							uint32_t timeout);				//等待超时时间

	// 步骤3：DMA设置CR（主要属性，包括中断使能）
	void dma_config_stream_cr( Enum_DMAnumber dma ,			//DMA编号
							Enum_DmaStream n,				//流编号
							Register_DMA_config config);	//CR配置

	// 步骤4：DMA设置fifo（fifo设置，每次传输xx字节等、突发传输等）
	void dma_config_stream_fifo( Enum_DMAnumber dma ,		//DMA编号
							Enum_DmaStream n,				//流编号
							Register_DMA_SxFCR config);		//FIFO配置

	// 步骤5：设置dma外设地址
	void dma_set_peripheral_addr(	Enum_DMAnumber dma ,	//DMA编号
									Enum_DmaStream n,		//流编号
									uint32_t addr);			//超时时间	

	// 步骤6：设置dma存储器地址
	void dma_set_memory_addr( 	Enum_DMAnumber dma ,		//DMA编号
								Enum_DmaStream n,			//流编号
								uint32_t buff0,				//缓冲1
								uint32_t buff1,				//缓冲2
								uint16_t len);				//长度

	//步骤7：使能DMAstream（使能后，触发DMA发送 / 开启DMA接收）
	void dma_enable_stream( Enum_DMAnumber dma ,			//DMA编号
							Enum_DmaStream n);				//流编号

	// 关闭DMAstream
	void dma_disable_stream( Enum_DMAnumber dma ,			//DMA编号
							Enum_DmaStream n);				//流编号
//----------------------------------------------------------------------------
//	功能API
//----------------------------------------------------------------------------	
	//获取DMA_Stream的地址
	void* dma_get_stream_addr(	Enum_DMAnumber dma ,		//DMA编号
								Enum_DmaStream n);
	//读取当前buff编号（返回 0/1）
	int dma_read_buff_index(Enum_DMAnumber dma ,			//DMA编号
							Enum_DmaStream n);				//流编号
	
	//切换DMA缓冲为buff_x
	void dma_switch_buff_x(	Enum_DMAnumber dma ,			//DMA编号
							Enum_DmaStream n,				//流编号
							int x	);						//设置为Buff0/1					
	//读取NDTR寄存器（剩余DMA字节，为0表示本次DMA事务完成）
	int dma_read_ndtr_reg(	Enum_DMAnumber dma ,			//DMA编号
							Enum_DmaStream n);				//流编号
	//------------------------- 中断相关 ------------------------
	// 读取DMA中断状态
	uint8_t dma_read_it_flag(	Enum_DMAnumber dma ,////DMA编号
								Enum_DmaStream n,	//流编号
								EnumDmaItFlag flag);//状态位
	
	// 清DMA中断标志
	void dma_clear_it_flag(	Enum_DMAnumber dma ,	//DMA编号
							Enum_DmaStream n,		//流编号
							EnumDmaItFlag flag);	//标志位

		// 清DMA-stream所有中断标志
	void dma_clear_it_all_flag(	Enum_DMAnumber dma ,	//DMA编号
							Enum_DmaStream n);		//流编号
	
	

	

	


	/*配置模板
		///例：
		Register_DMA_config* config =(Register_DMA_config*)&(DMA2_Stream7->CR);
		dma2_enable_clk();
		dma2_start_config( 7 );
		//开始设置DMA
		config -> DMEIE              =; //bit1          中断使能：直接模式错误中断使能
        config -> enable_TEIE        =; //bit2          中断使能：传输错误中断使能
        config -> enable_HTIE        =; //bit3          中断使能：半传输中断
        config -> enable_TCIE        =; //bit4          中断使能：传输完成中断
        config -> EnableFlowCtrl	 =; //bit5          是否使能外设流控
        config -> direction          =; //bit6-7        数据传输方向
										//              0：外设   --> 存储器
										//              1：存储器 --> 外设
										//              2：存储器 --> 存储器
        config -> isloopMode         =; //bit8          是否循环模式
        config -> isPeripheralInc    =; //bit9          是否外设地址自增模式
        config -> isMemoryInc        =; //bit10         是否存储器地址自增模式
        config -> psize              =; //bit11-12      外设数据大小（位数）
										//              0     1       2       
										//              8位   16位    32位
        config -> msize              =; //bit13-14      存储数据大小
										//              0     1       2       
										//              8位   16位    32位
        config -> pincos             =; //bit15         为1时，固定偏移量为4（32位对齐），
                                        //              此时msize不起作用   
        config -> DMA_priority       =; //bit16-17      DMA优先级
										//              0	1	2	3
										//              低	中	高	非常高
        config -> DBM          		 =; //bit18         双缓冲模式
        config -> current_taget      =; //bit19         当前使用哪个缓冲（针对双缓冲应用）
										//              0 ：缓冲0
										//              1 ：缓冲1
        config -> Reserved0          =; //bit20         预留       
        config -> pburst             =; //bit21-22      外设突发模式选择
										//              0	    1	    2	    4 
										//              单次	4个     8个     16个
        config -> mburst             =; //bit23-24      存储器突发模式选择
										//              0	    1	    2	    4 
										//              单次	4个     8个     16个
        config -> channel_select     =; //bit25-27      通道选择【0-7】
		
		//fifo设置
				//fifo设置
		Register_DMA_SxFCR* config_fifo = (Register_DMA_SxFCR*)&(DMA2_Stream7->FCR);
		config_fifo -> FTH			 =; // 阀值选择：
										//  00： FIFO 容量的 1/4?
										//	01： FIFO 容量的 1/2?
										//	10： FIFO 容量的 3/4?
										//	11： FIFO 完整容量
		config_fifo -> DMDIS		 =; // 直接模式使能
		config_fifo -> FS			 =;	// FIFO 状态（直接模式下无意义）
										//	000  0/4  < fifo_level < 1/4 
										//	001  1/4  < fifo_level < 2/4 
										//	010  2/4  < fifo_level < 3/4 
										//	011  3/4  < fifo_level < 4/4 
										//	100  FIFO空
										//	101	 FIFO已满
		config_fifo -> Reserved0	 =;	//
		config_fifo -> FEIE			 =;	//	FIFO 错误中断使能
	*/
#endif

