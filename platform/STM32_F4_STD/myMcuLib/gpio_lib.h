#ifndef GPIO_LIB_H
#define GPIO_LIB_H

    #include "stm32f4xx.h"
    //位带操作,实现51类似的GPIO控制功能
    //具体实现思想,参考<<CM3权威指南>>第五章(87页~92页).M4同M3类似,只是寄存器地址变了.
    //IO口操作宏定义
    #define BITBAND(addr, bitnum) ((addr & 0xF0000000)+0x2000000+((addr &0xFFFFF)<<5)+(bitnum<<2)) 
    #define MEM_ADDR(addr)  *((volatile unsigned long  *)(addr)) 
    #define BIT_ADDR(addr, bitnum)   MEM_ADDR(BITBAND(addr, bitnum)) 
    //IO口地址映射
    #define GPIOA_ODR_Addr    (GPIOA_BASE+20) //0x40020014
    #define GPIOB_ODR_Addr    (GPIOB_BASE+20) //0x40020414 
    #define GPIOC_ODR_Addr    (GPIOC_BASE+20) //0x40020814 
    #define GPIOD_ODR_Addr    (GPIOD_BASE+20) //0x40020C14 
    #define GPIOE_ODR_Addr    (GPIOE_BASE+20) //0x40021014 
    #define GPIOF_ODR_Addr    (GPIOF_BASE+20) //0x40021414    
    #define GPIOG_ODR_Addr    (GPIOG_BASE+20) //0x40021814   
    #define GPIOH_ODR_Addr    (GPIOH_BASE+20) //0x40021C14    
    #define GPIOI_ODR_Addr    (GPIOI_BASE+20) //0x40022014     

    #define GPIOA_IDR_Addr    (GPIOA_BASE+16) //0x40020010 
    #define GPIOB_IDR_Addr    (GPIOB_BASE+16) //0x40020410 
    #define GPIOC_IDR_Addr    (GPIOC_BASE+16) //0x40020810 
    #define GPIOD_IDR_Addr    (GPIOD_BASE+16) //0x40020C10 
    #define GPIOE_IDR_Addr    (GPIOE_BASE+16) //0x40021010 
    #define GPIOF_IDR_Addr    (GPIOF_BASE+16) //0x40021410 
    #define GPIOG_IDR_Addr    (GPIOG_BASE+16) //0x40021810 
    #define GPIOH_IDR_Addr    (GPIOH_BASE+16) //0x40021C10 
    #define GPIOI_IDR_Addr    (GPIOI_BASE+16) //0x40022010 
    
    //IO口操作,只对单一的IO口!
    //确保n的值小于16!
    #define PAout(n)   BIT_ADDR(GPIOA_ODR_Addr,n)  //输出 
    #define PAin(n)    BIT_ADDR(GPIOA_IDR_Addr,n)  //输入 

    #define PBout(n)   BIT_ADDR(GPIOB_ODR_Addr,n)  //输出 
    #define PBin(n)    BIT_ADDR(GPIOB_IDR_Addr,n)  //输入 

    #define PCout(n)   BIT_ADDR(GPIOC_ODR_Addr,n)  //输出 
    #define PCin(n)    BIT_ADDR(GPIOC_IDR_Addr,n)  //输入 

    #define PDout(n)   BIT_ADDR(GPIOD_ODR_Addr,n)  //输出 
    #define PDin(n)    BIT_ADDR(GPIOD_IDR_Addr,n)  //输入 

    #define PEout(n)   BIT_ADDR(GPIOE_ODR_Addr,n)  //输出 
    #define PEin(n)    BIT_ADDR(GPIOE_IDR_Addr,n)  //输入

    #define PFout(n)   BIT_ADDR(GPIOF_ODR_Addr,n)  //输出 
    #define PFin(n)    BIT_ADDR(GPIOF_IDR_Addr,n)  //输入

    #define PGout(n)   BIT_ADDR(GPIOG_ODR_Addr,n)  //输出 
    #define PGin(n)    BIT_ADDR(GPIOG_IDR_Addr,n)  //输入

    #define PHout(n)   BIT_ADDR(GPIOH_ODR_Addr,n)  //输出 
    #define PHin(n)    BIT_ADDR(GPIOH_IDR_Addr,n)  //输入

    #define PIout(n)   BIT_ADDR(GPIOI_ODR_Addr,n)  //输出 
    #define PIin(n)    BIT_ADDR(GPIOI_IDR_Addr,n)  //输入
    ////////////////////////////////////////////////////////////////////////////////// 
    //Ex_NVIC_Config专用定义
    #define GPIO_A 				0
    #define GPIO_B 				1
    #define GPIO_C				2
    #define GPIO_D 				3
    #define GPIO_E 				4
    #define GPIO_F 				5
    #define GPIO_G 				6 
    #define GPIO_H 				7 
    #define GPIO_I 				8 

    #define FTIR   				1  		//下降沿触发
    #define RTIR   				2  		//上升沿触发

    //GPIO设置专用宏定义
    #define GPIO_MODE_IN    	0		//普通输入模式
    #define GPIO_MODE_OUT		1		//普通输出模式
    #define GPIO_MODE_AF		2		//AF功能模式
    #define GPIO_MODE_AIN		3		//模拟输入模式

    #define GPIO_SPEED_2M		0		//GPIO速度2Mhz
    #define GPIO_SPEED_25M		1		//GPIO速度25Mhz
    #define GPIO_SPEED_50M		2		//GPIO速度50Mhz
    #define GPIO_SPEED_100M		3		//GPIO速度100Mhz

    #define GPIO_PUPD_NONE		0		//不带上下拉
    #define GPIO_PUPD_PU		1		//上拉
    #define GPIO_PUPD_PD		2		//下拉
    #define GPIO_PUPD_RES		3		//保留 

    #define GPIO_OTYPE_PP		0		//推挽输出
    #define GPIO_OTYPE_OD		1		//开漏输出 

    //GPIO引脚编号定义
    #define PIN0				1<<0
    #define PIN1				1<<1
    #define PIN2				1<<2
    #define PIN3				1<<3
    #define PIN4				1<<4
    #define PIN5				1<<5
    #define PIN6				1<<6
    #define PIN7				1<<7
    #define PIN8				1<<8
    #define PIN9				1<<9
    #define PIN10				1<<10
    #define PIN11				1<<11
    #define PIN12				1<<12
    #define PIN13				1<<13
    #define PIN14				1<<14
    #define PIN15				1<<15 

    /**
     * @brief 使能GPIO时钟
     * 
     * @param GPIOx : 宏GPIO_A -- I
     */
	void GPIO_Enable( int GPIOx);

    /**
	 * @brief GPIO通用设置 
	 * 
	 * @param GPIOx 	GPIOA~GPIOI.
	 * @param BITx 		0X0000~0XFFFF , 位设置,每个位代表一个IO（允许同时设置多个bit）,
	 * 									第0位代表Px0,第1位代表Px1,依次类推.
	 * 									比如0X0101,代表同时设置Px0和Px8.
	 * @param MODE 		0~3:模式选择	
	 * @param OTYPE  	0~1:输出类型选择	(输入模式下无效) 	 
	 * @param OSPEED 	0~3:输出速度设置	(输入模式下无效)  	 
	 * @param PUPD 		0~3:上下拉设置		 
	 */
	void GPIO_Set(	GPIO_TypeDef* GPIOx,//GPIOA~GPIOI.
					uint32_t BITx,		//0X0000~0XFFFF
					uint32_t MODE,		//0~3:模式选择 	 	 _________________
										//			  		| 0 | 输入(默认)  |
										//			 		| 1 | 普通输出    |
										//			 		| 2 | 复用功能    |
										//			  		| 3 | 模拟输入    |
										//			  		|___|____________|
					uint32_t OTYPE,		//0~1:输出类型选择	 __________________
										//(输入模式下无效) 	 | 0 | 推挽输出	   |
										//					| 1 | 开漏输出	  |
										//					|___|_____________
					uint32_t OSPEED,	//0~3:输出速度设置	 _________________
										//(输入模式下无效)   | 0 | 2MHz		  |
										//					| 1 | 25Mhz	     |
										//					| 2 | 50Mhz	     |
										//					| 3 | 100Mh	     |
										//					|___|____________|
					uint32_t PUPD
				);

    /**
	 * @brief 外部中断配置（只针对GPIOA~I;不包括PVD,RTC,USB_OTG,USB_HS,以太网唤醒等）
	 * 
	 * @param GPIOx : 取值范围（0~8）,代表GPIOA~I
	 * @param BITx  : 需要使能的位   _____________________________
	 * @param TRIM  触发模式： 		 | 1 	 | 2 		 |3		  |
	 * 								|-------|-----------|--------|
	 * 						   		|上升沿  |下降沿     |任意电平 |
	 * 						   		|_______|___________|________|	
	 */
	void Ex_NVIC_Config(uint8_t GPIOx, uint8_t BITx, uint8_t TRIM) ;
	
	
    //GPIO复用设置
	void GPIO_AF_Set(	GPIO_TypeDef* GPIOx,	// GPIOA ~ GPIOI.
						uint8_t BITx,			// 0 ~ 15,代表IO引脚编号.
						uint8_t AFx				// AF0~15 ，详细的请见407数据手册, 62页 Table9 : Alternate function mapping
					);
	
	
#endif
		
