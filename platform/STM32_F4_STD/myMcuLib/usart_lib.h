#ifndef usart_lib_h
#define usart_lib_h

    #include "stdint.h"
    #include "c_type.h"
//==================================================================
//      枚举
//==================================================================

    typedef enum{
        Uart1 = 0,
        Uart2,
        Uart3,
        Uart4,
        Uart5,
        Uart6
    }Enum_UART_Number;

    typedef enum{
        Parity_none,    //无校验
        Parity_even,    //偶校验
        Parity_odd      //奇校验
    }Enum_Parity;

    typedef enum{
        USART1_PA9_PA10,    //usart1: PA9, PA10
    }Enum_io_usart;
    
    //------------------ 配置用API ------------------------------------
    
	//  USARTn时钟使能(n:1--6)
    void usart_rcc_enable( Enum_UART_Number n);
    
	//  USARTn时钟关闭(n:1--6)
    void usart_rcc_disable( Enum_UART_Number n);
    
	//  设置IO口 -- usart的绑定关系
    void usart_select_io(Enum_io_usart x );
    
	//  设置usart的波特率、校验位
    void usart_config_port(Enum_UART_Number n ,int boundrate ,Enum_Parity parity);
    
	//  设置中断 -- 建议使用下列宏设置,可以相或 ，
    //  例： UART_IT_TX | UART_IT_RX | UART_IT_IDEL
    #define UART_IT_TX              (1<<2)   //发送完成中断（使用DMA时关闭此中断）
    #define UART_IT_RX              (1<<1)   //接收非空中断
    #define UART_IT_IDEL             1       //空闲中断
    #define UART_IT_DISABLE          0       //不开启中断
    void usart_interrupt_set(Enum_UART_Number n, uint8_t IT);        //中断
    
	
	//==========================================================
	void usart_put_byte( Enum_UART_Number n, uint8_t x);
	//==========================================================
	//uart - TXDMA使能
    void usart_enable_txDMA(Enum_UART_Number n);
	
	//uart - DMA中断使能+配置
	void usart_enable_txDMA_it(Enum_UART_Number  n);
	//uart - DMA接收使能+配置
	void usart_enable_rxDMA(Enum_UART_Number  n, 
							uint8_t* buff0,
							uint8_t* buff1,
							int bufflen);
	
	void usart_enable_rxDMA_it(Enum_UART_Number  n);
	
    //uart启动
    void usart_start(Enum_UART_Number n);
    
	//-------------------------- 接收DMA配置 ------------------------------
	void template_config_uart_RxDma(    Enum_UART_Number  n,
									uint8_t* buff0,
									uint8_t* buff1,
									uint16_t len);
    //-------------------------- 收发API ----------------------------------
    // DMA触发发送
    void usart_put_array_dma(uint8_t n,uint8_t* array, uint16_t len);
    
    //------------------------- uart中断API -------------------------------
	

	//DMA接收
	void usart_disable_rxDMA(Enum_UART_Number  n);
	
	//-------------------------- DMA中断 -------------------------------
    
	//使用宏来定义和注册 DMA发送中断
    //uart_tx_DMAHandlel(uart1,2,7,handle)    //UART1发送
    //uart_tx_DMAHandlel(uart2,1,6,handle)    //UART2发送
	#define uart_DMAHandlel(uartn,dma,stream,handle)           \
			void DMA##dma##_Stream##stream##_IRQHandler()      \
			{                                       \
				if(dma_read_it_flag(DMA_0##dma , DMA_Stream_0##stream, Flag_transfer_complete))        \
				{                                                                           \
					dma_clear_it_flag(DMA_0##dma , DMA_Stream_0##stream, Flag_transfer_complete);      \
					handle();                                                               \
				}                                                                           \
				if(dma_read_it_flag(DMA_0##dma , DMA_Stream_0##stream, Flag_transfer_error))           \
				{                                                                           \
					dma_clear_it_flag(DMA_02 , DMA_Stream_0##stream, Flag_transfer_error);         \
				}                                                                           \
			}
	
	//测试API
	int usart_read_dma_ndtr(int n);
#endif
