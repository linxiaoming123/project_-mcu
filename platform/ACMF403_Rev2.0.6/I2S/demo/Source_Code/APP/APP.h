/*
  ******************************************************************************
  * Copyright (c)  2008 - 2022, Shanghai AisinoChip Co.,Ltd .
  * @file    APP.h 
  * @version V1.0.0
  * @date    2022
  * @author  Aisinochip Firmware Team  
  * @brief   I2S demo Header code.
  ******************************************************************************        
*/
#ifndef __APP_H__
#define __APP_H__

#include "ACM32Fxx_HAL.h"

typedef enum
{
    LOOP_MODE,
    INT_MODE,
    DMA_MODE,
}enum_Mode;

/* APP_I2S_Test */
void APP_I2S_Test(enum_Mode fe_Mode);

#endif
