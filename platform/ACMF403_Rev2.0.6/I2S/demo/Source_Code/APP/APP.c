/*
  ******************************************************************************
  * Copyright (c)  2008 - 2022, Shanghai AisinoChip Co.,Ltd .
  * @file    APP.c 
  * @version V1.0.0
  * @date    2022
  * @author  Aisinochip Firmware Team  
  * @brief   I2S  demo source code.
  ******************************************************************************        
*/
#include "APP.h"

I2S_HandleTypeDef   I2S1_Handle;

DMA_HandleTypeDef   DMA1_I2S1_Handle;

GPIO_InitTypeDef    KEY_Handle;
#define USERKEY_PORT	GPIOC
#define USERKEY_PIN		GPIO_PIN_13

uint32_t gu32_TxBuffer[256];

bool gb_TransferFalg = false;

/*********************************************************************************
* Function    : DAM_Transfer_Complete_Callback
* Description : DAM_Transfer_Complete_Callback
**********************************************************************************/
void DAM_Transfer_Complete_Callback(void)
{
    gb_TransferFalg = true;
}

/*********************************************************************************
* Function    : I2S_IRQHandler
* Description : I2S_IRQHandler
**********************************************************************************/
void I2S_IRQHandler(void)
{
    HAL_I2S_IRQHandler(&I2S1_Handle);
}

/*********************************************************************************
* Function    : DMA_IRQHandler
* Description : DMA_IRQHandler
**********************************************************************************/
void DMA_IRQHandler(void)
{
    HAL_DMA_IRQHandler(&DMA1_I2S1_Handle);
}

/*********************************************************************************
* Function    : UserKEY_Init
* Description : User key initialize
**********************************************************************************/
void UserKEY_Init(void)
{
     System_Enable_Disable_RTC_Domain_Access(FUNC_ENABLE);
    __HAL_RTC_PC13_DIGIT();
    KEY_Handle.Pin       = USERKEY_PIN;
    KEY_Handle.Mode      = GPIO_MODE_INPUT;
    KEY_Handle.Pull      = GPIO_PULLUP;
    KEY_Handle.Alternate = GPIO_FUNCTION_0;

    HAL_GPIO_Init(USERKEY_PORT, &KEY_Handle);
}

/*********************************************************************************
* Function    : UserKEY_Get
* Description : detection button is pressed
**********************************************************************************/
bool UserKEY_Get(void)
{
    if (GPIO_PIN_CLEAR == HAL_GPIO_ReadPin(USERKEY_PORT, USERKEY_PIN)) 
    {
        System_Delay_MS(20);
        
        if (GPIO_PIN_CLEAR == HAL_GPIO_ReadPin(USERKEY_PORT, USERKEY_PIN)) 
        {
            return true;
        }
    }

    return false;
}

/*********************************************************************************
* Function    : APP_I2S_Test
* Description : I2S Test Case
**********************************************************************************/
void APP_I2S_Test(enum_Mode fe_Mode)
{
    uint32_t i;

    for (i = 0; i < 256; i++)
    {
        gu32_TxBuffer[i] |= i << 24;
        gu32_TxBuffer[i] |= i << 16;
        gu32_TxBuffer[i] |= i << 8;
        gu32_TxBuffer[i] |= i;
    }

    UserKEY_Init();

    printfS("---------- Please press the USR_PB button ----------\r\n");
    while(false == UserKEY_Get());

    switch (fe_Mode)
    {
        case LOOP_MODE: 
        {
            printf("I2S communication Test. LOOP_MODE \r\n");

            I2S1_Handle.Instance = I2S1;
            I2S1_Handle.Init.u32_Mode       = I2S_MODE_MASTER_TX;
            I2S1_Handle.Init.u32_Standard   = I2S_STANDARD_PHILIPS;
            I2S1_Handle.Init.u32_DataFormat = I2S_DATAFORMAT_32B;
            I2S1_Handle.Init.u32_MCLKOutput = I2S_MCLKOUTPUT_ENABLE;
            I2S1_Handle.Init.u32_CPOL       = I2S_CPOL_LOW;
            I2S1_Handle.Init.u32_FreqOF     = I2S_FREQ_OF_DISABLE;
            I2S1_Handle.Init.u32_FreqDIV    = 50;
            HAL_I2S_Init(&I2S1_Handle);

            while (1) 
            {
                HAL_I2S_Transmit(&I2S1_Handle, gu32_TxBuffer, 256, 0);
                System_Delay_MS(5);
            }
        }break;

        case INT_MODE: 
        {
            printf("I2S communication Test. INT_MODE \r\n");

            I2S1_Handle.Instance = I2S1;
            I2S1_Handle.Init.u32_Mode       = I2S_MODE_MASTER_TX;
            I2S1_Handle.Init.u32_Standard   = I2S_STANDARD_PHILIPS;
            I2S1_Handle.Init.u32_DataFormat = I2S_DATAFORMAT_32B;
            I2S1_Handle.Init.u32_MCLKOutput = I2S_MCLKOUTPUT_ENABLE;
            I2S1_Handle.Init.u32_CPOL       = I2S_CPOL_LOW;
            I2S1_Handle.Init.u32_FreqOF     = I2S_FREQ_OF_DISABLE;
            I2S1_Handle.Init.u32_FreqDIV    = 50;
            HAL_I2S_Init(&I2S1_Handle);
            
            while (1) 
            {
                HAL_I2S_Transmit_IT(&I2S1_Handle, gu32_TxBuffer, 256);
                System_Delay_MS(5);
            }
        }break;

        case DMA_MODE: 
        {
            printf("I2S communication Test. DMA_MODE \r\n");
            
            DMA1_I2S1_Handle.Instance = DMA_Channel1;
            DMA1_I2S1_Handle.Init.Data_Flow        = DMA_DATA_FLOW_M2P;
            DMA1_I2S1_Handle.Init.Request_ID       = REQ37_I2S1_TX;
            DMA1_I2S1_Handle.Init.Source_Inc       = DMA_SOURCE_ADDR_INCREASE_ENABLE;
            DMA1_I2S1_Handle.Init.Desination_Inc   = DMA_DST_ADDR_INCREASE_DISABLE;
            DMA1_I2S1_Handle.Init.Source_Width     = DMA_SRC_WIDTH_WORD;
            DMA1_I2S1_Handle.Init.Desination_Width = DMA_DST_WIDTH_WORD;
            
            DMA1_I2S1_Handle.DMA_ITC_Callback = DAM_Transfer_Complete_Callback;
            
            HAL_DMA_Init(&DMA1_I2S1_Handle);

            __HAL_LINK_DMA(I2S1_Handle, HDMA_Tx, DMA1_I2S1_Handle);
            
            I2S1_Handle.Instance = I2S1;
            I2S1_Handle.Init.u32_Mode       = I2S_MODE_MASTER_TX;
            I2S1_Handle.Init.u32_Standard   = I2S_STANDARD_PHILIPS;
            I2S1_Handle.Init.u32_DataFormat = I2S_DATAFORMAT_32B;
            I2S1_Handle.Init.u32_MCLKOutput = I2S_MCLKOUTPUT_ENABLE;
            I2S1_Handle.Init.u32_CPOL       = I2S_CPOL_LOW;
            I2S1_Handle.Init.u32_FreqOF     = I2S_FREQ_OF_DISABLE;
            I2S1_Handle.Init.u32_FreqDIV    = 50;
            HAL_I2S_Init(&I2S1_Handle);

            gb_TransferFalg = true;

            while (1) 
            {
                if (gb_TransferFalg == true) 
                {
                    gb_TransferFalg = false;
                    
                    HAL_I2S_Transmit_DMA(&I2S1_Handle, gu32_TxBuffer, 256);
                    System_Delay_MS(50);
                }
            }
        }break;

        default: break; 
    }
}

