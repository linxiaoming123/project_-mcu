/*
 **********************************************************************
 * Copyright (c)  2008 - 2022, Shanghai AisinoChip Co.,Ltd .
 * @file        main.c    
 * @version     V1.0.0
 * @date        2022 
 * @author      Aisinochip Firmware Team  
 * @brief       EXTI Test Main File 
 **********************************************************************
 */
#include "APP.h"

UART_HandleTypeDef Uart_Handle;

/*********************************************************************************
* Function    : Uart_Init
* Description : Debug Uart Initialization
**********************************************************************************/
void Uart_Init(uint32_t fu32_Baudrate)
{
    Uart_Handle.Instance        = UART2;
    Uart_Handle.Init.BaudRate   = fu32_Baudrate;
    Uart_Handle.Init.WordLength = UART_WORDLENGTH_8B;
    Uart_Handle.Init.StopBits   = UART_STOPBITS_1;
    Uart_Handle.Init.Parity     = UART_PARITY_NONE;
    Uart_Handle.Init.Mode       = UART_MODE_TX_RX_DEBUG;
    Uart_Handle.Init.HwFlowCtl  = UART_HWCONTROL_NONE;

    HAL_UART_Init(&Uart_Handle);
	
    // UART_DEBUG_ENABLE control printfS   
    printfS("MCU is running, HCLK=%dHz, PCLK=%dHz\n", System_Get_SystemClock(), System_Get_APBClock()); 
}

/*********************************************************************************
* Function    : main
* Description : The application entry point.
* Input       : None
* Output      : None
**********************************************************************************/
int main(void)
{
    System_Init();
    
    Uart_Init(115200);
    
    APP_EXTI_Test();

    while(1)
    {

    }
}

