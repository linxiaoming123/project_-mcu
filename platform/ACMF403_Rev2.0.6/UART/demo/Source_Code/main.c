/*
 **********************************************************************
 * Copyright (c)  2008 - 2022, Shanghai AisinoChip Co.,Ltd .
 * @file        main.c    
 * @version     V1.0.0
 * @date        2022 
 * @author      Aisinochip Firmware Team  
 * @brief       UART Test Main File 
 **********************************************************************
 */
#include "APP.h" 

#define UART_BAUD_RATE  115200       

UART_HandleTypeDef UART2_Handle;

static uint8_t gu8_UART1Test[] = {"This is UART1 Test Data"};
static uint8_t gu8_UART3Test[] = {"This is UART3 Test Data"};

/************************************************************************
 * function   : Uart_Init
 * Description: Uart Initiation. 
 ************************************************************************/ 
void Uart_Init(void) 
{
    UART2_Handle.Instance        = UART2;    
    UART2_Handle.Init.BaudRate   = UART_BAUD_RATE; 
    UART2_Handle.Init.WordLength = UART_WORDLENGTH_8B;
    UART2_Handle.Init.StopBits   = UART_STOPBITS_1;
    UART2_Handle.Init.Parity     = UART_PARITY_NONE;
    UART2_Handle.Init.Mode       = UART_MODE_TX_RX_DEBUG;
    UART2_Handle.Init.HwFlowCtl  = UART_HWCONTROL_NONE;
    
    HAL_UART_Init(&UART2_Handle);     
    /* ENABLE FIFO */ 
    HAL_UART_Enable_Disable_FIFO(&UART2_Handle,FUNC_ENABLE); 
    
    /* UART_DEBUG_ENABLE control printfS */     
    printfS("MCU is running, HCLK=%dHz, PCLK=%dHz\n", System_Get_SystemClock(), System_Get_APBClock());       
}


/*********************************************************************************
* Function    : main
* Description : The application entry point.
* Input       : None
* Output      : None
**********************************************************************************/
int main(void)
{
    System_Init();
    
    Uart_Init();
	UART1_Init();
	UART3_Init();
    
	/* UARTx Tx */
	HAL_UART_Transmit(&UART1_Handle, gu8_UART1Test, strlen((char *)gu8_UART1Test),0);
	HAL_UART_Transmit(&UART3_Handle, gu8_UART3Test, strlen((char *)gu8_UART3Test),0);
	
    /* Select Mode: TEST_LOOP、TEST_UART1_IT、TEST_DMA、TEST_UART_ABORT、TEST_UART2,TEST_UART3*/  
    APP_Uart_Test(TEST_UART2);     
	

    while(1)
    {

    }
}

