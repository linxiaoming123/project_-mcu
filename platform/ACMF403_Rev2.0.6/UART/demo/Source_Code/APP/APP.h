/*
  ******************************************************************************
  * Copyright (c)  2008 - 2022, Shanghai AisinoChip Co.,Ltd .
  * @file    APP.h 
  * @version V1.0.0
  * @date    2022
  * @author  Aisinochip Firmware Team  
  * @brief   UART demo Header code.
  ******************************************************************************        
*/
#ifndef __APP_H__
#define __APP_H__

#include "ACM32Fxx_HAL.h"



typedef enum
{
    TEST_LOOP,
    TEST_UART1_IT,
    TEST_DMA,
	TEST_UART_ABORT,
	TEST_UART2,
	TEST_UART3
}enum_TEST_MODE_t;



/*--------------------extern vars-----------------*/

extern UART_HandleTypeDef  UART1_Handle;
extern UART_HandleTypeDef  UART3_Handle;

/*---------------------functions------------------*/
/*UART1 Init*/
void UART1_Init(void);

/*UART3 Init*/
void UART3_Init(void);

void UARTxReceiveITEnable(void);

void UART2RecvDataHandle(void);

void UART3RecvDataHandle(void);

/* APP_Uart_Test */
void APP_Uart_Test(enum_TEST_MODE_t fe_Mode);

#endif
