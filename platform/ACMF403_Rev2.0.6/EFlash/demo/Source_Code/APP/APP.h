/*
  ******************************************************************************
  * @文件名   APP.h
  * @作者     Chris_Kyle
  * @版本     V1.00
  * @日期     2020年
  * @功能     EFlash Test Demo Header file.
  ******************************************************************************
*/
#ifndef __APP_H__
#define __APP_H__

#include "ACM32Fxx_HAL.h"

/* EFlash_Test */
void EFlash_Test(void);

#endif
