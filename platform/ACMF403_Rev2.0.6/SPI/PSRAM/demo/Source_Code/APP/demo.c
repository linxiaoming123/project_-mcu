#include  "demo.h"

uint8_t test_buf_1[1024*8] PSRAM;

bool debug = true;
volatile uint32_t test_buf[10] PSRAM;
#define BUF_LEN  1024
uint8_t TxBuffer[BUF_LEN];
uint8_t RxBuffer[BUF_LEN];

void msg_result(uint8_t err)
{
    if(err)
        printfS("[FAILED]\r\n");
    else
        printfS("[PASS]\r\n");
}

void hexdump_8(uint8_t *buf, uint32_t len)
{
    uint32_t i;
    for(i = 0; i < len; i++)
        printfS("%02X ", buf[i]);
    printfS("\r\n");
}


void SPI3_IRQHandler(void)
{
    HAL_SPI_IRQHandler(&get_psram_info()->hspi);
}

void HAL_SPI_MspInit(SPI_HandleTypeDef *hspi)
{
    /* 
      NOTE : This function should be modified by the user.
    */
    
    GPIO_InitTypeDef GPIO_Handle; 
    
    /* SPI1 */
    if (hspi->Instance == SPI1)
    {
    }
    /* SPI2 */
    else if (hspi->Instance == SPI2) 
    {
		/* Enable Clock */
		System_Module_Enable(EN_SPI2);

		/* SPI2 CS	 PortB Pin12 */
		/* SPI2 CLK  PortB Pin13 */
		/* SPI2 MOSI PortB Pin15 */
		/* SPI2 MISO PortB Pin14 */
		/* SPI2 WP   PortC Pin7  */
		/* SPI2 HOLD PortC Pin6  */
		
		GPIO_Handle.Pin 		   = GPIO_PIN_12 | GPIO_PIN_13 | GPIO_PIN_14 | GPIO_PIN_15;
		GPIO_Handle.Mode		   = GPIO_MODE_AF_PP;
		GPIO_Handle.Pull		   = GPIO_PULLUP;
		GPIO_Handle.Alternate	   = GPIO_FUNCTION_4;
		HAL_GPIO_Init(GPIOB, &GPIO_Handle);

		if (hspi->Init.X_Mode == SPI_4X_MODE) 
		{
			/* SPI2 IO3 PortC Pin6 */
			/* SPI2 IO2 PortC Pin7 */
			GPIO_Handle.Pin 		   = GPIO_PIN_6 | GPIO_PIN_7;
			GPIO_Handle.Mode		   = GPIO_MODE_AF_PP;
			GPIO_Handle.Pull		   = GPIO_PULLUP;
			GPIO_Handle.Alternate	   = GPIO_FUNCTION_2;
			HAL_GPIO_Init(GPIOC, &GPIO_Handle);
		}

#if 1
		SCU->PBSTR |= ((1<<(11*2)) | (1<<(12*2)) | (1<<(13*2)) | (1<<(14*2)));
		SCU->PCSTR |= ((1<<(6*2)) | (1<<(7*2)));

#endif

		/* Clear Pending Interrupt */
		NVIC_ClearPendingIRQ(SPI2_IRQn);
		
		/* Enable External Interrupt */
		NVIC_EnableIRQ(SPI2_IRQn);

    }
    /* SPI3 */
    else if (hspi->Instance == SPI3)
    {
        System_Module_Enable(EN_SPI3);
        System_Module_Enable(EN_GPIOCD);
            
        GPIO_Handle.Pin            = GPIO_PIN_13;
        GPIO_Handle.Mode           = GPIO_MODE_AF_PP;
        GPIO_Handle.Pull           = GPIO_PULLUP;
        GPIO_Handle.Alternate      = GPIO_FUNCTION_6;
        HAL_GPIO_Init(GPIOD, &GPIO_Handle);
        
            
        GPIO_Handle.Pin            = GPIO_PIN_8 | GPIO_PIN_9 | GPIO_PIN_10 | GPIO_PIN_11 | GPIO_PIN_12;
        GPIO_Handle.Mode           = GPIO_MODE_AF_PP;
        GPIO_Handle.Pull           = GPIO_PULLUP;
        GPIO_Handle.Alternate      = GPIO_FUNCTION_3;
        HAL_GPIO_Init(GPIOC, &GPIO_Handle);
        
        /* Clear Pending Interrupt */
		NVIC_ClearPendingIRQ(SPI3_IRQn);
		
		/* Enable External Interrupt */
		NVIC_EnableIRQ(SPI3_IRQn);
    }
}

void psram_demo_test(void)
{
    uint16_t id;
    uint8_t eid[6];
    uint8_t err = 0;
    uint32_t i;
    uint32_t *paddr = (uint32_t*)&test_buf_1[0];
    
    volatile psram_info_t *ppsram = get_psram_info();
    
    IC_APS1604M_Init(SPI3, DMA_Channel0, DMA_Channel1);
    
    id = IC_APS1604M_Read_ID(&eid[0]);
    printfS("id = %02X ", (id >> 8) & 0xFF);
    printfS("%02X ", (id) & 0xFF);
    hexdump_8(eid, 6);

    
    printfS("%-40s", "PSRAM FIFO read/write 1x mode");
    for(i = 0; i < BUF_LEN; i++)
        TxBuffer[i] = 1 + i;
        
    IC_APS1604M_Write((uint32_t)&test_buf_1[0], TxBuffer, BUF_LEN);
    
    IC_APS1604M_Read((uint32_t)&test_buf_1[0], RxBuffer, BUF_LEN);
    
    for(i = 0; i < BUF_LEN; i++)
    {
        if(TxBuffer[i] != RxBuffer[i])
        {
            err = 1;
            break;
        }
    }    
    msg_result(err);
    
    
    /////////////////////////////////////////////////////////
    /*
        xip read write
    */
    printfS("%-40s", "PSRAM XIP read/write");
    err = 0;
    memset(RxBuffer, 0, BUF_LEN);
    paddr = (uint32_t*)&test_buf_1[0];
    for(i = 0; i < BUF_LEN; i++)
        TxBuffer[i] = 2 + i;
    
    for(i = 0; i < BUF_LEN; i += 4)
    {
        /* XIP write only support word write*/
        *paddr++ = *(uint32_t*)&TxBuffer[i];
    }
    
    for(i = 0; i < BUF_LEN; i++)
    {
        RxBuffer[i] = test_buf_1[i];
    }
    
    for(i = 0; i < BUF_LEN; i++)
    {
        if(TxBuffer[i] != RxBuffer[i])
        {
            err = 1;
            break;
        }
    }
    msg_result(err);
    
    
    ////////////////////////////////////////////////////////
    printfS("%-40s", "PSRAM FIFO read/write 4x mode");
    err = 0;
    memset(RxBuffer, 0, BUF_LEN);
    
    for(i = 0; i < BUF_LEN; i++)
        TxBuffer[i] = 3 + i;

    IC_APS1604M_QPI_Write((uint32_t)&test_buf_1[0], TxBuffer, BUF_LEN);

    IC_APS1604M_QPI_Read((uint32_t)&test_buf_1[0], RxBuffer, BUF_LEN);
    
    for(i = 0; i < BUF_LEN; i++)
    {
        if(TxBuffer[i] != RxBuffer[i])
        {
            err = 1;
            break;
        }
    }
    msg_result(err);
    
//    IC_APS1604M_DMA_RW_Cfg(DMA_Channel0, DMA_Channel1);
    
    
    /////////////////////////////////////////////////////////////////////////////////////////////
    printfS("%-40s", "PSRAM FIFO DMA read/write 1x mode");
    err = 0;
    memset(RxBuffer, 0, BUF_LEN);
    
    for(i = 0; i < BUF_LEN; i++)
            TxBuffer[i] = 4 + i;
    
    IC_APS1604M_DMA_Write((uint32_t)&test_buf_1[0], TxBuffer, BUF_LEN);
    
    IC_APS1604M_DMA_Read((uint32_t)&test_buf_1[0], RxBuffer, BUF_LEN);
    
    for(i = 0; i < BUF_LEN; i++)
    {
        if(TxBuffer[i] != RxBuffer[i])
        {
            err = 1;
            break;
        }
    }
    msg_result(err);
    
    
    ////////////////////////////////////////////////////////
    printfS("%-40s", "PSRAM FIFO read/write 4x mode");
    err = 0;
    memset(RxBuffer, 0, BUF_LEN);
        
    if(debug == true)
    {
        for(i = 0; i < BUF_LEN; i++)
            TxBuffer[i] = 5 + i;
    
        IC_APS1604M_QPI_Write((uint32_t)&test_buf_1[0], TxBuffer, BUF_LEN);
    
        IC_APS1604M_QPI_Read((uint32_t)&test_buf_1[0], RxBuffer, BUF_LEN);
    }
    
    for(i = 0; i < BUF_LEN; i++)
    {
        if(TxBuffer[i] != RxBuffer[i])
        {
            err = 1;
            break;
        }
    }
    msg_result(err);
    
    
    /////////////////////////////////////////////////////////////////////////////////////////////
    printfS("%-40s", "PSRAM FIFO DMA read/write 1x mode");
    err = 0;
    memset(RxBuffer, 0, BUF_LEN);  
    for(i = 0; i < BUF_LEN; i++)
            TxBuffer[i] = 6 + i;
    
    IC_APS1604M_DMA_Write((uint32_t)&test_buf_1[0], TxBuffer, BUF_LEN);
    
    IC_APS1604M_DMA_Read((uint32_t)&test_buf_1[0], RxBuffer, BUF_LEN);
    
    for(i = 0; i < BUF_LEN; i++)
    {
        if(TxBuffer[i] != RxBuffer[i])
        {
            err = 1;
            break;
        }
    }
    msg_result(err);
    
    
    ////////////////////////////////////////////////////////////////////////////////////////////    
    printfS("%-40s", "PSRAM FIFO DMA read/write 4x mode");
    err = 0;
    memset(RxBuffer, 0, BUF_LEN);
    for(i = 0; i < BUF_LEN; i++)
            TxBuffer[i] = 7 + i;
    
    IC_APS1604M_QPI_DMA_Write((uint32_t)&test_buf_1[0], TxBuffer, BUF_LEN);
    
    IC_APS1604M_QPI_DMA_Read((uint32_t)&test_buf_1[0], RxBuffer, BUF_LEN);
    
    for(i = 0; i < BUF_LEN; i++)
    {
        if(TxBuffer[i] != RxBuffer[i])
        {
            err = 1;
            break;
        }
    }
    msg_result(err);
    
    
    ////////////////////////////////////////////////////////
    printfS("%-40s", "PSRAM FIFO read/write 4x mode");
    err = 0;
    memset(RxBuffer, 0, BUF_LEN);
        
    if(debug == true)
    {
        for(i = 0; i < BUF_LEN; i++)
            TxBuffer[i] = 8 + i;
    
        IC_APS1604M_QPI_Write((uint32_t)&test_buf_1[0], TxBuffer, BUF_LEN);
    
        IC_APS1604M_QPI_Read((uint32_t)&test_buf_1[0], RxBuffer, BUF_LEN);
    }
    
    for(i = 0; i < BUF_LEN; i++)
    {
        if(TxBuffer[i] != RxBuffer[i])
        {
            err = 1;
            break;
        }
    }
    msg_result(err);
    
    ////////////////////////////////////////////////////////////////////////////////////////////    
    printfS("%-40s", "PSRAM FIFO DMA read/write 4x mode");
    err = 0;
    memset(RxBuffer, 0, BUF_LEN);
    for(i = 0; i < BUF_LEN; i++)
            TxBuffer[i] = 9 + i;
    
    IC_APS1604M_QPI_DMA_Write((uint32_t)&test_buf_1[0], TxBuffer, BUF_LEN);
    
    IC_APS1604M_QPI_DMA_Read((uint32_t)&test_buf_1[0], RxBuffer, BUF_LEN);
    
    for(i = 0; i < BUF_LEN; i++)
    {
        if(TxBuffer[i] != RxBuffer[i])
        {
            err = 1;
            break;
        }
    }
    msg_result(err);
    
    //////////////////////////////////////////////////////////////////////////////////////////// 
    printfS("%-40s", "PSRAM FIFO DMA read/write 1x mode");
    err = 0;
    memset(RxBuffer, 0, BUF_LEN);    
    for(i = 0; i < BUF_LEN; i++)
            TxBuffer[i] = 10 + i;
    
    IC_APS1604M_DMA_Write((uint32_t)&test_buf_1[0], TxBuffer, BUF_LEN);
    
    IC_APS1604M_DMA_Read((uint32_t)&test_buf_1[0], RxBuffer, BUF_LEN);
    
    for(i = 0; i < BUF_LEN; i++)
    {
        if(TxBuffer[i] != RxBuffer[i])
        {
            err = 1;
            break;
        }
    }
    msg_result(err);
    
    /////////////////////////////////////////////////////////
    /*
        xip read write
    */
    printfS("%-40s", "PSRAM XIP read/write");
    err = 0;
    paddr = (uint32_t*)&test_buf_1[0];
    memset(RxBuffer, 0, BUF_LEN);
    for(i = 0; i < BUF_LEN; i++)
        TxBuffer[i] = 11 + i;
    
    for(i = 0; i < BUF_LEN; i += 4)
    {
        /* XIP write only support word write*/
        *paddr++ = *(uint32_t*)&TxBuffer[i];
    }
    
    for(i = 0; i < BUF_LEN; i++)
    {
        RxBuffer[i] = test_buf_1[i];
    }
    
    for(i = 0; i < BUF_LEN; i++)
    {
        if(TxBuffer[i] != RxBuffer[i])
        {
            err = 1;
            break;
        }
    }
    msg_result(err);
    
}


