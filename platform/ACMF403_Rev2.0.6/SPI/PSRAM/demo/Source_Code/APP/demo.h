#ifndef __DEMO_H__
#define __DEMO_H__

#include  "ACM32Fxx_HAL.h"
#include "IC_APS1604M.h"
#include "HAL_DMA.h"

#define SPI_PSRAM_CS_PORT						GPIOD
#define SPI_PSRAM_CS_PIN						GPIO_PIN_13

#define SPI_PSRAM_CS_H()						HAL_GPIO_WritePin(SPI_PSRAM_CS_PORT, SPI_PSRAM_CS_PIN, GPIO_PIN_SET)
#define SPI_PSRAM_CS_L()						HAL_GPIO_WritePin(SPI_PSRAM_CS_PORT, SPI_PSRAM_CS_PIN, GPIO_PIN_CLEAR)


void psram_demo_test(void);

#endif



