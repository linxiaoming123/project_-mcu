#include  "demo.h"

#define UART_BAUD_RATE  115200

UART_HandleTypeDef UART1_Handle;
SPI_HandleTypeDef SPI_Handle_Nor;


/************************************************************************
 * function   : Uart_Init
 * Description: Uart Initiation. 
 ************************************************************************/ 
void Uart_Init(void) 
{
    UART1_Handle.Instance        = UART1;    
    UART1_Handle.Init.BaudRate   = UART_BAUD_RATE; 
    UART1_Handle.Init.WordLength = UART_WORDLENGTH_8B;
    UART1_Handle.Init.StopBits   = UART_STOPBITS_1;
    UART1_Handle.Init.Parity     = UART_PARITY_NONE;
    UART1_Handle.Init.Mode       = UART_MODE_TX_RX_DEBUG;
    UART1_Handle.Init.HwFlowCtl  = UART_HWCONTROL_NONE;
    
    HAL_UART_Init(&UART1_Handle);
    
    /*UART_DEBUG_ENABLE control printfS */    
    printfS("MCU is running, HCLK=%dHz, PCLK=%dHz\n", System_Get_SystemClock(), System_Get_APBClock());       
}


void test_gpio_init()
{
	GPIO_InitTypeDef	gpio_cfg = {0};

	/* Initialization GPIO CS pin */
	System_Module_Enable(EN_GPIOEF);
	gpio_cfg.Pin       = GPIO_PIN_7;
	gpio_cfg.Mode      = GPIO_MODE_OUTPUT_PP;
	gpio_cfg.Pull      = GPIO_PULLUP;
	gpio_cfg.Alternate = GPIO_FUNCTION_0;

	HAL_GPIO_Init(GPIOE, &gpio_cfg);

	HAL_GPIO_WritePin(GPIOE, GPIO_PIN_7, GPIO_PIN_SET);
}

/***********************************************************************
 * function: main frame
 * input�� (none)
 * output��(none)
 * return��(none)
 ***********************************************************************/
int main(void)  
{
    System_Init(); 

    Uart_Init();

	test_gpio_init();
	
    psram_demo_test();
    
    printfS("PSRAM demo finished\r\n\r\n");
    
    while(1)
    {

    }
}
