
#include "ACM32Fxx_HAL.h"

extern UART_HandleTypeDef UART2_Handle;

void UART_SendBytes(uint8_t *pdata, uint32_t len)
{
    while(len--)
    {
        UART2_Handle.Instance->DR = *pdata++;          
        while (!(UART2_Handle.Instance->FR & UART_FR_TXFE));
    }
}


int xip_func(int a, int b)
{
    int result; 
    unsigned char log_string[] = "Application runs in SPI Flash, IROM2: 0x90000000\n";
    unsigned char func_string[] = "int xip_func(int a, int b)\n";
    unsigned char code_string[] = "result = a+b;\n";
    UART_SendBytes(log_string, strlen((char *)log_string));
    UART_SendBytes(func_string, strlen((char *)func_string));
    UART_SendBytes(code_string, strlen((char *)code_string));
    result = a+b;
    return (result);
}





















