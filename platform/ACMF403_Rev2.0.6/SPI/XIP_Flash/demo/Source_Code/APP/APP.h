/*
  ******************************************************************************
  * @file    APP.h
  * @author  LWQ
  * @version V1.0.0
  * @date    2023
  * @brief   XIP demo Header file.
  ******************************************************************************
*/
#ifndef __APP_H__
#define __APP_H__

#include "ACM32Fxx_HAL.h"


void APP_Test(void);

#endif