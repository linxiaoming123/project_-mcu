/*
  ******************************************************************************
  * @file    APP.c
  * @author  Xiao Han
  * @version V1.0.0
  * @date    2020
  * @brief   SPI demo source code.
  ******************************************************************************
*/
 
#include "APP.h"
#include "IC_W25Qxx.h"
#include "XIP.h"



/************************For SPI NorFlash Test***************************/ 
#define NORFLASH_TEST_ADDR      (0x80000)   //512K
#define BUFFER_LENGTH_NOR    (1024)

uint8_t gu8_TxBuffer_Nor[BUFFER_LENGTH_NOR];
uint8_t gu8_RxBuffer_Nor[BUFFER_LENGTH_NOR];





/************************************************************************
 * function   : SPI_Nor_Flash_Test
 * Description: SPI_Nor_Flash_Test.
 ************************************************************************/ 
void SPI_Nor_Flash_Test(void)
{
    uint32_t i;
    uint16_t lu16_ID;
    uint32_t lu32_ErrCount = 0;

    HAL_SPI_Wire_Config(&SPI_Handle_Nor, SPI_1X_MODE);
    
    /* First Set CS HIGH */
    __SPI_CS_Release(); 
    
    System_Delay(50000);
    

    /*************************************** Test Prepare ***************************************/
    if (SPI_Handle_Nor.Init.X_Mode == SPI_1X_MODE) 
        printfS("SPI TEST_1X_MODE is Start!!! \r\n");
    else if (SPI_Handle_Nor.Init.X_Mode == SPI_2X_MODE) 
        printfS("SPI TEST_2X_MODE is Start!!! \r\n");
    else if (SPI_Handle_Nor.Init.X_Mode == SPI_4X_MODE) 
        printfS("SPI TEST_4X_MODE is Start!!! \r\n");

    /* Read Read Manufacture ID and Device ID */
    lu16_ID = IC_W25Qxx_Read_ID();

    printfS("Get Manufacture ID and Device ID : 0x%04X \r\n", lu16_ID);

    /* Erase Sector */
    IC_W25Qxx_EraseSector(NORFLASH_TEST_ADDR);
    
    IC_W25Qxx_Read_Data(gu8_RxBuffer_Nor, NORFLASH_TEST_ADDR, BUFFER_LENGTH_NOR);

    for (i = 0; i < BUFFER_LENGTH_NOR; i++)
    {
        if (gu8_RxBuffer_Nor[i] != 0xFF) 
        {
            lu32_ErrCount++;
        } 
    }

    if (lu32_ErrCount) 
        printfS("Erase Sector Fail!!! \r\n");
    else 
        printfS("Erase Sector Success!!! \r\n");

    /* Clear Error Count */
    lu32_ErrCount = 0;
    
    for (i = 0; i < BUFFER_LENGTH_NOR; i++)
    {
        gu8_TxBuffer_Nor[i] = i;
    }
    
    /************************************* Test Prepare End **************************************/
    
    switch (SPI_Handle_Nor.Init.X_Mode)
    {
        case SPI_1X_MODE: 
        {
            IC_W25Qxx_PageProgram(gu8_TxBuffer_Nor, NORFLASH_TEST_ADDR,   256);
            IC_W25Qxx_PageProgram(gu8_TxBuffer_Nor, NORFLASH_TEST_ADDR+256, 256);
            IC_W25Qxx_PageProgram(gu8_TxBuffer_Nor, NORFLASH_TEST_ADDR+512, 256);
            IC_W25Qxx_PageProgram(gu8_TxBuffer_Nor, NORFLASH_TEST_ADDR+768, 256);
            
            IC_W25Qxx_Read_Data(gu8_RxBuffer_Nor, NORFLASH_TEST_ADDR, BUFFER_LENGTH_NOR);
            
            for (i = 0; i < BUFFER_LENGTH_NOR; i++)
            {
                if (gu8_TxBuffer_Nor[i] != gu8_RxBuffer_Nor[i]) 
                {
                    lu32_ErrCount++;
                } 
            }
            
            if (lu32_ErrCount) 
                printfS("SPI TEST_1X_MODE is Fail!!! \r\n");
            else 
                printfS("SPI TEST_1X_MODE is Success!!! \r\n");
        }break;

        case SPI_2X_MODE: 
        {
            IC_W25Qxx_PageProgram(gu8_TxBuffer_Nor, NORFLASH_TEST_ADDR,   256);
            IC_W25Qxx_PageProgram(gu8_TxBuffer_Nor, NORFLASH_TEST_ADDR+256, 256);
            IC_W25Qxx_PageProgram(gu8_TxBuffer_Nor, NORFLASH_TEST_ADDR+512, 256);
            IC_W25Qxx_PageProgram(gu8_TxBuffer_Nor, NORFLASH_TEST_ADDR+768, 256);
            
            IC_W25Qxx_Read_Dual_Output(gu8_RxBuffer_Nor, NORFLASH_TEST_ADDR, BUFFER_LENGTH_NOR);
            
            for (i = 0; i < BUFFER_LENGTH_NOR; i++)
            {
                if (gu8_TxBuffer_Nor[i] != gu8_RxBuffer_Nor[i]) 
                {
                    lu32_ErrCount++;
                } 
            }

            if (lu32_ErrCount) 
                printfS("SPI TEST_2X_MODE is Fail!!! \r\n");
            else 
                printfS("SPI TEST_2X_MODE is Success!!! \r\n");
        }break;

        case SPI_4X_MODE: 
        {
            IC_W25Qxx_QuadConfig(true);
            
            IC_W25Qxx_PageProgram_Quad(gu8_TxBuffer_Nor, NORFLASH_TEST_ADDR,   256);
            IC_W25Qxx_PageProgram_Quad(gu8_TxBuffer_Nor, NORFLASH_TEST_ADDR+256, 256);
            IC_W25Qxx_PageProgram_Quad(gu8_TxBuffer_Nor, NORFLASH_TEST_ADDR+512, 256);
            IC_W25Qxx_PageProgram_Quad(gu8_TxBuffer_Nor, NORFLASH_TEST_ADDR+768, 256);
            
            IC_W25Qxx_Read_Quad_Output(gu8_RxBuffer_Nor, NORFLASH_TEST_ADDR, BUFFER_LENGTH_NOR);
            
            for (i = 0; i < BUFFER_LENGTH_NOR; i++)
            {
                if (gu8_TxBuffer_Nor[i] != gu8_RxBuffer_Nor[i]) 
                {
                    lu32_ErrCount++;
                } 
            }

            if (lu32_ErrCount) 
                printfS("SPI TEST_4X_MODE is Fail!!! \r\n");
            else 
                printfS("SPI TEST_4X_MODE is Success!!! \r\n");

            IC_W25Qxx_QuadConfig(false);
        }break;

        default: break; 
    }
}


extern int xip_func(int a, int b);

/************************************************************************
 * function   : APP_Test
 * Description: APP Test. 
 ************************************************************************/ 
void APP_Test(void)
{
    int result;
    
    SPI_Init();
    
    SPI_Nor_Flash_Test();
    
    XIP_Init();
     
    result = xip_func(1,2); 
    
    printfS("The result = %d .\n", result);
}
