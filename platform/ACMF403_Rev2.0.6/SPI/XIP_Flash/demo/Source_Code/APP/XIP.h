

#ifndef __XIP_H__
#define __XIP_H__

#include "ACM32Fxx_HAL.h"



#if defined External_FLASH
    #define SPI_CS_PROT     GPIOA
    #define SPI_CS_PIN      GPIO_PIN_15
#elif defined Internal_FLASH  
    #define SPI_CS_PROT     GPIOD
    #define SPI_CS_PIN      GPIO_PIN_13
#endif


extern SPI_HandleTypeDef SPI_Handle_Nor;



void SPI_Init(void);
void XIP_Init(void);
void __SPI_SET_1X_MODE(void);
void __SPI_SET_2X_MODE(void);
void __SPI_SET_4X_MODE(void);
void __SPI_CS_Select(void);
void __SPI_CS_Release(void);
uint8_t __SPI_Write_Data(uint8_t *pData, uint32_t Size);
uint8_t __SPI_Read_Data(uint8_t *pData, uint32_t Size);



#endif



