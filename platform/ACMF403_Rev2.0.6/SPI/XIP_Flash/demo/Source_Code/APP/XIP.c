
 
#include "XIP.h"
#include "IC_W25Qxx.h"



#define SPI_NOR_FLASH_BASE      0x90000000    

SPI_HandleTypeDef SPI_Handle_Nor;

/************************************************************************
 * function   : HAL_SPI_MspInit
 * Description: SPI inition for interal Nor Flash.
 * input      : hspi : pointer to a SPI_HandleTypeDef structure that contains
 *                     the configuration information for SPI module
 ************************************************************************/
void HAL_SPI_MspInit(SPI_HandleTypeDef *hspi)
{
    GPIO_InitTypeDef GPIO_Handle; 
    
    /* SPI3 */
    if (hspi->Instance == SPI3)
    {
        /* Enable Clock */
        System_Module_Enable(EN_SPI3);      
        
#if defined External_FLASH
        /* SPI3 CS   PortA Pin15    The CS set as GPIO mode*/
        /* SPI3 CLK  PortC Pin10 */
        /* SPI3 MISO PortC Pin11 */
        /* SPI3 MOSI PortC Pin12 */
        GPIO_Handle.Pin            = GPIO_PIN_10 | GPIO_PIN_11 | GPIO_PIN_12;
        GPIO_Handle.Mode           = GPIO_MODE_AF_PP;
        GPIO_Handle.Pull           = GPIO_PULLUP;
        GPIO_Handle.Alternate      = GPIO_FUNCTION_3;
        HAL_GPIO_Init(GPIOC, &GPIO_Handle);    
        
#ifdef HARDWARE_SPI_CS
        GPIO_Handle.Mode           = GPIO_MODE_AF_PP;   
        GPIO_Handle.Alternate      = GPIO_FUNCTION_5;  
#else  
        /* CS USE Software Control */
        GPIO_Handle.Mode           = GPIO_MODE_OUTPUT_PP;   //The CS set as GPIO mode  
        GPIO_Handle.Alternate      = GPIO_FUNCTION_0; 
#endif
        GPIO_Handle.Pin            = SPI_CS_PIN;
        GPIO_Handle.Pull           = GPIO_PULLUP; 
        HAL_GPIO_Init(SPI_CS_PROT, &GPIO_Handle);        
        
        printfS("SPI3 PINs selected:\r\n");
        printfS("SPI3 CS  :PA15\r\n");
        printfS("SPI3 SCK :PC10\r\n");
        printfS("SPI3 MOSI:PC12\r\n");
        printfS("SPI3 MISO:PC11\r\n");
        
        if (hspi->Init.X_Mode == SPI_4X_MODE) 
        {
            /* SPI3 IO3 PortC Pin8 */
            /* SPI3 IO2 PortC Pin9 */
            GPIO_Handle.Pin            = GPIO_PIN_8 | GPIO_PIN_9;
            GPIO_Handle.Mode           = GPIO_MODE_AF_PP;
            GPIO_Handle.Pull           = GPIO_PULLUP;
            GPIO_Handle.Alternate      = GPIO_FUNCTION_3;
            HAL_GPIO_Init(GPIOC, &GPIO_Handle);
            printfS("SPI3 WP(IO2)   :PC9\r\n");
            printfS("SPI3 HOLD(IO3) :PC8\r\n");
        }
#elif defined Internal_FLASH         
        /* SPI3 CS   PortD Pin13    The CS set as GPIO mode*/
        /* SPI3 CLK  PortE Pin3 */
        /* SPI3 MOSI PortE Pin2 */
        /* SPI3 MISO PortD Pin14 */
        GPIO_Handle.Pin            = GPIO_PIN_2 | GPIO_PIN_3;
        GPIO_Handle.Mode           = GPIO_MODE_AF_PP;
        GPIO_Handle.Pull           = GPIO_PULLUP;
        GPIO_Handle.Alternate      = GPIO_FUNCTION_6;
        HAL_GPIO_Init(GPIOE, &GPIO_Handle);
        
        GPIO_Handle.Pin            = GPIO_PIN_14;
        GPIO_Handle.Mode           = GPIO_MODE_AF_PP;
        GPIO_Handle.Pull           = GPIO_PULLUP;
        GPIO_Handle.Alternate      = GPIO_FUNCTION_6;
        HAL_GPIO_Init(GPIOD, &GPIO_Handle);

#ifdef HARDWARE_SPI_CS
        GPIO_Handle.Mode           = GPIO_MODE_AF_PP;   
        GPIO_Handle.Alternate      = GPIO_FUNCTION_6;  
#else  
        /* CS USE Software Control */
        GPIO_Handle.Mode           = GPIO_MODE_OUTPUT_PP;   //The CS set as GPIO mode  
        GPIO_Handle.Alternate      = GPIO_FUNCTION_0; 
#endif
        GPIO_Handle.Pin            = SPI_CS_PIN;
        GPIO_Handle.Pull           = GPIO_PULLUP; 
        HAL_GPIO_Init(SPI_CS_PROT, &GPIO_Handle);
        
        printfS("SPI3 PINs selected:\r\n");
        printfS("SPI3 CS  :PD13\r\n");
        printfS("SPI3 SCK :PE3\r\n");
        printfS("SPI3 MOSI:PE2\r\n");
        printfS("SPI3 MISO:PD14\r\n");
        
        if (hspi->Init.X_Mode == SPI_4X_MODE) 
        {
            /* SPI3 IO3 PortE Pin4 */
            /* SPI3 IO2 PortC Pin9 */
            GPIO_Handle.Pin            = GPIO_PIN_4;
            GPIO_Handle.Mode           = GPIO_MODE_AF_PP;
            GPIO_Handle.Pull           = GPIO_PULLUP;
            GPIO_Handle.Alternate       = GPIO_FUNCTION_6;
            HAL_GPIO_Init(GPIOE, &GPIO_Handle);
            
            GPIO_Handle.Pin            = GPIO_PIN_9;
            GPIO_Handle.Mode           = GPIO_MODE_AF_PP;
            GPIO_Handle.Pull           = GPIO_PULLUP;
            GPIO_Handle.Alternate       = GPIO_FUNCTION_3;
            HAL_GPIO_Init(GPIOC, &GPIO_Handle);
            printfS("SPI3 WP(IO2)   :PC9\r\n");
            printfS("SPI3 HOLD(IO3) :PE4\r\n");
        }
#endif
        
        /* Clear Pending Interrupt */
        NVIC_ClearPendingIRQ(SPI3_IRQn);
        
        /* Enable External Interrupt */
        NVIC_EnableIRQ(SPI3_IRQn);
    }
}
/************************************************************************
 * function   : SPI_Init
 * Description: SPI Initiation For SPI NorFlash. 
 ************************************************************************/ 
void SPI_Init(void)
{
    uint32_t lu32_TestMode = 0;
    
    SPI_Handle_Nor.Instance                 = SPI3;
    SPI_Handle_Nor.Init.SPI_Mode            = SPI_MODE_MASTER;
    SPI_Handle_Nor.Init.SPI_Work_Mode       = SPI_WORK_MODE_0;
    SPI_Handle_Nor.Init.X_Mode              = SPI_4X_MODE;
    SPI_Handle_Nor.Init.First_Bit           = SPI_FIRSTBIT_MSB;
    SPI_Handle_Nor.Init.BaudRate_Prescaler  = SPI_BAUDRATE_PRESCALER_4;

    HAL_SPI_Init(&SPI_Handle_Nor);
    
    if (SPI_Handle_Nor.Init.X_Mode == SPI_1X_MODE) 
        lu32_TestMode = 1;
    else if (SPI_Handle_Nor.Init.X_Mode == SPI_2X_MODE) 
        lu32_TestMode = 2;
    else if (SPI_Handle_Nor.Init.X_Mode == SPI_4X_MODE) 
        lu32_TestMode = 4;
    printfS("SPI X_Mode = %d, SPI Prescaler = %d, SPI CLK = %dHZ\r\n", lu32_TestMode, SPI_Handle_Nor.Instance->BAUD, System_Get_SystemClock()/SPI_Handle_Nor.Instance->BAUD);
}

/************************************************************************
 * function   : XIP_Init
 * Description: XIP Initiation For SPI NorFlash. 
 ************************************************************************/ 
void XIP_Init(void)
{
    HAL_SPI_Wire_Config(&SPI_Handle_Nor, SPI_1X_MODE);
    
    /* First Set CS HIGH */
    __SPI_CS_Release(); 
    
    System_Delay(50000);  
  
    IC_W25Qxx_QuadConfig(true);  
    
	SPI_Handle_Nor.Instance->CTL = (SPI_Handle_Nor.Instance->CTL & ~(0x3<<5)) | (2 << 5); //4Xģʽ 
	//Addr_width:24bit, PARA_NO2:8bit, PARA_NO1:disable, Con_Rd_EN:enable, Para_Ord2:1,  Para_Ord1:0, 
	SPI_Handle_Nor.Instance->MEMO_ACC = 0x18 << 14 | 0x08 << 9 | 0x00 << 5 | 0x01 << 3 | 0x01 << 2 | 0x00 << 1;    
	SPI_Handle_Nor.Instance->CMD = 0x00 << 8 | QUAD_OUTPUT_FAST_READ << 0; //Wr_Cmd:0x00, Rd_Cmd: QUAD_OUT_FAST_READ
	SPI_Handle_Nor.Instance->PARA = 0x55 << 8 | 0x00 << 0; //Para2:0x55, Para1:0x00
	SPI_Handle_Nor.Instance->MEMO_ACC |= 0x01;//ʹ��
}


/************************************************************************
 * function   : __SPI_Write_Data 
 * Description: SPI Transmit an amount of data. 
 ************************************************************************/
uint8_t __SPI_Write_Data(uint8_t *pData, uint32_t Size)
{
    HAL_StatusTypeDef Status = HAL_OK;
    uint32_t i;
    __IO uint32_t uiTimeout;

    /* Check SPI Parameter */
    if (!IS_SPI_ALL_INSTANCE(SPI_Handle_Nor.Instance))    return HAL_ERROR;
    if(!Size)    return HAL_ERROR;
    if (pData == NULL)    return HAL_ERROR;
    
    SPI_Handle_Nor.Tx_Count = 0;
    SPI_Handle_Nor.Tx_Size = Size;
    SPI_Handle_Nor.Tx_Buffer = pData;
    
    uiTimeout = 0;

    /* Clear Batch Done Flag  */
    SET_BIT(SPI_Handle_Nor.Instance->STATUS, SPI_STATUS_TX_BATCH_DONE);
    SET_BIT(SPI_Handle_Nor.Instance->STATUS, SPI_STATUS_BATCH_DONE);
    
    /* Clear TX FIFO */
    SET_BIT(SPI_Handle_Nor.Instance->TX_CTL, SPI_TX_CTL_FIFO_RESET);
    CLEAR_BIT(SPI_Handle_Nor.Instance->TX_CTL, SPI_TX_CTL_FIFO_RESET);
    
    /* Set Data Size */
    SPI_Handle_Nor.Instance->BATCH = Size;
    
    /* Tx Enable */
    SPI_Handle_Nor.Instance->TX_CTL |= SPI_TX_CTL_EN;

    if (SPI_Handle_Nor.Init.SPI_Mode == SPI_MODE_MASTER) 
    {
        /* Transmit Start */
        SPI_Handle_Nor.Instance->CS |= SPI_CS_CS0;
    }
    else
    {
        /* Rx Disable */
        SPI_Handle_Nor.Instance->RX_CTL &= (~SPI_RX_CTL_EN);
    }
    
    while(SPI_Handle_Nor.Tx_Size > 0)
    {
        /* Wait Tx FIFO Not Full */
        while (SPI_Handle_Nor.Instance->STATUS & SPI_STATUS_TX_FIFO_FULL)
        {
            if(uiTimeout)
            {
                uiTimeout--;
                if (uiTimeout == 0)
                {
                    Status = HAL_TIMEOUT;
                    goto End;
                }
            }
        }        
        SPI_Handle_Nor.Instance->DAT = SPI_Handle_Nor.Tx_Buffer[SPI_Handle_Nor.Tx_Count++];
        SPI_Handle_Nor.Tx_Size--;
        uiTimeout = 0;
    }
    
    if (SPI_Handle_Nor.Init.SPI_Mode == SPI_MODE_SLAVE) 
    {
        /* Wait Transmit Done */
        while (!(SPI_Handle_Nor.Instance->STATUS & SPI_STATUS_TX_BUSY));
        while (SPI_Handle_Nor.Instance->STATUS & SPI_STATUS_TX_BUSY)
        {
            if(uiTimeout)
            {
                uiTimeout--;
                if (uiTimeout == 0)
                {
                    Status = HAL_TIMEOUT;
                    goto End;
                }
            }
        }
    }
    else
    {
        /* Wait Transmit Done */
        while (!(SPI_Handle_Nor.Instance->STATUS & SPI_STATUS_TX_BATCH_DONE));
        Status = HAL_OK;
    }
    
End:
    /* Clear Batch Done Flag  */
    SET_BIT(SPI_Handle_Nor.Instance->STATUS, SPI_STATUS_TX_BATCH_DONE);
    SET_BIT(SPI_Handle_Nor.Instance->STATUS, SPI_STATUS_BATCH_DONE);

    /* Tx Disable */
    SPI_Handle_Nor.Instance->TX_CTL &= (~SPI_TX_CTL_EN);
    
    
    //Status = HAL_SPI_Transmit(&SPI_Handle_Nor, pData, Size, 0);
    
    
    return Status;
}


/************************************************************************
 * function   : __SPI_Read_Data 
 * Description: SPI Receive an amount of data. 
 ************************************************************************/
uint8_t __SPI_Read_Data(uint8_t *pData, uint32_t Size)
{
    HAL_StatusTypeDef Status = HAL_OK; 
    Status = HAL_SPI_Receive(&SPI_Handle_Nor, pData, Size, 0);
    return Status;
}

/************************************************************************
 * function   : __SPI_SET_1X_MODE 
 * Description: SPI 1-wire mode. 
 ************************************************************************/ 
void __SPI_SET_1X_MODE(void)
{
    HAL_SPI_Wire_Config(&SPI_Handle_Nor, SPI_1X_MODE);
}

/************************************************************************
 * function   : __SPI_SET_2X_MODE 
 * Description: SPI 2-wire mode. 
 ************************************************************************/ 
void __SPI_SET_2X_MODE(void)
{                         
    HAL_SPI_Wire_Config(&SPI_Handle_Nor, SPI_2X_MODE);
}

/************************************************************************
 * function   : __SPI_SET_4X_MODE 
 * Description: SPI 4-wire mode. 
 ************************************************************************/ 
void __SPI_SET_4X_MODE(void)
{                         
    HAL_SPI_Wire_Config(&SPI_Handle_Nor, SPI_4X_MODE);
}

/************************************************************************
 * function   : __SPI_CS_Select 
 * Description: SPI CS Select. 
 ************************************************************************/ 
void __SPI_CS_Select(void)
{
#ifdef HARDWARE_SPI_CS
    SPI_Handle_Nor.Instance->CS |= SPI_CS_CS0; 
#else
    HAL_GPIO_WritePin(SPI_CS_PROT, SPI_CS_PIN, GPIO_PIN_CLEAR);
#endif
}

/************************************************************************
 * function   : __SPI_CS_Release 
 * Description: SPI CS Release. 
 ************************************************************************/ 
void __SPI_CS_Release(void) 
{
#ifdef HARDWARE_SPI_CS
    SPI_Handle_Nor.Instance->CS &= (~SPI_CS_CS0);   
#else
    HAL_GPIO_WritePin(SPI_CS_PROT, SPI_CS_PIN, GPIO_PIN_SET);
#endif
}

  