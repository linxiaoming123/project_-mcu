/*
  ******************************************************************************
  * Copyright (c)  2008 - 2022, Shanghai AisinoChip Co.,Ltd .
  * @file    APP.h 
  * @version V1.0.0
  * @date    2022
  * @author  Aisinochip Firmware Team  
  * @brief   Standby demo Header code.
  ******************************************************************************        
*/
#ifndef __APP_H__
#define __APP_H__

#include "ACM32Fxx_HAL.h"

/* APP_Standby_Test */
void APP_Standby_Test(void);

#endif
