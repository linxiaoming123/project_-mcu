/*
  ******************************************************************************
  * @file    APP_IWDT.h
  * @author  CWT
  * @version V1.0.0
  * @date    2020
  * @brief   IWDT demo Header file.
  ******************************************************************************
*/
#ifndef __APP_IWDT_H__
#define __APP_IWDT_H__

#include "ACM32Fxx_HAL.h"
void IWDT_Reset_Test(void);


#endif
