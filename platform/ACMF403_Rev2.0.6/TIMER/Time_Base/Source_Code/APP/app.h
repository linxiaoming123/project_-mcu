/***********************************************************************
 * Filename    : app.h
 * Description : app header file
 * Author(s)   : xwl  
 * version     : V1.0
 * Modify date : 2019-09-24
 ***********************************************************************/
#ifndef __APP_H__
#define __APP_H__

#include "ACM32Fxx_HAL.h"   


/************************************************************************
* function   : Uart_Test
* Description: uart test.  
* input : none 
* return: none 
************************************************************************/ 
extern void Timer_Update_Test(void);              

#endif

