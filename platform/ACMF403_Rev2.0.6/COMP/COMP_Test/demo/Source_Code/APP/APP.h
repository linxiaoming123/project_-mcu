/*
  ******************************************************************************
  * @file    APP.h
  * @author  Xiao Han
  * @version V1.0.0
  * @date    2020
  * @brief   COMP demo Header file.
  ******************************************************************************
*/

#ifndef __APP_H__
#define __APP_H__

#include "ACM32Fxx_HAL.h"

/* Function : COMP_Init */
void COMP_Init(void);
    
/* Function : COMP_Test */
void COMP_Test(void);

#endif
