/*
  ******************************************************************************
  * @file    main.c
  * @brief   main source File.
  ******************************************************************************
*/
#include "ACM32Fxx_HAL.h"  

uint8_t data_buf[1024];

#define UART_BAUD_RATE  115200

UART_HandleTypeDef UART2_Handle;

/************************************************************************
 * function   : Uart_Init
 * Description: Uart Initiation. 
 ************************************************************************/ 
void Uart_Init(void) 
{
    UART2_Handle.Instance        = UART2;    
    UART2_Handle.Init.BaudRate   = UART_BAUD_RATE; 
    UART2_Handle.Init.WordLength = UART_WORDLENGTH_8B;
    UART2_Handle.Init.StopBits   = UART_STOPBITS_1;
    UART2_Handle.Init.Parity     = UART_PARITY_NONE;
    UART2_Handle.Init.Mode       = UART_MODE_TX_RX_DEBUG;
    UART2_Handle.Init.HwFlowCtl  = UART_HWCONTROL_NONE;
    
    HAL_UART_Init(&UART2_Handle);  
    
    /* UART_DEBUG_ENABLE control printfS */     
    printfS("MCU is running, HCLK=%dHz, PCLK=%dHz\n", System_Get_SystemClock(), System_Get_APBClock());       
}

void hrng_test(void)
{
	UINT32 i;
	UINT32 j;

	printfS("---------HRNG test-------\n"); 
    
    HAL_HRNG_Initial();  
	for(i = 1; i < 64; i++)
	{
		printfS("get random number: %d\n", i);
		if(HAL_HRNG_GetHrng(data_buf, i))
		{
			printfS("random number is error\n");
			return;
		}
		for(j = 0; j < i; j++)
		{
			printfS("%x", data_buf[j]);
		}
		printfS("\n");
	}

}

/*********************************************************************************
* Function    : main
* Description : 
* Input       : 
* Outpu       : 
* Author      : Chris_Kyle                         Data : 2020年
**********************************************************************************/
int main(void)
{
    System_Init();
    
    Uart_Init();
	
	printfS("hrng test start\n\n");	
	hrng_test();
	printfS("\nhrng test over\n");	
	
    while(1)
    {
			;
    }
}

