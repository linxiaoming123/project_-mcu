/*
  ******************************************************************************
  * @file    APP.h
  * @author  PJ
  * @version V1.0.0
  * @date    2021
  * @brief   
  ******************************************************************************
*/
#ifndef __APP_H__
#define __APP_H__

#include "ACM32Fxx_HAL.h"



/*---------------------functions------------------*/
int32_t APP_ARM_Fir_Test(void);

#endif
