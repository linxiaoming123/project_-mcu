/*
  ******************************************************************************
  * @file    main.c
  * @brief   main source File.
  ******************************************************************************
*/
#include "APP.h"

UART_HandleTypeDef Uart2_Handle;

/************************************************************************
 * function   : Uart_Init
 * Description: Uart Initiation. 
 ************************************************************************/ 
void Uart_Init(uint32_t fu32_Baudrate)
{
    Uart2_Handle.Instance = UART2;
    Uart2_Handle.Init.BaudRate   = fu32_Baudrate;
    Uart2_Handle.Init.WordLength = UART_WORDLENGTH_8B;
    Uart2_Handle.Init.StopBits   = UART_STOPBITS_1;
    Uart2_Handle.Init.Parity     = UART_PARITY_NONE;
    Uart2_Handle.Init.Mode       = UART_MODE_TX_RX_DEBUG;
    Uart2_Handle.Init.HwFlowCtl  = UART_HWCONTROL_NONE;

    HAL_UART_Init(&Uart2_Handle);
	
    /* UART_DEBUG_ENABLE control printfS */   
    printfS("MCU is running, HCLK=%dHz, PCLK=%dHz\n", System_Get_SystemClock(), System_Get_APBClock());
}

/*********************************************************************************
* Function    : main
* Description : The application entry point.
* Input       : None
* Output      : None
**********************************************************************************/
int main(void)
{
    System_Init();
    
    Uart_Init(115200);

    /* GPIO_OUTPUT、GPIO_INT、GPIO_PC13、LED_BLINK */
    APP_GPIO_Test(GPIO_INT);
    
    while(1)
    {

    }
}

