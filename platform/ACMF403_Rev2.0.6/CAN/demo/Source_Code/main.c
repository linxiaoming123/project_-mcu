/*
  ******************************************************************************
  * @file    main.c
  * @brief   main source File.
  ******************************************************************************
*/
#include "APP.h"

#define UART_BAUD_RATE  115200

UART_HandleTypeDef UART2_Handle;

/************************************************************************
 * function   : Uart_Init
 * Description: Uart Initiation. 
 ************************************************************************/ 
void Uart_Init(void) 
{
    UART2_Handle.Instance        = UART2;    
    UART2_Handle.Init.BaudRate   = UART_BAUD_RATE; 
    UART2_Handle.Init.WordLength = UART_WORDLENGTH_8B;
    UART2_Handle.Init.StopBits   = UART_STOPBITS_1;
    UART2_Handle.Init.Parity     = UART_PARITY_NONE;
    UART2_Handle.Init.Mode       = UART_MODE_TX_RX_DEBUG;
    UART2_Handle.Init.HwFlowCtl  = UART_HWCONTROL_NONE;
    
    HAL_UART_Init(&UART2_Handle);  
    
    /* UART_DEBUG_ENABLE control printfS */     
    printfS("MCU is running, HCLK=%dHz, PCLK=%dHz\n", System_Get_SystemClock(), System_Get_APBClock());       
}


int main(void)
{
    System_Init();	//when test CAN, you need select XTH as HCLK or as input of PLL. 
    Uart_Init();
		/*
		when use the case TEST_SELF, please modify in function "CAN_Mode_Config()", 
		CAN_Handle.Init.CAN_Mode = CAN_Mode_SelfTest; 
		*/
    APP_CAN_Test(TEST_LOOP);
    while(1)
    {

    }
}

