DMA应用示例工程：

示例demo提供了几个测试项，根据enum_TEST_MODE_t枚举选择特定的功能：

    TEST_M2M_CYCLE：DMA内存到内存搬运循环模式；

    TEST_M2M_STANDARD_IT：DMA内存到内存单次中断模式；

    TEST_M2M_STANDARD_POLLING：DMA内存到内存单次查询模式；
