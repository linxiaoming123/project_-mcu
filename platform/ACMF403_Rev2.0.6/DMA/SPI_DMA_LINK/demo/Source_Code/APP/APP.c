/*
  ******************************************************************************
  * Copyright (c)  2008 - 2022, Shanghai AisinoChip Co.,Ltd .
  * @file    APP.c 
  * @version V1.0.0
  * @date    2022
  * @author  Aisinochip Firmware Team  
  * @brief   DMA demo source code.
  ******************************************************************************        
*/
 
#include  "APP.h"

#define BUFFER_NUMBER    (40)
#define BUFFER_LENGTH    (1024) // 最大4095，如果位宽为字，则为4*4095字节。

__attribute__ ((aligned (4))) uint8_t gu8_Buffer[BUFFER_NUMBER][BUFFER_LENGTH];
__attribute__ ((aligned (4))) DMA_LLI_InitTypeDef g_Link[BUFFER_NUMBER];

SPI_HandleTypeDef SPI_Handle;
DMA_HandleTypeDef DMA_Handle;

static volatile uint32_t gu32_Transfer_Error;
static volatile uint32_t gu32_Transfer_Complete;

/************************************************************************
 * function   : DMA_Link_ITC_Callback
 * Description: DMA transfer complete interrupt Callback. 
 ************************************************************************/ 
static void DMA_Link_ITC_Callback(void)
{
    gu32_Transfer_Complete = 1;
}

/************************************************************************
 * function   : DMA_Link_IE_Callback
 * Description: DMA transfer error interrupt Callback. 
 ************************************************************************/ 
static void DMA_Link_IE_Callback(void)
{
    gu32_Transfer_Error = 1;
    gu32_Transfer_Complete = 1;
}

void SPI_Init(void)
{
    SPI_Handle.Instance                 = SPI3;
    SPI_Handle.Init.SPI_Mode            = SPI_MODE_MASTER;
    SPI_Handle.Init.SPI_Work_Mode       = SPI_WORK_MODE_0;
    SPI_Handle.Init.X_Mode              = SPI_1X_MODE;
    SPI_Handle.Init.First_Bit           = SPI_FIRSTBIT_MSB;
    SPI_Handle.Init.BaudRate_Prescaler  = SPI_BAUDRATE_PRESCALER_4;

    HAL_SPI_Init(&SPI_Handle);
    
    DMA_Handle.Instance              = DMA_Channel0;
    DMA_Handle.Init.Data_Flow        = DMA_DATA_FLOW_M2P;
    DMA_Handle.Init.Request_ID       = REQ47_SPI3_SEND;
    DMA_Handle.Init.Mode             = DMA_NORMAL;
    DMA_Handle.Init.Source_Inc       = DMA_SOURCE_ADDR_INCREASE_ENABLE;
    DMA_Handle.Init.Desination_Inc   = DMA_DST_ADDR_INCREASE_DISABLE;
    DMA_Handle.Init.Source_Width     = DMA_SRC_WIDTH_BYTE;
    DMA_Handle.Init.Desination_Width = DMA_DST_WIDTH_BYTE;
    DMA_Handle.DMA_ITC_Callback      = DMA_Link_ITC_Callback;
    DMA_Handle.DMA_IE_Callback       = DMA_Link_IE_Callback;

    HAL_DMA_Init(&DMA_Handle);
    
    __HAL_LINK_DMA(SPI_Handle, HDMA_Tx, DMA_Handle);

    /* Clear Pending Interrupt */
    NVIC_ClearPendingIRQ(DMA_IRQn);
    
    /* Enable External Interrupt */
    NVIC_EnableIRQ(DMA_IRQn);
}


HAL_StatusTypeDef HAL_DMA_Start_Link(DMA_HandleTypeDef *hdma, DMA_LLI_InitTypeDef *Link)
{
    if (Link == 0)
        return HAL_ERROR;
    
    /* DMA Channel Disable */
    hdma->Instance->CONFIG &= ~DMA_CHANNEL_CONFIG_EN;
	
    /* Set source address and desination address */
    hdma->Instance->SRC_ADDR  = Link->SrcAddr;
    hdma->Instance->DEST_ADDR = Link->DstAddr;

    /* Set Next Link */
    hdma->Instance->LLI = (uint32_t)Link->Next;

    /* Set Transfer Size */
    hdma->Instance->CTRL = Link->Control;

	hdma->Instance->CONFIG |= DMA_CHANNEL_CONFIG_ITC | DMA_CHANNEL_CONFIG_IE;	
	
    /* DMA Channel Enable */
    hdma->Instance->CONFIG |= DMA_CHANNEL_CONFIG_EN;

    return HAL_OK;
}

HAL_StatusTypeDef HAL_SPI_Transmit_DMA_Link(SPI_HandleTypeDef *hspi, DMA_LLI_InitTypeDef *Link, uint32_t Size)
{
    /* Check SPI Parameter */
    if (!IS_SPI_ALL_INSTANCE(hspi->Instance))    return HAL_ERROR;
    
    /* Rx machine is running */
    if (hspi->TxState != SPI_TX_STATE_IDLE) 
    {
        return HAL_ERROR;
    }
    /* Set machine is Sending */
    hspi->TxState = SPI_TX_STATE_SENDING;


    /* Clear Batch Done Flag  */
    SET_BIT(hspi->Instance->STATUS, SPI_STATUS_TX_BATCH_DONE);
    
    /* Set Data Size */
    hspi->Instance->BATCH = Size;
    
    /* Tx FIFO */
    hspi->Instance->TX_CTL &= ~SPI_TX_CTL_DMA_LEVEL;
    hspi->Instance->TX_CTL |= SPI_TX_CTL_DMA_LEVEL_0;    

    /* Tx Enable */
    hspi->Instance->TX_CTL |= SPI_TX_CTL_EN;
    
    HAL_DMA_Start_Link(hspi->HDMA_Tx, Link);

    hspi->Instance->TX_CTL |= SPI_TX_CTL_DMA_REQ_EN;  
    if (hspi->Init.SPI_Mode == SPI_MODE_MASTER)
    {
        /* Transmit Start */
        hspi->Instance->CS |= SPI_CS_CS0;
    }
    else
    {
        /* Rx Disable */
        hspi->Instance->RX_CTL &= (~SPI_RX_CTL_EN);
    }
    
    return HAL_OK;
}

/************************************************************************
 * function   : DMA_M2M_Test
 * Description: DMA memory to memory test. 
 ************************************************************************/ 
void DMA_Link_Test(void)
{
    uint32_t i;
    uint32_t j;
    uint32_t lu32_COM_OK  = 0;
    uint32_t lu32_COM_Err = 0;
    
    printfS("DMA Link test begin. \r\n");

    // 初始化 SPI
    SPI_Init();
    
    // 初始化缓冲区数据
    for (i = 0; i < BUFFER_NUMBER; i++)
    {
        for (j = 0; j < BUFFER_LENGTH; j++)
        {
            gu8_Buffer[i][j] = i + 1;
        }
    }
    
    // 初始化链表
    for (i = 0; i < BUFFER_NUMBER; i++)
    {
        g_Link[i].SrcAddr = (uint32_t)&gu8_Buffer[i][0];
        g_Link[i].DstAddr = (uint32_t)&SPI_Handle.Instance->DAT;
        if (i == (BUFFER_NUMBER - 1))
        {
            // 尾节点，使能中断
            g_Link[i].Control = (uint32_t)(SPI_Handle.HDMA_Tx->Instance->CTRL | DMA_CHANNEL_CTRL_ITC) + BUFFER_LENGTH;
            g_Link[i].Next = (struct DMA_NextLink *)0;
        }
        else
        {
            // 非尾节点，禁止中断
            g_Link[i].Control = (uint32_t)(SPI_Handle.HDMA_Tx->Instance->CTRL & ~DMA_CHANNEL_CTRL_ITC) + BUFFER_LENGTH;
            g_Link[i].Next = &g_Link[i + 1];
        }
    }
    
    gu32_Transfer_Error = 0;
    gu32_Transfer_Complete = 0;
    
    // 发送数据，数量为所有节点的总数量
    HAL_SPI_Transmit_DMA_Link(&SPI_Handle, &g_Link[0], BUFFER_LENGTH * BUFFER_NUMBER);
    
    while (gu32_Transfer_Complete == 0);
    
    if (gu32_Transfer_Error)
    {
        // DMA故障，关闭SPI
        CLEAR_BIT(SPI_Handle.Instance->IE, SPI_STATUS_TX_BATCH_DONE);
        SPI_Handle.Instance->TX_CTL &= (~SPI_TX_CTL_EN);  
        SPI_Handle.Instance->TX_CTL &= (~SPI_TX_CTL_DMA_REQ_EN);         
        SPI_Handle.Instance->CS &= (~SPI_CS_CS0);
        SET_BIT(SPI_Handle.Instance->STATUS, SPI_STATUS_TX_BATCH_DONE);
        SET_BIT(SPI_Handle.Instance->STATUS, SPI_STATUS_BATCH_DONE);
        SPI_Handle.TxState = SPI_TX_STATE_IDLE;
    }
    else
    {
        // 等待SPI发送完成
        while (SPI_Handle.Instance->STATUS & SPI_STATUS_TX_BUSY);
        
        // 关闭SPI
        CLEAR_BIT(SPI_Handle.Instance->IE, SPI_STATUS_TX_BATCH_DONE);
        SET_BIT(SPI_Handle.Instance->STATUS, SPI_STATUS_TX_BATCH_DONE);
        SPI_Handle.Instance->TX_CTL &= (~SPI_TX_CTL_EN);  
        SPI_Handle.Instance->TX_CTL &= (~SPI_TX_CTL_DMA_REQ_EN); 
        SPI_Handle.Instance->CS &= (~SPI_CS_CS0);  
        SPI_Handle.TxState = SPI_TX_STATE_IDLE; 
    }
    
    printfS("DMA Link test end. \r\n");    
}

/************************************************************************
 * function   : DMA_IRQHandler
 * Description: DMA interrupt handler. 
 ************************************************************************/ 
void DMA_IRQHandler(void)
{
    HAL_DMA_IRQHandler(&DMA_Handle);
}





























