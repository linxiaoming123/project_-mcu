/*
  ******************************************************************************
  * Copyright (c)  2008 - 2022, Shanghai AisinoChip Co.,Ltd .
  * @file    APP.h 
  * @version V1.0.0
  * @date    2022
  * @author  Aisinochip Firmware Team  
  * @brief   DMA demo Header code.
  ******************************************************************************        
*/

#ifndef __APP_H__
#define __APP_H__

#include "ACM32Fxx_HAL.h"

extern uint8_t gu8_Buffer_A[256];
extern uint8_t gu8_Buffer_B[256];

extern DMA_HandleTypeDef DMA_M2M_Handle;

typedef enum
{
    TEST_M2M_CYCLE,
    TEST_M2M_STANDARD_IT,
    TEST_M2M_STANDARD_POLLING,
}enum_TEST_MODE_t;

/* Function : DMA_M2M_Test */
void DMA_Link_Test(void);

#endif
