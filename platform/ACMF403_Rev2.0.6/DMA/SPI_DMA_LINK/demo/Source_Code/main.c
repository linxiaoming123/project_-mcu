/*
 **********************************************************************
 * Copyright (c)  2008 - 2022, Shanghai AisinoChip Co.,Ltd .
 * @file        main.c    
 * @version     V1.0.0
 * @date        2022 
 * @author      Aisinochip Firmware Team  
 * @brief       DMA Test Main File 
 **********************************************************************
 */
#include  "APP.h"

#define UART_BAUD_RATE  115200

UART_HandleTypeDef UART_Handle;

/************************************************************************
 * function   : Uart_Init
 * Description: Uart Initiation. 
 ************************************************************************/ 
void Uart_Init(void) 
{
    UART_Handle.Instance        = UART2;    
    UART_Handle.Init.BaudRate   = UART_BAUD_RATE; 
    UART_Handle.Init.WordLength = UART_WORDLENGTH_8B;
    UART_Handle.Init.StopBits   = UART_STOPBITS_1;
    UART_Handle.Init.Parity     = UART_PARITY_NONE;
    UART_Handle.Init.Mode       = UART_MODE_TX_RX_DEBUG;
    UART_Handle.Init.HwFlowCtl  = UART_HWCONTROL_NONE;
    
    HAL_UART_Init(&UART_Handle);
	
    // UART_DEBUG_ENABLE control printfS   
    printfS("MCU is running, HCLK=%dHz, PCLK=%dHz\n", System_Get_SystemClock(), System_Get_APBClock());
}

/*********************************************************************************
* Function    : main
* Description : The application entry point.
* Input       : None
* Output      : None
**********************************************************************************/
int main(void)  
{
    System_Init(); 

    Uart_Init();

    DMA_Link_Test();
    
    while(1)
    {
        
    }
}
