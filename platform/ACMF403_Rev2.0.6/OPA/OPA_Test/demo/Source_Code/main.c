/***********************************************************************
 * All rights reserved.
 * Filename    : main.c
 * Description : main source file
 * Author(s)   : Chris_Kyle
 * version     : V1.0
 * Modify date : 2019-11-13
 ***********************************************************************/
#include  "APP.h"

#define UART_BAUD_RATE  115200

UART_HandleTypeDef UART2_Handle;

/************************************************************************
 * function   : Uart_Init
 * Description: Uart Initiation. 
 ************************************************************************/ 
void Uart_Init(void) 
{
    UART2_Handle.Instance        = UART2;    
    UART2_Handle.Init.BaudRate   = UART_BAUD_RATE; 
    UART2_Handle.Init.WordLength = UART_WORDLENGTH_8B;
    UART2_Handle.Init.StopBits   = UART_STOPBITS_1;
    UART2_Handle.Init.Parity     = UART_PARITY_NONE;
    UART2_Handle.Init.Mode       = UART_MODE_TX_RX_DEBUG;
    UART2_Handle.Init.HwFlowCtl  = UART_HWCONTROL_NONE;
    
    HAL_UART_Init(&UART2_Handle);
	
    // UART_DEBUG_ENABLE control printfS   
    printfS("MCU is running, HCLK=%dHz, PCLK=%dHz\n", System_Get_SystemClock(), System_Get_APBClock());
}

/***********************************************************************
 * function: main frame
 * input�� (none)
 * output��(none)
 * return��(none)
 ***********************************************************************/
int main(void)  
{
    System_Init(); 

    Uart_Init();

    OPA_Test(TEST_OPA_PGA);
    
    while(1)
    {
    }
}
