OPA应用示例工程：

示例demo提供了几个测试项，根据enum_TEST_MODE_t枚举选择特定的功能：

    TEST_OPA_PGA：运放可编程增益模式；可以通过软件修改运放增益；

    TEST_OPA_SA：运放外置模式；需搭配外部电路使用；

    TEST_OPA_UG：运放单位增益模式；此时放大倍数为1，可做缓冲；
