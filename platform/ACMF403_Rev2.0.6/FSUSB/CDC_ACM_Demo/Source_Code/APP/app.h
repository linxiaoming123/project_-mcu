/***********************************************************************
 * Filename    : app.h
 * Description : app header file
 * Author(s)   : xwl  
 * version     : V1.0
 * Modify date : 2019-09-24
 ***********************************************************************/
#ifndef __APP_H__
#define __APP_H__

#include "usb_app.h"  
#include "cdc_acm.h"    

extern uint8_t g_usb_data_buffer[4096]; 

#define CDC_IN_BUF  (g_usb_data_buffer)   
#define CDC_OUT_BUF (g_usb_data_buffer+2048)     
#define CDC_TX_SIZE  2048   
#define CDC_RX_SIZE  2048    

void VCOM_Process_Out_Data(void);  

void VCOM_Process_InData(void);    

#endif

