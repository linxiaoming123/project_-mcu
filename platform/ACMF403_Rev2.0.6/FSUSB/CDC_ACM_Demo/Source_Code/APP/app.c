/***********************************************************************
 * Filename    : app.c
 * Description : app source file
 * Author(s)   : xwl  
 * version     : V1.1
 * Modify date : 2021-09-10  
 ***********************************************************************/
#include  "app.h" 
#include "cdc_acm.h"  

uint8_t g_usb_data_buffer[4096];  

void VCOM_Cdc_Acm_Init(void)  
{
    CDC_Acm_Init_Info();     
} 

// process OUT packet which recieved by usb device  
void VCOM_Process_Out_Data(void)
{
    uint32_t packet_length;
    
    packet_length = HAL_FSUSB_Receive_Data(CDC_OUT_BUF, EPX_MAX_PACKET_SIZE, USB_EP_OUT, 1);  
    
    if (packet_length)
    {
        CDC_Acm_Send_USB_Data(CDC_OUT_BUF, packet_length);    
        packet_length = 0;   
    }
    
}

// process in data received by uart  
void VCOM_Process_InData(void) 
{
    CDC_Acm_Send_Uart_Data();   
}    


