/***********************************************************************
 * Filename    : main.c
 * Description : Modules studying
 * Author(s)   : eric   
 * version     : 1.0
 * Modify date : 2012-02-02
 ***********************************************************************/

#include  "usb_app.h"
#include  "usb_desc.h"


const USB_DEVICE_DESCRIPTOR device_descr=
 {
	0x12,
	USB_DEVICE_DESCRIPTOR_TYPE,	//bDescriptorType: DEVICE
	0x0200,	                    //bcdUSB: usb 2.0

	0x02,				        // cdc class      
	0x02,				        //bDeviceSubClass: 0
	0x01,				        //bDeviceProtocol: class specific protocols NOT used on device basis
	EP0_MAX_PACKET_SIZE,	    //bMaxPacketSize0: maximum packet size for endpoint zero
	0x1235,	                    //idVendor	//0x1235   	  	
	0xABCE,	                    //idProduct
	0x0200,	                    //bcdDevice
	INDEX_MANUFACTURER,         //iManufacturer: index of string
	INDEX_PRODUCT,              //iProduct: index of manufacturer string
	INDEX_SERIALNUMBER,         //iSerialNumber: index of product string
	0x01                        //bNumConfigurations: 1 configuration
};


USB_DESCRIPTOR_CONFIG 	usb_descriptor_config=
{
  //����������
  {
	0x09,	             //bLength
	USB_CONFIGURATION_DESCRIPTOR_TYPE,	//bDescriptor 02
	CONFIG_TOTAL_LENGTH,   //wTotalLength 
	0x02,	          	   //bNumInterface       	   
	0x01,		           //bConfiguration Value
	0x00,			       //iConfiguration
	0x80,			       //bmAttributes,BUS-POWER
	0xfa                   //0x64	 //bMaxPower,200mA
  },
   //�ӿ�0������(CDC Communication)
  {
	0x09,		         //bLength: 0x09 bytes
	USB_INTERFACE_DESCRIPTOR_TYPE,		         //bDescriptorType: INTERFACE
	0x00,                //bInterfaceNumber: interface 0
	0x00,                //bAlternateSetting: alternate setting 0
	0x01,                //bNumEndpoints: 1 endpoint + zero ep
	0x02,                //bInterfaceClass: Communication Interface Class
	0x02,                //bInterfaceSubClass: vendor specific
	0x01,                //bInterfaceProtocol:
	0x00                 //iInterface: index of string
  },
  //�˵���������EP4 INTR IN
  {
	0x07,			     //bLength: 0x07 bytes
	USB_ENDPOINT_DESCRIPTOR_TYPE,                //bDescriptorType: ENDPOINT
	(EP_DIR_IN+USB_EP4),                //bEndpointAddress: IN endpoint 1
	0x03,                //bmAttributes: BULK-ONLY(00:Control 01:isoch 10:bulk 11:intr
	0x08,    //wMaxPacketSize: 8 bytes
	0x20                 //bInterval: polling interval is 1 ms
  },
  
  //�ӿ�1������(CDC Data)
  {
	0x09,		         //bLength: 0x09 bytes
	USB_INTERFACE_DESCRIPTOR_TYPE,		         //bDescriptorType: INTERFACE
	0x01,                //bInterfaceNumber: interface 1
	0x00,                //bAlternateSetting: alternate setting 0
	0x02,                //bNumEndpoints: 2 endpoint + zero ep
	0x0A,   //bInterfaceClass: Data Interface Class
	0x00,      //bInterfaceSubClass: 
	0x00,//bInterfaceProtocol:
	0x00                 //iInterface: index of string
  },
//�˵���������EP3 BULK OUT
  {
	0x07,               //bLength: 0x07 bytes
	0x05,               //bDescriptorType: ENDPOINT
	(EP_DIR_OUT+USB_EP_OUT),               //bEndpointAddress: out endpoint 1
	0x02,               //bmAttributes: BULK-ONLY
	EPX_MAX_PACKET_SIZE,   //wMaxPacketSize: 64 bytes
	0x00		    	 //bInterval: polling interval is 1 ms
  },
  //�˵���������EP2 BULK IN
  {
	0x07,               //bLength: 0x07 bytes
	0x05,               //bDescriptorType: ENDPOINT
	(EP_DIR_IN+USB_EP_IN),               //bEndpointAddress: out endpoint 1
	0x02,               //bmAttributes: BULK-ONLY
	EPX_MAX_PACKET_SIZE,   //wMaxPacketSize: 64 bytes
	0x00		    	 //bInterval: polling interval is 1 ms
  },  
	

};


const UINT8 StrDesc_LanguageID[4] = 
{
	4,				// Num bytes of this descriptor
	3,				// String descriptor
	0x09,			// Language ID LSB
	0x04			// Language ID	
};

const UINT8 StrDesc_Manufacturer[22]=
{
	22,				// Num bytes of this descriptor
	3,				// String descriptor
	'A',	0,
	'i',	0,
	's',	0,
	'i',	0,
	'n',	0,
	'o',	0,
	'C',	0,
	'h',	0,
	'i',	0,
	'p',	0
};

#if 1  
const UINT8 StrDesc_Product[42]=  
{
	0x2A, 0x03, 'A', 0x00,  'I', 0x00, 'S', 0x00, 'I', 0x00, 'N', 0x00, 'O', 0x00, 'C', 0x00, 'H', 0x00, 'I', 0x00, 'P', 0x00, 
	0x20, 0x00, 0x43, 0x00, 0x4d, 0x00, 0x53, 0x00, 0x49, 0x00,  0x53, 0x00, 0x2d, 0x00, 0x44, 0x00, 0x41, 0x00, 0x50, 0x00,    
};
#else  
const UINT8 StrDesc_Product[40]=  
{
	40,				// Num bytes of this descriptor
	3,				// String descriptor
	'U',	0,
	'S',	0,
	'B',	0,
	' ',	0,
	'A',	0,
	'i',	0,
	's',	0,
	'i',	0,
	'n',	0,
	'o',	0,
	'C',	0,
	'h',	0,
	'i',	0,
	'p',	0,
	' ',	0,
	'V',	0,
	'C',	0,
	'O',	0,
	'M',	0
};  
#endif 

const UINT8 StrDesc_SerialNumber[34]=
{
	34,			// Num bytes of this descriptor
	3,			// String descriptor
	'1',	0,
	'2',	0,
	'3',	0,
	'4',	0,
	'5',	0,
	'6',	0,
	'7',	0,
	'8',	0,
	'9',	0,
	'0',	0,
	'A',	0,
	'B',	0,
	'C',	0,
	'D',	0,
	'E',	0,
	'F',	0
};  



