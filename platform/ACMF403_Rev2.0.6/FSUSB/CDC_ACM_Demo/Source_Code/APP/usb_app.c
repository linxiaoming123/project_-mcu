/***********************************************************************
 * Filename    : app.c
 * Description : app source file
 * Author(s)   : xwl  
 * version     : V1.0
 * Modify date : 2020-04-07  
 ***********************************************************************/
#include  "app.h"
#include  "usb_app.h"  
#include  "usb_desc.h" 
#include  "cdc_acm.h" 


static DEVICE_REQUEST dev_req;  
static uint8_t config_value=0;
static uint8_t AltSetting=0;
static uint16_t usb_device_status=0;  // 远程唤醒和自供电状态 
static uint16_t usb_configure = 0;      

volatile uint32_t flag_usb_int=0;
volatile uint32_t flag_usb_ep1_int=0;
volatile uint32_t flag_usb_ep2_int=0;
volatile uint32_t flag_usb_ep3_int=0;
volatile uint32_t flag_usb_ep4_int=0;


void USB_IRQHandler(void)
{
	volatile uint32_t temp = 0;      

	temp = HAL_FSUSB_GET_Interrupt_Status;  

	if(temp&USB_SUSPEND)	  //suspend
	{		
		flag_usb_int |=USB_SUSPEND; 
        HAL_FSUSB_Clear_Interrupt(USB_SUSPEND);  
	}
	if(temp&USB_RESUME)	  //resume
	{
		flag_usb_int |=USB_RESUME;  
        HAL_FSUSB_Clear_Interrupt(USB_RESUME);  
	}
	if(temp&USB_BUS_RESET)	  	//bus reset	  
	{		
		flag_usb_int |=USB_BUS_RESET;  
        HAL_FSUSB_Clear_Interrupt(USB_BUS_RESET);    
	}	

	if(temp &USB_EP0_SETUP_PACKET)	//EP0
	{
		flag_usb_int |= USB_EP0_SETUP_PACKET;  
        HAL_FSUSB_Clear_Interrupt(USB_EP0_SETUP_PACKET);         
	}
	
	if(!flag_usb_ep1_int)
	{
		if(temp & USB_EP1_OUT_PACKET)	//EP1
		{   
			while(!(HAL_FSUSB_GET_Interrupt_Status & MASK_EPX_ACK(USB_EP1))){}; // wait for ACK sent to host      
            HAL_FSUSB_Clear_Interrupt(MASK_EPX_ACK(USB_EP1));   

            if (HAL_OK == HAL_FSUSB_Check_Out_Packet(USB_EP1) ) 
            {
                flag_usb_ep1_int = USB_EP1_OUT_PACKET;   
            }							
			// clear in token flag, for Interrupt tranfer type, in token flag may set after sending response    
            HAL_FSUSB_Clear_Interrupt(USB_EP1_OUT_PACKET | USB_EP1_IN);      
		}  
	} 
	if(!flag_usb_ep2_int)
	{
		if(temp &USB_EP2_OUT_PACKET)	//EP2
		{  		
			while(!(HAL_FSUSB_GET_Interrupt_Status & MASK_EPX_ACK(USB_EP2))){}; // wait for ACK sent to host    
            HAL_FSUSB_Clear_Interrupt(MASK_EPX_ACK(USB_EP2));      
                
			if (HAL_OK == HAL_FSUSB_Check_Out_Packet(USB_EP2) ) 
            {
                flag_usb_ep2_int = USB_EP2_OUT_PACKET;   
            }			
							
			// clear in token flag, for Interrupt tranfer type, in token flag may set after sending response    
            HAL_FSUSB_Clear_Interrupt(USB_EP2_OUT_PACKET | USB_EP2_IN);   
		}  
	} 
	if(!flag_usb_ep3_int)
	{
		if(temp &USB_EP3_OUT_PACKET)	//EP3
		{   
			while(!(HAL_FSUSB_GET_Interrupt_Status & MASK_EPX_ACK(USB_EP3))){}; // wait for ACK sent to host    
            HAL_FSUSB_Clear_Interrupt(MASK_EPX_ACK(USB_EP3));       
			
            if (HAL_OK == HAL_FSUSB_Check_Out_Packet(USB_EP3) )   
            {
                flag_usb_ep3_int = USB_EP3_OUT_PACKET;    
            }	   
			
			// clear in token flag, for Interrupt tranfer type, in token flag may set after sending response    
            HAL_FSUSB_Clear_Interrupt(USB_EP3_OUT_PACKET | USB_EP3_IN);   
		}  
	}      		
	if(!flag_usb_ep4_int)
	{
		if(temp &USB_EP4_OUT_PACKET)	//EP4
		{   
			while(!(HAL_FSUSB_GET_Interrupt_Status & MASK_EPX_ACK(USB_EP4))){}; // wait for ACK sent to host    
            HAL_FSUSB_Clear_Interrupt(MASK_EPX_ACK(USB_EP4));    
			
            if (HAL_OK == HAL_FSUSB_Check_Out_Packet(USB_EP4) )   
            {
                flag_usb_ep4_int = USB_EP4_OUT_PACKET;      
            }	 
			
			// clear in token flag, for Interrupt tranfer type, in token flag may set after sending response     
		    HAL_FSUSB_Clear_Interrupt(USB_EP4_OUT_PACKET | USB_EP4_IN);     
		}  
	}  

	USB_Setup();  
}  


/*----------------- 标准请求--------------------*/
void usb_get_status(void)
{  
	uint8_t recipient=0;
	uint16_t status=0;
	uint8_t bEpIndex=0;
	uint8_t bEpDir=0;
	uint16_t ep_status=0;

	printfS("get_status!\n");

	recipient = dev_req.bmRequestType & USB_RECIPIENT_MASK;
	if(recipient==USB_RECIPIENT_DEVICE)
	{
		HAL_FSUSB_Send_Data((uint8_t *)(&usb_device_status),2,USB_EP0);	   
	}
	else if(recipient==USB_RECIPIENT_INTERFACE)
   	{
		HAL_FSUSB_Send_Data((uint8_t *)(&status),2,USB_EP0);   
   	}
   	else if(recipient==USB_RECIPIENT_ENDPOINT)
   	{
		bEpIndex = dev_req.wIndex & 0x0f;
		bEpDir = dev_req.wIndex & 0x80;
		ep_status=HAL_USB_Get_Stall_Status(bEpIndex, bEpDir);   

		HAL_FSUSB_Send_Data((uint8_t *)(&ep_status),2,USB_EP0);	   	
   	}
   	else
      	HAL_FSUSB_EP0_Send_Stall();	
}


void usb_clear_feature(void)
{ 
	uint8_t recipient=0;
	uint8_t bEpIndex=0;
	uint8_t bEpDir=0;

	printfS("clear_feature!\n");   

	recipient = dev_req.bmRequestType & USB_RECIPIENT_MASK;
	if(recipient==USB_RECIPIENT_DEVICE)
	{
		if(dev_req.wValue==USB_FEATURE_REMOTE_WAKEUP)
		{
			usb_device_status &= (~USB_GETSTATUS_REMOTE_WAKEUP);
		}
		HAL_FSUSB_EP0_Send_Empty_Packet();  
	}
   	else if(recipient==USB_RECIPIENT_ENDPOINT)
   	{
		bEpIndex = dev_req.wIndex & 0x0f;
		bEpDir = dev_req.wIndex & 0x80;
    	usb_clear_stall(bEpIndex, bEpDir);
		HAL_FSUSB_EP0_Send_Empty_Packet();    
   	}
   	else
		HAL_FSUSB_EP0_Send_Stall();
}


void usb_set_feature(void)
{
	uint8_t recipient=0;
	uint8_t bEpIndex=0;
	uint8_t bEpDir=0;

	printfS("set_feature!\n");
   
	recipient = dev_req.bmRequestType & USB_RECIPIENT_MASK;
	if(recipient==USB_RECIPIENT_DEVICE)
	{
		if(dev_req.wValue==USB_FEATURE_REMOTE_WAKEUP)
		{  
       		usb_device_status |= USB_GETSTATUS_REMOTE_WAKEUP;
		}
		HAL_FSUSB_EP0_Send_Empty_Packet();
	}
	else if(recipient==USB_RECIPIENT_ENDPOINT)
	{
		bEpIndex = dev_req.wIndex & 0x0f;
		bEpDir = dev_req.wIndex & 0x80;
    	usb_send_stall(bEpIndex, bEpDir);
		HAL_FSUSB_EP0_Send_Empty_Packet();  
   	}
   	else
      	HAL_FSUSB_EP0_Send_Stall();
}


void usb_set_address(void)
{
//	printfS("set_address,addr=0x%x\n",dev_req.wValue);
//	printfS("ADDR=0x%x \n", USBCTRL->USB_ADDR);   
}


void usb_get_configuration(void)
{
//	printfS("get_configuration!\n");

	HAL_FSUSB_Send_Data(&config_value,1,USB_EP0);            
}

void usb_set_configuration(void)
{  
    usb_configure = USB_CONFIGURE_PATTERN;   
	
	if( (dev_req.wValue==0)||(dev_req.wValue==1) )
   	{
	  	config_value=dev_req.wValue;
		HAL_FSUSB_EP0_Send_Empty_Packet();    
   	}
   	else
	  	HAL_FSUSB_EP0_Send_Stall();	
}

void usb_get_interface(void)
{
	uint8_t inter_value=0;
	uint8_t recipient=0;

//	printfS("get_interface!\n");

	recipient = dev_req.bmRequestType & USB_RECIPIENT_MASK;
	if(recipient==USB_RECIPIENT_INTERFACE)
	{
		inter_value=AltSetting+dev_req.wIndex;	 
		HAL_FSUSB_Send_Data(&inter_value,1,USB_EP0);     
	}
	else	
		HAL_FSUSB_EP0_Send_Stall();

}

    
void usb_set_interface(void)
{
	uint8_t  alt,inter_num;
	uint8_t recipient=0;

	printfS("set_interface!\n");

	recipient = dev_req.bmRequestType & USB_RECIPIENT_MASK;
	if(recipient==USB_RECIPIENT_INTERFACE)
	{
		HAL_FSUSB_EP0_Send_Stall();

	}
	else	
		HAL_FSUSB_EP0_Send_Stall();
    
}

  
void usb_get_description(void)
{
	uint16_t wValue;
	uint16_t wLength;
	uint8_t wValue_bIndex;

	printfS("get_description->\n");

	wValue = (dev_req.wValue>>8);
	wValue_bIndex = dev_req.wValue & 0xff;
	wLength = dev_req.wLength; 
   
	if(wValue == USB_DEVICE_DESCRIPTOR_TYPE)
   	{
	    printfS("DEV_DESCRIPTOR!\n");
      
	  	if(wLength >device_descr.bLength)
	  	{
			wLength = device_descr.bLength;
	  	}
		HAL_FSUSB_Send_Data((uint8_t *)(&device_descr),wLength,USB_EP0);	  
   	}
   	else if(wValue == USB_CONFIGURATION_DESCRIPTOR_TYPE)
   	{
	    printfS("CONFIG_DESCRIPTOR!\n");
      
      	if( wLength > sizeof(usb_descriptor_config) )
	  	{
		 	wLength = sizeof(usb_descriptor_config);
	  	}
		HAL_FSUSB_Send_Data( (uint8_t *)(&usb_descriptor_config),wLength, USB_EP0 );		
   	}

	else if (wValue == USB_STRING_DESCRIPTOR_TYPE)
	{    
		if(wValue_bIndex == INDEX_LANGUAGE_ID)       //LANGID string 
		{
			printfS("STRING_DESCRIPTOR__LANGID!\n");

		   	if(wLength>StrDesc_LanguageID[0]) 
		   	{
		    	wLength = StrDesc_LanguageID[0];
		   	}
			HAL_FSUSB_Send_Data((uint8_t *)StrDesc_LanguageID,wLength,USB_EP0);	  
		}
		else if(wValue_bIndex == INDEX_MANUFACTURER) //vender string
		{
			printfS("STRING_DESCRIPTOR__vender!\n");
		   
		   	if(wLength>StrDesc_Manufacturer[0]) 
		   	{
		      	wLength = StrDesc_Manufacturer[0];
		   	}
			HAL_FSUSB_Send_Data((uint8_t *)StrDesc_Manufacturer,wLength,USB_EP0);	  	
		}
		else if(wValue_bIndex == INDEX_PRODUCT)      //product string
		{
	        printfS("STRING_DESCRIPTOR__product!\n");
		   
 		   	if(wLength>StrDesc_Product[0]) 
		   	{
		      	wLength = StrDesc_Product[0];
		   	}
			HAL_FSUSB_Send_Data((uint8_t *)StrDesc_Product,wLength,USB_EP0);
		}
		else if(wValue_bIndex == INDEX_SERIALNUMBER) //SerialNumber	string
		{
         	printfS("STRING_DESCRIPTOR__SerialNumber!\n");
		   
 		   	if(wLength>StrDesc_SerialNumber[0]) 
		   	{
		      	wLength = StrDesc_SerialNumber[0];
		   	}
			HAL_FSUSB_Send_Data((uint8_t *)StrDesc_SerialNumber,wLength,USB_EP0);
		}
		else
		{		
			HAL_FSUSB_EP0_Send_Stall();
		}
	}
	else
	{
		HAL_FSUSB_EP0_Send_Stall();   
	}
}

 
void reserved(void)
{
	printfS("reserved!\n");
    
	HAL_FSUSB_EP0_Send_Stall(); 
}

/*************************************************************************
 * USB标准设备请求入口地址指针表	
 * 请求代码值由4个bit位组成，共16种可能，所以有16个元素
 *************************************************************************/
void (*StandardDeviceRequest[])(void) =
{
	usb_get_status,	        //0x00
	usb_clear_feature,	    //0x01
	reserved,
	usb_set_feature,	    //0x03
	reserved,
	usb_set_address,        //0x05
	usb_get_description,	//0x06
	reserved,
	usb_get_configuration,	//0x08
	usb_set_configuration,	//0x09
	usb_get_interface,		//0x0a
	usb_set_interface,		//0x0b
	reserved,
	reserved,
	reserved,
	reserved
}; 


void usb_control_transfer(void)
{		
	uint8_t type,req;  

	dev_req.bmRequestType =	USBCTRL->SETIP_0_3_DATA &0xff;	
	dev_req.bRequest = (USBCTRL->SETIP_0_3_DATA>>8)&0xff;
	dev_req.wValue   = (USBCTRL->SETIP_0_3_DATA>>16)&0xffff;
	dev_req.wIndex   = USBCTRL->SETIP_4_7_DATA&0xffff;
	dev_req.wLength  = (USBCTRL->SETIP_4_7_DATA>>16)&0xffff;   

	type = dev_req.bmRequestType & USB_REQUEST_TYPE_MASK;
	req  = dev_req.bRequest&USB_REQUEST_MASK;	
	if (type == USB_STANDARD_REQUEST)		//标准请求
	{
		printfS("standard request-->");
	
		(*StandardDeviceRequest[req])();   
	}
    else if(type == USB_CLASS_REQUEST)	    //类请求
	{
		if((dev_req.bmRequestType == 0x21) && (dev_req.bRequest == SET_LINE_CODING))         // SET_LINE_CODING
		{
			CDC_Acm_Set_Line_Coding(dev_req.wLength);           
		}
		else if((dev_req.bmRequestType == 0x21) && (dev_req.bRequest == SET_CONTROL_LINE_STATE))  // SET_CONTROL_LINE_STATE
		{
			// add your code here 		
			HAL_FSUSB_EP0_Send_Empty_Packet();	

		}
		else if((dev_req.bmRequestType == 0xa1) && (dev_req.bRequest == GET_LINE_CODING))  // GET_LINE_CODING
		{
			CDC_Acm_Get_Line_Coding(dev_req.wLength);       
		}
        else
        {
            HAL_FSUSB_EP0_Send_Stall();   	
        }
    }
	else
    {
		HAL_FSUSB_EP0_Send_Stall();  
    }
				
}

void USB_Bus_Reset(void)
{
    usb_configure = 0;    
	HAL_FSUSB_Enable_Interrupt(USB_SUSPEND);  //enable suspend interrupt     
    HAL_FSUSB_Bus_Reset();      
}

void USB_Suspend(void) 
{	    
	HAL_FSUSB_Disable_Interrupt(USB_SUSPEND); 	//disable suspend interrupt
	HAL_FSUSB_Suspend();
    
	//low power
}

void USB_Resume(void)  
{	
    HAL_FSUSB_Enable_Interrupt(USB_SUSPEND);      	
	HAL_FSUSB_Resume();     
}

uint8_t usb_check_if_config(void)
{
    if (usb_configure == USB_CONFIGURE_PATTERN)
    {
        return 1;  
    }
    
    return 0;  
}


void USB_Setup(void)   
{   
	if((flag_usb_int & USB_BUS_RESET))	//bus reset 
	{
		flag_usb_int &= (~USB_BUS_RESET);
		USB_Bus_Reset(); 	    	
	}

	if((flag_usb_int & USB_SUSPEND))	//suspend
	{
		flag_usb_int &= (~USB_SUSPEND);
		USB_Suspend(); 	    	
	}

	if((flag_usb_int & USB_RESUME))	    //resume
	{
		flag_usb_int &= (~USB_RESUME);
		USB_Resume();       	    	
	}
	
	if((flag_usb_int & USB_EP0_SETUP_PACKET))	//EP0 setup packet received 
	{    
		flag_usb_int &= (~USB_EP0_SETUP_PACKET);
		usb_control_transfer();
	}
}



void usb_data_transfer_monitor(void)
{
	if( flag_usb_ep3_int == USB_EP3_OUT_PACKET)	//EP3 OUT packet received
	{
        HAL_FSUSB_Disable_Interrupt(USB_EP3_OUT_PACKET); 	  
                 
		VCOM_Process_Out_Data();   

		flag_usb_ep3_int=0;	  
		HAL_FSUSB_Enable_Interrupt(USB_EP3_OUT_PACKET);   	  
	}
}




