#include  "app.h"
#include  "usb_app.h"  
#include  "cdc_acm.h"   


STR_USBD_CDC_T gCdcInfo; 
UART_HandleTypeDef CDC_UART_Handle;  

/*********************************************************************************
* Function    : UART2_IRQHandler
* Description : UART2 Interrupt handler
* Input       : None
* Output      : None
**********************************************************************************/
void UART2_IRQHandler(void)
{
    HAL_UART_IRQHandler(&CDC_UART_Handle);
}

void CDC_Itf_Init(void)  
{  
    CDC_UART_Handle.Instance = CDC_UART_INSTANCE;   
    
    CDC_UART_Handle.lu32_RxCount = 0;
    CDC_UART_Handle.lu32_RxSize = 0; 
    CDC_UART_Handle.lu8_RxBusy = false;  
    CDC_UART_Handle.lu8_TxBusy = false;     
    CDC_UART_Handle.lu32_TxCount = 0;
    CDC_UART_Handle.lu32_TxSize = 0;  
    CDC_UART_Handle.ErrorCode = HAL_UART_ERROR_NONE;  
       
    CDC_UART_Handle.Init.BaudRate   = LINE_DEFAULT_BAUD_RATE;  
    CDC_UART_Handle.Init.WordLength = UART_WORDLENGTH_8B;
    CDC_UART_Handle.Init.StopBits   = UART_STOPBITS_1;
    CDC_UART_Handle.Init.Parity     = UART_PARITY_NONE;
    CDC_UART_Handle.Init.Mode       = UART_MODE_TX_RX;
    CDC_UART_Handle.Init.HwFlowCtl  = UART_HWCONTROL_NONE;   
    
    HAL_UART_Init(&CDC_UART_Handle);    
    
    HAL_UART_Receive_IT(&CDC_UART_Handle, CDC_IN_BUF, CDC_RX_SIZE, UART_RX_FIFO_1_2);     
}
   
void CDC_Acm_Init_Info(void)
{    
    gCdcInfo.u32DTERate = LINE_DEFAULT_BAUD_RATE;  
    gCdcInfo.u8CharFormat = LINE_STOP_1; 
    gCdcInfo.u8DataBits = LINE_DATA_LEN_8;  
    gCdcInfo.u8ParityType = LINE_PARITY_NONE;   
    
    CDC_Itf_Init();       
}

void ComPort_Config(void)
{
    HAL_UART_DeInit(&CDC_UART_Handle);   
    
    switch(gCdcInfo.u8CharFormat)
    {
        case LINE_STOP_1: 
        CDC_UART_Handle.Init.StopBits = UART_STOPBITS_1; 
        break;
        
        case LINE_STOP_2: 
        CDC_UART_Handle.Init.StopBits = UART_STOPBITS_2;  
        break;
        
        default:   
        CDC_UART_Handle.Init.StopBits = UART_STOPBITS_1;     
        break;   
    }
    
    switch(gCdcInfo.u8ParityType) 
    {
        case LINE_PARITY_NONE: 
        CDC_UART_Handle.Init.Parity = UART_PARITY_NONE;    
        break; 
        
        case LINE_PARITY_ODD:  
        CDC_UART_Handle.Init.Parity = UART_PARITY_ODD;   
        break;
        
        case LINE_PARITY_EVEN:  
        CDC_UART_Handle.Init.Parity = UART_PARITY_EVEN;     
        break;   
        
        default:  
        CDC_UART_Handle.Init.Parity = UART_PARITY_NONE;     
        break;  
        
    }   
    
    CDC_UART_Handle.Init.BaudRate = gCdcInfo.u32DTERate;        
    CDC_UART_Handle.Init.WordLength = UART_WORDLENGTH_8B;   
       
    HAL_UART_Init(&CDC_UART_Handle);    

    HAL_UART_Receive_IT(&CDC_UART_Handle, CDC_IN_BUF, CDC_RX_SIZE, UART_RX_FIFO_1_2);           
}

void CDC_Acm_Set_Line_Coding(uint32_t length)
{
    HAL_FSUSB_Receive_Data((UINT8 *)&gCdcInfo, length, USB_EP0, 0) ;       
    HAL_FSUSB_EP0_Send_Empty_Packet();	
    
    ComPort_Config();           
}

void CDC_Acm_Get_Line_Coding(uint32_t length)  
{
    HAL_FSUSB_Send_Data((UINT8 *)&gCdcInfo, MIN(sizeof(STR_USBD_CDC_T), length), USB_EP0);     
}    

void CDC_Acm_Send_USB_Data(uint8_t *pdata, uint32_t length)
{
    HAL_UART_Transmit(&CDC_UART_Handle, pdata, length, 0);      
}

void CDC_Acm_Send_Uart_Data(void)
{
    uint32_t rx_len;
        
    if ( (CDC_UART_Handle.lu32_RxCount) && (false == CDC_UART_Handle.lu8_RxBusy) )     
    {
        rx_len = CDC_UART_Handle.lu32_RxCount; 

        if  (usb_check_if_config())
        {
            HAL_FSUSB_Send_Data(CDC_IN_BUF, rx_len, USB_EP_IN);   
        }
        CDC_UART_Handle.lu32_RxCount = 0;    
        HAL_UART_Receive_IT(&CDC_UART_Handle, CDC_IN_BUF, CDC_RX_SIZE, UART_RX_FIFO_1_2);       
    }
}


