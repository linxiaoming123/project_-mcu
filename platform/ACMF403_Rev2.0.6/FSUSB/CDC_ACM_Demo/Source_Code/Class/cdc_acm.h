#ifndef __CDC_ACM_H__
#define __CDC_ACM_H__   

#include "usb_app.h"  

extern volatile uint32_t flag_usb_ep1_int;   
extern volatile uint32_t flag_usb_ep2_int;  
extern volatile uint32_t flag_usb_ep3_int;  
extern volatile uint32_t flag_usb_ep4_int;      



/* CDC Requests                                   */
/**************************************************/
#define SEND_ENCAPSULATED_COMMAND               0x00
#define GET_ENCAPSULATED_RESPONSE               0x01
#define SET_COMM_FEATURE                        0x02
#define GET_COMM_FEATURE                        0x03
#define CLEAR_COMM_FEATURE                      0x04
#define SET_LINE_CODING                         0x20
#define GET_LINE_CODING                         0x21
#define SET_CONTROL_LINE_STATE                  0x22
#define SEND_BREAK                              0x23
#define NO_CMD                                  0xFF   


#define LINE_PARITY_NONE 0
#define LINE_PARITY_ODD  1   
#define LINE_PARITY_EVEN 2 

#define LINE_STOP_1  0  
#define LINE_STOP_2  2   

#define LINE_DATA_LEN_8   8    

#define LINE_DEFAULT_BAUD_RATE  115200  

#define CDC_UART_INSTANCE  UART2   


typedef struct DRVUSBD_CDC_STRUCT
{
	UINT32  u32DTERate;     /* Baud rate    */
	UINT8   u8CharFormat;   /* stop bit     */
	UINT8   u8ParityType;   /* parity       */
	UINT8   u8DataBits;     /* data bits    */

}STR_USBD_CDC_T;  

extern STR_USBD_CDC_T gCdcInfo; 

void CDC_Acm_Init_Info(void); 
void CDC_Acm_Set_Line_Coding(uint32_t length);         
void CDC_Acm_Get_Line_Coding(uint32_t length);     
void CDC_Acm_Send_USB_Data(uint8_t *pdata, uint32_t length);    
void CDC_Acm_Send_Uart_Data(void);   

#endif
