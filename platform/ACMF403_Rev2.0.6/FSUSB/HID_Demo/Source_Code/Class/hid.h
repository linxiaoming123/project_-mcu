#ifndef __HID_H__
#define __HID_H__

#include "usb_app.h"  

extern volatile uint32_t flag_usb_ep1_int;   
extern volatile uint32_t flag_usb_ep2_int;  
extern volatile uint32_t flag_usb_ep3_int;  
extern volatile uint32_t flag_usb_ep4_int;      

extern void (*ClassRequest[])(void);  
void hid_proc(void);

#endif
