#include  "app.h"
#include  "usb_app.h"  
#include  "hid.h"
// 

uint8_t g_usb_data_buffer[4096];     

#define USB_IN_BUF  (g_usb_data_buffer)   
#define USB_OUT_BUF (g_usb_data_buffer)  

//*************************************************************************
//HID类(class)请求入口地址指针表	
//*************************************************************************
// for EP0 
void get_report(void)
{
	printfS( " get_report! \n"); 	
}

// for EP0 
void set_report(void)
{ 
	printfS( " set_report! \n");  	
}

void get_idle(void)
{
	printfS( " get_idle! \n");
	HAL_FSUSB_EP0_Send_Empty_Packet();
}

void get_protocol(void)
{
	printfS( " get_protocol! \n");
	HAL_FSUSB_EP0_Send_Empty_Packet();
}

void set_idle(void)
{
	printfS( " set_idle! \n");	 
	HAL_FSUSB_EP0_Send_Empty_Packet();
}

void set_protocol(void)
{
	printfS( "set_protocol! \n");
	HAL_FSUSB_EP0_Send_Empty_Packet();
}

void reserved_class(void)  
{   
	HAL_FSUSB_EP0_Send_Stall(); 
}

void (*ClassRequest[])(void) =
{
	reserved_class,
	get_report,	       
	get_idle,	      
	get_protocol,      
	reserved_class,
	reserved_class,
	reserved_class,
	reserved_class,
	reserved_class,
	set_report,       
	set_idle,	      
	set_protocol,     
};  

void hid_proc()
{ 
	UINT32 length;

	length=HAL_FSUSB_Get_FIFO_Length(out_ep_index);
	if(length==0)	return; 
    
	HAL_FSUSB_Read_EP_MEM8(USB_IN_BUF, length, 0, out_ep_index);       
    USBCTRL->EPxCSR[out_ep_index] |= 1<<11;  //set rx ready    

	if(length<EPX_MAX_PACKET_SIZE)
	{
		flag_usb_ep1_int=0;
		USB_OUT_BUF[0] = 0x01;
		HAL_FSUSB_Send_Data(USB_OUT_BUF, length, in_ep_index);
	}
	else
	{
		HAL_FSUSB_Receive_Data(USB_IN_BUF+EPX_MAX_PACKET_SIZE, HID_REPORT_SIZE+1-EPX_MAX_PACKET_SIZE, out_ep_index, 0);  
		
		flag_usb_ep1_int=0;
		USB_OUT_BUF[0] = 0x01; 		
		HAL_FSUSB_Send_Data(USB_OUT_BUF, HID_REPORT_SIZE+1, in_ep_index); 	
		   
	}
}


