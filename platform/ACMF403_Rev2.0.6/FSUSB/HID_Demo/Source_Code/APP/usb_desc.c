/***********************************************************************
 * Filename    : main.c
 * Description : Modules studying
 * Author(s)   : eric   
 * version     : 1.0
 * Modify date : 2012-02-02
 ***********************************************************************/

#include  "usb_app.h"
#include  "usb_desc.h"


const USB_DEVICE_DESCRIPTOR device_descr=
 {
	0x12,
	USB_DEVICE_DESCRIPTOR_TYPE,	//bDescriptorType: DEVICE
	0x0200,	                    //bcdUSB: usb 2.0

	0,
	0,				            //bDeviceSubClass: 0
	0,				            //bDeviceProtocol: class specific protocols NOT used on device basis
	EP0_MAX_PACKET_SIZE,	    //bMaxPacketSize0: maximum packet size for endpoint zero
	0x1235,	                    //idVendor	//0x1235  	  	
	0xABCD,	                    //idProduct
	0x0200,	                    //bcdDevice
	INDEX_MANUFACTURER,         //iManufacturer: index of string
	INDEX_PRODUCT,             	//iProduct: index of manufacturer string
	INDEX_SERIALNUMBER,         //iSerialNumber: index of product string
	0x01                        //bNumConfigurations: 1 configuration
};


USB_DESCRIPTOR_CONFIG 	usb_descriptor_config=
{
	//����������
  {
	0x09,	             //bLength
	USB_CONFIGURATION_DESCRIPTOR_TYPE,	//bDescriptor 02
	CONFIG_TOTAL_LENGTH,   //wTotalLength 
	0x01,	          	   //bNumInterface   
	  
	0x01,		           //bConfiguration Value
	0x00,			       //iConfiguration
	0x80,			       //bmAttributes,BUS-POWER
	0xfa                   //0x64	 //bMaxPower,200mA
  },
  //�ӿ�1������
  {
	0x09,		         //bLength: 0x09 bytes
	0x04,		         //bDescriptorType: INTERFACE
	0x00,                //bInterfaceNumber: interface 0
	0x00,                //bAlternateSetting: alternate setting 0
	0x02,                //bNumEndpoints: 2 endpoint + zero ep
	USB_DEVICE_CLASS_HUMAN_INTERFACE,	// Interface Class: HID
	0x00,			     // Interface Sub Class: RCB
	0x00,			     // Class specific protocol:
	0x00                 //iInterface: index of string
  },
  //HID������
  {
    0x09,		     	   //HID����������,= 09H
	0x21,				   //HID����������,= 21H
	0x00,				   //HID Class Specification release number in BCD
	0x01,
	0x00,				   //bCountryCode: none
	0x01,				   //bNumDescriptors:
	0x22,				   //REPORT����������,= 22H
	sizeof(hid_msg_descr), //REPORT����������
	0x00
  },
  //�˵���������EP1 INT IN
  {
	0x07,			        //bLength: 0x07 bytes
	0x05,                   //bDescriptorType: ENDPOINT
	(EP_DIR_IN+USB_EP_IN),  //bEndpointAddress: IN endpoint 1
	0x03,                   //bmAttributes: INT(00:Control 01:isoch 10:bulk 11:intr
	EPX_MAX_PACKET_SIZE,    //wMaxPacketSize: 64 bytes
	0x01                    //bInterval: polling interval is 1 ms
  },
  //�˵���������EP1 INT OUT
  {
	 0x07,                    //bLength: 0x07 bytes
	 0x05,                    //bDescriptorType: ENDPOINT
	 (EP_DIR_OUT+USB_EP_OUT), //bEndpointAddress: out endpoint 1
	 0x03,                    //bmAttributes: INT
	 EPX_MAX_PACKET_SIZE,     //wMaxPacketSize: 64 bytes
	 0x01		    	      //bInterval: polling interval is 1 ms
  }	

};

const UINT8 hid_msg_descr[30] = 
{
	0x06, 0x00, 0xff,           //usage page (Vendor defined) 
	0x09, 0x01,                 //usage (Vendor defined)
	0xa1, 0x01,                 //collection (application)
	//the input report
	0x09, 0x01,                 //usage (Vendor defined)
	0x75, 0x08,                 //report size 8 (bits) 
	0x96, (HID_REPORT_SIZE&0xff), (HID_REPORT_SIZE>>8),  //report count 32 (fields) 
	0x85, 0x01,                 //report ID (1)
	0x81, 0x06,                 //input (Data,Var,rel)
	//the output report
	0x09, 0x01,                 //usage (Vendor defined)
	0x75, 0x08,                 //report size 8 (bits) 
	0x96, (HID_REPORT_SIZE&0xff), (HID_REPORT_SIZE>>8),  //report count 32 (fields) 
	0x85, 0x02,                 //report ID (2)
	0x91, 0x06,                 //onput (Data,Var,rel)
	0xc0                        //END_COLLECTION	
};


const UINT8 StrDesc_LanguageID[4] = 
{
	4,				// Num bytes of this descriptor
	3,				// String descriptor
	0x09,			// Language ID LSB
	0x04			// Language ID	
};

const UINT8 StrDesc_Manufacturer[22]=
{
	22,				// Num bytes of this descriptor
	3,				// String descriptor
	'A',	0,
	'i',	0,
	's',	0,
	'i',	0,
	'n',	0,
	'o',	0,
	'C',	0,
	'h',	0,
	'i',	0,
	'p',	0
};

const UINT8 StrDesc_Product[24]=
{
	24,				                   //bLength: 24 bytes
	USB_STRING_DESCRIPTOR_TYPE,		   //bDescriptorType: 0x03
	'U',	0,
	'S',	0,
	'B',	0,
	' ',	0,
	'H',	0,
	'I',	0,
	'D',	0,	
	' ',	0,
	'K',	0,
	'e',	0,
	'y',	0
};

const UINT8 StrDesc_SerialNumber[34]=
{
	34,			// Num bytes of this descriptor
	3,			// String descriptor
	'1',	0,
	'2',	0,
	'3',	0,
	'4',	0,
	'5',	0,
	'6',	0,
	'7',	0,
	'8',	0,
	'9',	0,
	'0',	0,
	'A',	0,
	'B',	0,
	'C',	0,
	'D',	0,
	'E',	0,
	'F',	0
};





