


#include "mcu.h"
#include "stdint.h"
#include "board.h"
#include "SoftDelay.h"
#include "mlibc_base.h"
#include "stdio.h"
#include "mcu_usart_1.h"
#include "ModbusRTU_Slaver.h"


uint16_t read_reg(uint16_t regAddr)
{
	log_inf(0,"Read addr = %04X\r\n",regAddr);
	return 0x1122;
}

ModbusGroup grouptest = {
	.startRegAddr = 0x1000,	//起始地址
	.RegCount	  = 10,		//寄存器个数
	.Suport = {
		.cmd03 = 1,
		.cmd06 = 1,
		.cmd10 = 1
	},
	.rMapMode = MAP_REG_API,		//读映射方式
	.rMapObj  = read_reg,			//读映射对象【不同的映射模式，对应不同的映射对象类型】	
	.wMapMode = MAP_REG_API,		//写映射模式
	.wMapObj  = NULL,				//写映射对象【不同的映射模式，对应不同的映射对象类型】	
	.writeCheck = NULL,				//组的写检查
	.writeCallback = NULL,			//组的写回调 - 应答前
};
ModbusSlaver md;
uint8_t txbuff[] = "hello9999\r\n";
int main()
{
	int i=0;
	md.devAddr = 3;
	md.groupList = &grouptest;
	md.groupNum = 1;
	mcu_clk_init();
	RCC_Enable_DMA1();
	RCC_Enable_DMA2();
	RCC_Enable_GPIOA();
	RCC_Enable_GPIOE();
	RCC_Enable_USART1();
	RCC_Enable_USART2();
	uart1_init(  USART1_PA9_PA10, 115200, Parity_none );
	uart1_put_array_start(txbuff,sizeof(txbuff));
	while(1)
	{
		TypeFrame* uart1rx = uart1_read();
		if(uart1rx != NULL)
		{
			if( uart1rx->len != 0)
			{
				log_inf(0,"rx=%d\r\n" , uart1rx->len);
				//--- 接收处理 ----
				//xcmd_task( uart1rx->array , uart1rx->len);
				//
				if( ModbusProcess( uart1rx->array , uart1rx->len ,&md) == 0)
				{
					if( md.txInf.frameLen > 0)
					{
						uart1_put_array_start( md.txInf.value.u8 , md.txInf.frameLen);
					}
				}
//				LcomBase_Analayse(uart1rx->array ,uart1rx->len , app_callback);
				
				uart1_clear_rxframe(uart1rx);
			}
		}
	}
}


